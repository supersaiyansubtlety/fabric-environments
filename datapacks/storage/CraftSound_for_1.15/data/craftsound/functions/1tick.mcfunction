execute unless entity @a[tag=CSTimer] run tag @r add CSTimer
execute store result score @r[tag=CSTimer] CSTagCheck run execute if entity @a[tag=CSTimer]
execute as @a[scores={CSTagCheck=2..}] run tag @a[tag=CSTimer] remove CSTimer
execute as @a[scores={CSTagCheck=2..}] run scoreboard players reset @a[scores={CSTagCheck=1..}] CSTagCheck
scoreboard players add @a[tag=CSTimer] CSThick 1
execute if entity @a[tag=CSTimer,scores={CSThick=1}] run function craftsound:craftcheck1
execute if entity @a[tag=CSTimer,scores={CSThick=2}] run function craftsound:craftcheck2
execute if entity @a[tag=CSTimer,scores={CSThick=3}] run function craftsound:craftcheck3
execute if entity @a[tag=CSTimer,scores={CSThick=4}] run function craftsound:craftcheck4
scoreboard players reset @a[tag=CSTimer,scores={CSThick=5..}] CSThick