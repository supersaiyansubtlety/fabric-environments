playsound minecraft:block.crop.break player @a[distance=..12] ~ ~ ~ 1 0.0

scoreboard players reset @s[scores={c.bone_meal=1..}] c.bone_meal
scoreboard players reset @s[scores={c.dried_kelp=1..}] c.dried_kelp
scoreboard players reset @s[scores={c.dried_kelp_blo=1..}] c.dried_kelp_blo
scoreboard players reset @s[scores={c.hay_block=1..}] c.hay_block
scoreboard players reset @s[scores={c.jack_o_lantern=1..}] c.jack_o_lantern
scoreboard players reset @s[scores={c.kelp=1..}] c.kelp
scoreboard players reset @s[scores={c.sea_pickle=1..}] c.sea_pickle
scoreboard players reset @s[scores={c.seagrass=1..}] c.seagrass
scoreboard players reset @s[scores={c.sugar=1..}] c.sugar
scoreboard players reset @s[scores={c.sugar_cane=1..}] c.sugar_cane
scoreboard players reset @s[scores={c.seeds=1..}] c.seeds
scoreboard players reset @s[scores={c.tnt=1..}] c.tnt
scoreboard players reset @s[scores={c.wet_sponge=1..}] c.wet_sponge
scoreboard players reset @s[scores={c.wheat=1..}] c.wheat
