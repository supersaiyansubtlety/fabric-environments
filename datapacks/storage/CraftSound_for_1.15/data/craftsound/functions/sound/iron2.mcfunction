playsound minecraft:block.anvil.place player @a[distance=..2] ~ ~ ~ 0.5 1.3

scoreboard players reset @s[scores={c.golden_axe=1..}] c.golden_axe
scoreboard players reset @s[scores={c.golden_boots=1..}] c.golden_boots
scoreboard players reset @s[scores={c.golden_chest=1..}] c.golden_chest
scoreboard players reset @s[scores={c.golden_helmet=1..}] c.golden_helmet
scoreboard players reset @s[scores={c.golden_hoe=1..}] c.golden_hoe
scoreboard players reset @s[scores={c.gold_horse=1..}] c.gold_horse
scoreboard players reset @s[scores={c.golden_legging=1..}] c.golden_legging
scoreboard players reset @s[scores={c.golden_pickaxe=1..}] c.golden_pickaxe
scoreboard players reset @s[scores={c.golden_shovel=1..}] c.golden_shovel
scoreboard players reset @s[scores={c.golden_sword=1..}] c.golden_sword
scoreboard players reset @s[scores={c.iron_axe=1..}] c.iron_axe
scoreboard players reset @s[scores={c.iron_boots=1..}] c.iron_boots
scoreboard players reset @s[scores={c.iron_chest=1..}] c.iron_chest
scoreboard players reset @s[scores={c.iron_helmet=1..}] c.iron_helmet
scoreboard players reset @s[scores={c.iron_hoe=1..}] c.iron_hoe
scoreboard players reset @s[scores={c.iron_leggings=1..}] c.iron_leggings
scoreboard players reset @s[scores={c.iron_pickaxe=1..}] c.iron_pickaxe
scoreboard players reset @s[scores={c.iron_shovel=1..}] c.iron_shovel
scoreboard players reset @s[scores={c.iron_sword=1..}] c.iron_sword
scoreboard players reset @s[scores={c.shears=1..}] c.shears



