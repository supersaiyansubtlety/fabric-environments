playsound minecraft:item.armor.equip_generic player @a[distance=..12] ~ ~ ~ 1 1

scoreboard players reset @s[scores={c.chain_block=1..}] c.chain_block
scoreboard players reset @s[scores={c.chain_boots=1..}] c.chain_boots
scoreboard players reset @s[scores={c.chain_chest=1..}] c.chain_chest
scoreboard players reset @s[scores={c.chain_helmet=1..}] c.chain_helmet
scoreboard players reset @s[scores={c.chain_leg=1..}] c.chain_leg
scoreboard players reset @s[scores={c.chest_minecart=1..}] c.chest_minecart
scoreboard players reset @s[scores={c.command_block=1..}] c.command_block
scoreboard players reset @s[scores={c.command_mineca=1..}] c.command_mineca
scoreboard players reset @s[scores={c.furnace_cart=1..}] c.furnace_cart
scoreboard players reset @s[scores={c.hopper_cart=1..}] c.hopper_cart
scoreboard players reset @s[scores={c.repeat_command=1..}] c.repeat_command
scoreboard players reset @s[scores={c.saddle=1..}] c.saddle
scoreboard players reset @s[scores={c.tnt_minecart=1..}] c.tnt_minecart


