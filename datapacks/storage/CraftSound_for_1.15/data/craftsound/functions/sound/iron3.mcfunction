playsound minecraft:block.anvil.use player @a[distance=..2] ~ ~ ~ 0.5 1.1

scoreboard players reset @s[scores={c.diamond_axe=1..}] c.diamond_axe
scoreboard players reset @s[scores={c.diamond_boots=1..}] c.diamond_boots
scoreboard players reset @s[scores={c.diamond_chest=1..}] c.diamond_chest
scoreboard players reset @s[scores={c.diamond_helmet=1..}] c.diamond_helmet
scoreboard players reset @s[scores={c.diamond_horse=1..}] c.diamond_horse
scoreboard players reset @s[scores={c.diamond_hoe=1..}] c.diamond_hoe
scoreboard players reset @s[scores={c.diamond_leg=1..}] c.diamond_leg
scoreboard players reset @s[scores={c.diamond_pickax=1..}] c.diamond_pickax
scoreboard players reset @s[scores={c.diamond_shovel=1..}] c.diamond_shovel
scoreboard players reset @s[scores={c.diamond_sword=1..}] c.diamond_sword
scoreboard players reset @s[scores={c.totem=1..}] c.totem
scoreboard players reset @s[scores={c.trident=1..}] c.trident

