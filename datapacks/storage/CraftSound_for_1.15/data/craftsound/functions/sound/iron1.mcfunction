playsound minecraft:block.stone.break player @a[distance=..12] ~ ~ ~ 1 1.1

scoreboard players reset @s[scores={c.anvil=1..}] c.anvil
scoreboard players reset @s[scores={c.activator_rail=1..}] c.activator_rail
scoreboard players reset @s[scores={c.bell=1..}] c.bell
scoreboard players reset @s[scores={c.bucket=1..}] c.bucket
scoreboard players reset @s[scores={c.cauldron=1..}] c.cauldron
scoreboard players reset @s[scores={c.chipped_anvil=1..}] c.chipped_anvil
scoreboard players reset @s[scores={c.clock=1..}] c.clock
scoreboard players reset @s[scores={c.compass=1..}] c.compass
scoreboard players reset @s[scores={c.dandelion=1..}] c.dandelion
scoreboard players reset @s[scores={c.detector_rail=1..}] c.detector_rail
scoreboard players reset @s[scores={c.diamond=1..}] c.diamond
scoreboard players reset @s[scores={c.diamond_block=1..}] c.diamond_block
scoreboard players reset @s[scores={c.flint_and_stee=1..}] c.flint_and_stee
scoreboard players reset @s[scores={c.glowstone_dust=1..}] c.glowstone_dust
scoreboard players reset @s[scores={c.hopper=1..}] c.hopper
scoreboard players reset @s[scores={c.iron_bars=1..}] c.iron_bars
scoreboard players reset @s[scores={c.iron_block=1..}] c.iron_block
scoreboard players reset @s[scores={c.iron_door=1..}] c.iron_door
scoreboard players reset @s[scores={c.iron_ingot=1..}] c.iron_ingot
scoreboard players reset @s[scores={c.iron_nugget=1..}] c.iron_nugget
scoreboard players reset @s[scores={c.iron_trapdoor=1..}] c.iron_trapdoor
scoreboard players reset @s[scores={c.lantern=1..}] c.lantern
scoreboard players reset @s[scores={c.light_plate=1..}] c.light_plate
scoreboard players reset @s[scores={c.minecart=1..}] c.minecart
scoreboard players reset @s[scores={c.disc_1=1..}] c.disc_1
scoreboard players reset @s[scores={c.disc_2=1..}] c.disc_2
scoreboard players reset @s[scores={c.disc_3=1..}] c.disc_3
scoreboard players reset @s[scores={c.disc_4=1..}] c.disc_4
scoreboard players reset @s[scores={c.disc_5=1..}] c.disc_5
scoreboard players reset @s[scores={c.disc_6=1..}] c.disc_6
scoreboard players reset @s[scores={c.disc_7=1..}] c.disc_7
scoreboard players reset @s[scores={c.disc_8=1..}] c.disc_8
scoreboard players reset @s[scores={c.disc_9=1..}] c.disc_9
scoreboard players reset @s[scores={c.disc_10=1..}] c.disc_10
scoreboard players reset @s[scores={c.disc_11=1..}] c.disc_11
scoreboard players reset @s[scores={c.disc_12=1..}] c.disc_12
scoreboard players reset @s[scores={c.powered_rail=1..}] c.powered_rail
scoreboard players reset @s[scores={c.rail=1..}] c.rail
scoreboard players reset @s[scores={c.light_plate=1..}] c.light_plate
scoreboard players reset @s[scores={c.light_plate=1..}] c.light_plate



