playsound minecraft:block.slime_block.break player @a[distance=..12] ~ ~ ~ 1 1.3

scoreboard players reset @s[scores={c.honey_block=1..}] c.honey_block
scoreboard players reset @s[scores={c.honeycomb_bloc=1..}] c.honeycomb_bloc
scoreboard players reset @s[scores={c.slime_ball=1..}] c.slime_ball
scoreboard players reset @s[scores={c.slime_block=1..}] c.slime_block
scoreboard players reset @s[scores={c.sticky_piston=1..}] c.sticky_piston
