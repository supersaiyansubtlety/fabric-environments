playsound minecraft:block.gravel.break player @a[distance=..12] ~ ~ ~ 1 0.8

scoreboard players reset @s[scores={c.clay=1..}] c.clay
scoreboard players reset @s[scores={c.clay_ball=1..}] c.clay_ball
scoreboard players reset @s[scores={c.coarse_dirt=1..}] c.coarse_dirt
scoreboard players reset @s[scores={c.dirt=1..}] c.dirt
scoreboard players reset @s[scores={c.farmland=1..}] c.farmland
scoreboard players reset @s[scores={c.gravel=1..}] c.gravel
scoreboard players reset @s[scores={c.podzol=1..}] c.podzol

