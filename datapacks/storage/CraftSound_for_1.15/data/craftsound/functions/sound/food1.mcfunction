playsound minecraft:item.bottle.empty player @a[distance=..12] ~ ~ ~ 0.6 0.65
playsound minecraft:block.wooden_button.click_off player @a[distance=..12] ~ ~ ~ 1 0.6

scoreboard players reset @s[scores={c.beetroot_soup=1..}] c.beetroot_soup
scoreboard players reset @s[scores={c.honey_bottle=1..}] c.honey_bottle
scoreboard players reset @s[scores={c.mushroom_stew=1..}] c.mushroom_stew
scoreboard players reset @s[scores={c.rabbit_stew=1..}] c.rabbit_stew
scoreboard players reset @s[scores={c.suspiciousstew=1..}] c.suspiciousstew

