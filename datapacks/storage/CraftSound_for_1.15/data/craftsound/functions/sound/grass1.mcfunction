playsound minecraft:block.grass.break player @a[distance=..12] ~ ~ ~ 0.8 1.1

scoreboard players reset @s[scores={c.acacia_leaves=1..}] c.acacia_leaves
scoreboard players reset @s[scores={c.acacia_sapling=1..}] c.acacia_sapling
scoreboard players reset @s[scores={c.allium=1..}] c.allium
scoreboard players reset @s[scores={c.azure_bluet=1..}] c.azure_bluet
scoreboard players reset @s[scores={c.blue_orchid=1..}] c.blue_orchid
scoreboard players reset @s[scores={c.birch_leaves=1..}] c.birch_leaves
scoreboard players reset @s[scores={c.birch_sapling=1..}] c.birch_sapling
scoreboard players reset @s[scores={c.brain_coral=1..}] c.brain_coral
scoreboard players reset @s[scores={c.brain_coral_b=1..}] c.brain_coral_b
scoreboard players reset @s[scores={c.brain_coral_f=1..}] c.brain_coral_f
scoreboard players reset @s[scores={c.bubble_coral=1..}] c.brain_coral
scoreboard players reset @s[scores={c.bubble_coral_b=1..}] c.brain_coral_b
scoreboard players reset @s[scores={c.bubble_coral_f=1..}] c.brain_coral_f
scoreboard players reset @s[scores={c.cornflower=1..}] c.cornflower
scoreboard players reset @s[scores={c.dandelion=1..}] c.dandelion
scoreboard players reset @s[scores={c.dark_oak_leave=1..}] c.dark_oak_leave
scoreboard players reset @s[scores={c.dark_oak_sapli=1..}] c.dark_oak_sapli
scoreboard players reset @s[scores={c.dead_brain_co=1..}] c.dead_brain_co
scoreboard players reset @s[scores={c.dead_brain_cob=1..}] c.dead_brain_cob
scoreboard players reset @s[scores={c.dead_brain_c=1..}] c.dead_brain_c
scoreboard players reset @s[scores={c.dead_bubble_c=1..}] c.dead_bubble_c
scoreboard players reset @s[scores={c.dead_bubble_cb=1..}] c.dead_bubble_cb
scoreboard players reset @s[scores={c.dead_bubble_cf=1..}] c.dead_bubble_cf
scoreboard players reset @s[scores={c.dead_bush=1..}] c.dead_bush
scoreboard players reset @s[scores={c.dead_fire_cor=1..}] c.dead_fire_cor
scoreboard players reset @s[scores={c.dead_fire_corb=1..}] c.dead_fire_corb
scoreboard players reset @s[scores={c.dead_fire_corf=1..}] c.dead_fire_corf
scoreboard players reset @s[scores={c.dead_horn_cor=1..}] c.dead_horn_cor
scoreboard players reset @s[scores={c.dead_horn_corb=1..}] c.dead_horn_corb
scoreboard players reset @s[scores={c.dead_horn_corf=1..}] c.dead_horn_corf
scoreboard players reset @s[scores={c.dead_tube_cor=1..}] c.dead_tube_cor
scoreboard players reset @s[scores={c.dead_tube_corb=1..}] c.dead_tube_corb
scoreboard players reset @s[scores={c.dead_tube_corf=1..}] c.dead_tube_corf
scoreboard players reset @s[scores={c.dead_tube_corf=1..}] c.fire_coral
scoreboard players reset @s[scores={c.dead_tube_corf=1..}] c.fire_coral_b
scoreboard players reset @s[scores={c.dead_tube_corf=1..}] c.fire_coral_f
scoreboard players reset @s[scores={c.grass=1..}] c.grass
scoreboard players reset @s[scores={c.grass_block=1..}] c.grass_block
scoreboard players reset @s[scores={c.grass_path=1..}] c.grass_path
scoreboard players reset @s[scores={c.horn_coral=1..}] c.horn_coral
scoreboard players reset @s[scores={c.horn_coral_b=1..}] c.horn_coral_b
scoreboard players reset @s[scores={c.horn_coral_f=1..}] c.horn_coral_f
scoreboard players reset @s[scores={c.jungle_leaves=1..}] c.jungle_leaves
scoreboard players reset @s[scores={c.jungle_sapling=1..}] c.jungle_sapling
scoreboard players reset @s[scores={c.large_fern=1..}] c.large_fern
scoreboard players reset @s[scores={c.lilac=1..}] c.lilac
scoreboard players reset @s[scores={c.lily=1..}] c.lily
scoreboard players reset @s[scores={c.mycelium=1..}] c.mycelium
scoreboard players reset @s[scores={c.oak_leaves=1..}] c.oak_leaves
scoreboard players reset @s[scores={c.oak_sapling=1..}] c.oak_sapling
scoreboard players reset @s[scores={c.orange_tulip=1..}] c.orange_tulip
scoreboard players reset @s[scores={c.oxeye_daisy=1..}] c.oxeye_daisy
scoreboard players reset @s[scores={c.peony=1..}] c.peony
scoreboard players reset @s[scores={c.pink_tulip=1..}] c.pink_tulip
scoreboard players reset @s[scores={c.poppy=1..}] c.poppy
scoreboard players reset @s[scores={c.red_tulip=1..}] c.red_tulip
scoreboard players reset @s[scores={c.rose_bush=1..}] c.rose_bush
scoreboard players reset @s[scores={c.sponge=1..}] c.sponge
scoreboard players reset @s[scores={c.spru_leaves=1..}] c.spru_leaves
scoreboard players reset @s[scores={c.spru_sapling=1..}] c.spru_sapling
scoreboard players reset @s[scores={c.sunflower=1..}] c.sunflower
scoreboard players reset @s[scores={c.tall_grass=1..}] c.tall_grass
scoreboard players reset @s[scores={c.tube_coral=1..}] c.tube_coral
scoreboard players reset @s[scores={c.tube_coral_b=1..}] c.tube_coral_b
scoreboard players reset @s[scores={c.tube_coral_f=1..}] c.tube_coral_f
scoreboard players reset @s[scores={c.vine=1..}] c.vine
scoreboard players reset @s[scores={c.white_tulip=1..}] c.white_tulip
scoreboard players reset @s[scores={c.wither_rose=1..}] c.wither_rose






