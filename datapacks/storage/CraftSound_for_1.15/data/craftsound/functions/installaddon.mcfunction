scoreboard objectives add c.acacia_leaves minecraft.crafted:minecraft.acacia_leaves
scoreboard objectives add c.acacia_log minecraft.crafted:minecraft.acacia_log
scoreboard objectives add c.acacia_sapling minecraft.crafted:minecraft.acacia_sapling
scoreboard objectives add c.allium minecraft.crafted:minecraft.allium
scoreboard objectives add c.apple minecraft.crafted:minecraft.apple
scoreboard objectives add c.azure_bluet minecraft.crafted:minecraft.azure_bluet
scoreboard objectives add c.baked_potato minecraft.crafted:minecraft.baked_potato
scoreboard objectives add c.bamboo minecraft.crafted:minecraft.bamboo
scoreboard objectives add c.barrier minecraft.crafted:minecraft.barrier
scoreboard objectives add c.bat_egg minecraft.crafted:minecraft.bat_spawn_egg
scoreboard objectives add c.bedrock minecraft.crafted:minecraft.bedrock
scoreboard objectives add c.bee_nest minecraft.crafted:minecraft.bee_nest
scoreboard objectives add c.bee_egg minecraft.crafted:minecraft.bee_spawn_egg
scoreboard objectives add c.beef minecraft.crafted:minecraft.beef
scoreboard objectives add c.beetroot minecraft.crafted:minecraft.beetroot
scoreboard objectives add c.beetroot_seeds minecraft.crafted:minecraft.beetroot_seeds
scoreboard objectives add c.bell minecraft.crafted:minecraft.bell
scoreboard objectives add c.birch_leaves minecraft.crafted:minecraft.birch_leaves
scoreboard objectives add c.birch_log minecraft.crafted:minecraft.birch_log
scoreboard objectives add c.birch_sapling minecraft.crafted:minecraft.birch_sapling
scoreboard objectives add c.black_gterraco minecraft.crafted:minecraft.black_glazed_terracotta
scoreboard objectives add c.black_box minecraft.crafted:minecraft.black_shulker_box
scoreboard objectives add c.blaze_rod minecraft.crafted:minecraft.blaze_rod
scoreboard objectives add c.blue_gterracot minecraft.crafted:minecraft.blue_glazed_terracotta
scoreboard objectives add c.blue_box minecraft.crafted:minecraft.blue_shulker_box
scoreboard objectives add c.blue_orchid minecraft.crafted:minecraft.blue_orchid
scoreboard objectives add c.bone minecraft.crafted:minecraft.bone
scoreboard objectives add c.brain_coral minecraft.crafted:minecraft.brain_coral
scoreboard objectives add c.brain_coral_b minecraft.crafted:minecraft.brain_coral_block
scoreboard objectives add c.brain_coral_f minecraft.crafted:minecraft.brain_coral_fan
scoreboard objectives add c.brick minecraft.crafted:minecraft.brick
scoreboard objectives add c.brown_gterraco minecraft.crafted:minecraft.brown_glazed_terracotta
scoreboard objectives add c.brown_mushroom minecraft.crafted:minecraft.brown_mushroom
scoreboard objectives add c.brown_mushbock minecraft.crafted:minecraft.brown_mushroom_block
scoreboard objectives add c.brown_box minecraft.crafted:minecraft.brown_shulker_box
scoreboard objectives add c.bubble_coral minecraft.crafted:minecraft.bubble_coral
scoreboard objectives add c.bubble_coral_b minecraft.crafted:minecraft.bubble_coral_block
scoreboard objectives add c.bubble_coral_f minecraft.crafted:minecraft.bubble_coral_fan
scoreboard objectives add c.cactus minecraft.crafted:minecraft.cactus
scoreboard objectives add c.carrot minecraft.crafted:minecraft.carrot
scoreboard objectives add c.carved_pumpkin minecraft.crafted:minecraft.carved_pumpkin
scoreboard objectives add c.cat_egg minecraft.crafted:minecraft.cat_spawn_egg
scoreboard objectives add c.cave_egg minecraft.crafted:minecraft.cave_spider_spawn_egg
scoreboard objectives add c.chain_block minecraft.crafted:minecraft.chain_command_block
scoreboard objectives add c.chain_boots minecraft.crafted:minecraft.chainmail_boots
scoreboard objectives add c.chain_chest minecraft.crafted:minecraft.chainmail_chestplate
scoreboard objectives add c.chain_helmet minecraft.crafted:minecraft.chainmail_helmet
scoreboard objectives add c.chain_leg minecraft.crafted:minecraft.chainmail_leggings
scoreboard objectives add c.charcoal minecraft.crafted:minecraft.charcoal
scoreboard objectives add c.chicken minecraft.crafted:minecraft.chicken
scoreboard objectives add c.chicken_egg minecraft.crafted:minecraft.chicken_spawn_egg
scoreboard objectives add c.chipped_anvil minecraft.crafted:minecraft.chipped_anvil
scoreboard objectives add c.chorus_flower minecraft.crafted:minecraft.chorus_flower
scoreboard objectives add c.chorus_fruit minecraft.crafted:minecraft.chorus_fruit
scoreboard objectives add c.chorus_plant minecraft.crafted:minecraft.chorus_plant
scoreboard objectives add c.clay_ball minecraft.crafted:minecraft.clay_ball
scoreboard objectives add c.coal_ore minecraft.crafted:minecraft.coal_ore
scoreboard objectives add c.cobweb minecraft.crafted:minecraft.cobweb
scoreboard objectives add c.cocoa_beans minecraft.crafted:minecraft.cocoa_beans
scoreboard objectives add c.cod_bucket minecraft.crafted:minecraft.cod_bucket
scoreboard objectives add c.cod_egg minecraft.crafted:minecraft.cod_spawn_egg
scoreboard objectives add c.command_block minecraft.crafted:minecraft.command_block
scoreboard objectives add c.command_mineca minecraft.crafted:minecraft.command_block_minecart
scoreboard objectives add c.cook_beef minecraft.crafted:minecraft.cooked_beef
scoreboard objectives add c.cook_chicken minecraft.crafted:minecraft.cooked_chicken
scoreboard objectives add c.cook_cod minecraft.crafted:minecraft.cooked_cod
scoreboard objectives add c.cook_mutton minecraft.crafted:minecraft.cooked_mutton
scoreboard objectives add c.cook_porkchop minecraft.crafted:minecraft.cooked_porkchop
scoreboard objectives add c.cook_rabbit minecraft.crafted:minecraft.cooked_rabbit
scoreboard objectives add c.cook_salmon minecraft.crafted:minecraft.cooked_salmon
scoreboard objectives add c.cornflower minecraft.crafted:minecraft.cornflower
scoreboard objectives add c.cow_egg minecraft.crafted:minecraft.cow_spawn_egg
scoreboard objectives add c.creeper_head minecraft.crafted:minecraft.creeper_head
scoreboard objectives add c.creeper_egg minecraft.crafted:minecraft.creeper_spawn_egg
scoreboard objectives add c.cyan_gterracot minecraft.crafted:minecraft.cyan_glazed_terracotta
scoreboard objectives add c.cyan_box minecraft.crafted:minecraft.cyan_shulker_box
scoreboard objectives add c.damaged_anvil minecraft.crafted:minecraft.damaged_anvil
scoreboard objectives add c.dandelion minecraft.crafted:minecraft.dandelion
scoreboard objectives add c.dark_oak_leave minecraft.crafted:minecraft.dark_oak_leaves
scoreboard objectives add c.dark_oak_log minecraft.crafted:minecraft.dark_oak_log
scoreboard objectives add c.dark_oak_sapli minecraft.crafted:minecraft.dark_oak_sapling
scoreboard objectives add c.dead_brain_co minecraft.crafted:minecraft.dead_brain_coral
scoreboard objectives add c.dead_brain_cob minecraft.crafted:minecraft.dead_brain_coral_block
scoreboard objectives add c.dead_brain_cof minecraft.crafted:minecraft.dead_brain_coral_fan
scoreboard objectives add c.dead_bubble_c minecraft.crafted:minecraft.dead_bubble_coral
scoreboard objectives add c.dead_bubble_cb minecraft.crafted:minecraft.dead_bubble_coral_block
scoreboard objectives add c.dead_bubble_cf minecraft.crafted:minecraft.dead_bubble_coral_fan
scoreboard objectives add c.dead_bush minecraft.crafted:minecraft.dead_bush
scoreboard objectives add c.dead_fire_cor minecraft.crafted:minecraft.dead_fire_coral
scoreboard objectives add c.dead_fire_corb minecraft.crafted:minecraft.dead_fire_coral_block
scoreboard objectives add c.dead_fire_corf minecraft.crafted:minecraft.dead_fire_coral_fan
scoreboard objectives add c.dead_horn_cor minecraft.crafted:minecraft.dead_horn_coral
scoreboard objectives add c.dead_horn_corb minecraft.crafted:minecraft.dead_horn_coral_block
scoreboard objectives add c.dead_horn_corf minecraft.crafted:minecraft.dead_horn_coral_fan
scoreboard objectives add c.dead_tube_cor minecraft.crafted:minecraft.dead_tube_coral
scoreboard objectives add c.dead_tube_corb minecraft.crafted:minecraft.dead_tube_coral_block
scoreboard objectives add c.dead_tube_corf minecraft.crafted:minecraft.dead_tube_coral_fan
scoreboard objectives add c.diamond_horse minecraft.crafted:minecraft.diamond_horse_armor
scoreboard objectives add c.diamond_ore minecraft.crafted:minecraft.diamond_ore
scoreboard objectives add c.dirt minecraft.crafted:minecraft.dirt
scoreboard objectives add c.dolphin_egg minecraft.crafted:minecraft.dolphin_spawn_egg
scoreboard objectives add c.donkey_egg minecraft.crafted:minecraft.donkey_spawn_egg
scoreboard objectives add c.dragon_breath minecraft.crafted:minecraft.dragon_breath
scoreboard objectives add c.dragon_egg minecraft.crafted:minecraft.dragon_egg
scoreboard objectives add c.dragon_head minecraft.crafted:minecraft.dragon_head
scoreboard objectives add c.egg minecraft.crafted:minecraft.egg
scoreboard objectives add c.elder_egg minecraft.crafted:minecraft.elder_guardian_spawn_egg
scoreboard objectives add c.elytra minecraft.crafted:minecraft.elytra
scoreboard objectives add c.emerald_ore minecraft.crafted:minecraft.emerald_ore
scoreboard objectives add c.enchanted_book minecraft.crafted:minecraft.enchanted_book
scoreboard objectives add c.en_gold_apple minecraft.crafted:minecraft.enchanted_golden_apple
scoreboard objectives add c.end_portal minecraft.crafted:minecraft.end_portal_frame
scoreboard objectives add c.end_stone minecraft.crafted:minecraft.end_stone
scoreboard objectives add c.enderman_egg minecraft.crafted:minecraft.enderman_spawn_egg
scoreboard objectives add c.endermite_egg minecraft.crafted:minecraft.endermite_spawn_egg
scoreboard objectives add c.evoker_egg minecraft.crafted:minecraft.evoker_spawn_egg
scoreboard objectives add c.xp_bottle minecraft.crafted:minecraft.experience_bottle
scoreboard objectives add c.farmland minecraft.crafted:minecraft.farmland
scoreboard objectives add c.feather minecraft.crafted:minecraft.feather
scoreboard objectives add c.fern minecraft.crafted:minecraft.fern
scoreboard objectives add c.filled_map minecraft.crafted:minecraft.filled_map
scoreboard objectives add c.fire_coral minecraft.crafted:minecraft.fire_coral
scoreboard objectives add c.fire_coral_b minecraft.crafted:minecraft.fire_coral_block
scoreboard objectives add c.fire_coral_f minecraft.crafted:minecraft.fire_coral_fan
scoreboard objectives add c.firework minecraft.crafted:minecraft.firework_rocket
scoreboard objectives add c.firework_star minecraft.crafted:minecraft.firework_star
scoreboard objectives add c.flint minecraft.crafted:minecraft.flint
scoreboard objectives add c.fox_egg minecraft.crafted:minecraft.fox_spawn_egg
scoreboard objectives add c.ghast_egg minecraft.crafted:minecraft.ghast_spawn_egg
scoreboard objectives add c.ghast_tear minecraft.crafted:minecraft.ghast_tear
scoreboard objectives add c.glowstone_dust minecraft.crafted:minecraft.glowstone_dust
scoreboard objectives add c.gold_ore minecraft.crafted:minecraft.gold_ore
scoreboard objectives add c.gold_horse minecraft.crafted:minecraft.golden_horse_armor
scoreboard objectives add c.grass minecraft.crafted:minecraft.grass
scoreboard objectives add c.grass_block minecraft.crafted:minecraft.grass_block
scoreboard objectives add c.grass_path minecraft.crafted:minecraft.grass_path
scoreboard objectives add c.gravel minecraft.crafted:minecraft.gravel
scoreboard objectives add c.gray_gterracot minecraft.crafted:minecraft.gray_glazed_terracotta
scoreboard objectives add c.gray_box minecraft.crafted:minecraft.gray_shulker_box
scoreboard objectives add c.green_gterraco minecraft.crafted:minecraft.green_glazed_terracotta
scoreboard objectives add c.green_box minecraft.crafted:minecraft.green_shulker_box
scoreboard objectives add c.guardian_egg minecraft.crafted:minecraft.guardian_spawn_egg
scoreboard objectives add c.gunpowder minecraft.crafted:minecraft.gunpowder
scoreboard objectives add c.heart_sea minecraft.crafted:minecraft.heart_of_the_sea
scoreboard objectives add c.honey_bottle minecraft.crafted:minecraft.honey_bottle
scoreboard objectives add c.honeycomb minecraft.crafted:minecraft.honeycomb
scoreboard objectives add c.horn_coral minecraft.crafted:minecraft.horn_coral
scoreboard objectives add c.horn_coral_b minecraft.crafted:minecraft.horn_coral_block
scoreboard objectives add c.horn_coral_f minecraft.crafted:minecraft.horn_coral_fan
scoreboard objectives add c.horse_egg minecraft.crafted:minecraft.horse_spawn_egg
scoreboard objectives add c.husk_egg minecraft.crafted:minecraft.husk_spawn_egg
scoreboard objectives add c.infested_1 minecraft.crafted:minecraft.infested_chiseled_stone_bricks
scoreboard objectives add c.infested_2 minecraft.crafted:minecraft.infested_cobblestone
scoreboard objectives add c.infested_3 minecraft.crafted:minecraft.infested_cracked_stone_bricks
scoreboard objectives add c.infested_4 minecraft.crafted:minecraft.infested_mossy_stone_bricks
scoreboard objectives add c.infested_5 minecraft.crafted:minecraft.infested_stone
scoreboard objectives add c.infested_6 minecraft.crafted:minecraft.infested_stone_bricks
scoreboard objectives add c.ink_sac minecraft.crafted:minecraft.ink_sac
scoreboard objectives add c.ice minecraft.crafted:minecraft.ice
scoreboard objectives add c.iron_ore minecraft.crafted:minecraft.iron_ore
scoreboard objectives add c.jungle_leaves minecraft.crafted:minecraft.jungle_leaves
scoreboard objectives add c.jungle_log minecraft.crafted:minecraft.jungle_log
scoreboard objectives add c.jungle_sapling minecraft.crafted:minecraft.jungle_sapling
scoreboard objectives add c.kelp minecraft.crafted:minecraft.kelp
scoreboard objectives add c.lapis_ore minecraft.crafted:minecraft.lapis_ore
scoreboard objectives add c.large_fern minecraft.crafted:minecraft.large_fern
scoreboard objectives add c.lava_bucket minecraft.crafted:minecraft.lava_bucket
scoreboard objectives add c.lblue_gterraco minecraft.crafted:minecraft.light_blue_glazed_terracotta
scoreboard objectives add c.lblue_box minecraft.crafted:minecraft.light_blue_shulker_box
scoreboard objectives add c.lgray_gterraco minecraft.crafted:minecraft.light_gray_glazed_terracotta
scoreboard objectives add c.lgray_box minecraft.crafted:minecraft.light_gray_shulker_box
scoreboard objectives add c.lilac minecraft.crafted:minecraft.lilac
scoreboard objectives add c.lily minecraft.crafted:minecraft.lily_of_the_valley
scoreboard objectives add c.lily_pad minecraft.crafted:minecraft.lily_pad
scoreboard objectives add c.lime_gterracot minecraft.crafted:minecraft.lime_glazed_terracotta
scoreboard objectives add c.lime_box minecraft.crafted:minecraft.lime_shulker_box
scoreboard objectives add c.linger_potion minecraft.crafted:minecraft.lingering_potion
scoreboard objectives add c.llama_egg minecraft.crafted:minecraft.llama_spawn_egg
scoreboard objectives add c.magenta_gterra minecraft.crafted:minecraft.magenta_glazed_terracotta
scoreboard objectives add c.magenta_box minecraft.crafted:minecraft.magenta_shulker_box
scoreboard objectives add c.magma_spawn minecraft.crafted:minecraft.magma_cube_spawn_egg
scoreboard objectives add c.melon_slice minecraft.crafted:minecraft.melon_slice
scoreboard objectives add c.milk minecraft.crafted:minecraft.milk_bucket
scoreboard objectives add c.mooshroom_egg minecraft.crafted:minecraft.mooshroom_spawn_egg
scoreboard objectives add c.mule_egg minecraft.crafted:minecraft.mule_spawn_egg
scoreboard objectives add c.mushroom_stem minecraft.crafted:minecraft.mushroom_stem
scoreboard objectives add c.disc_1 minecraft.crafted:minecraft.music_disc_11
scoreboard objectives add c.disc_2 minecraft.crafted:minecraft.music_disc_13
scoreboard objectives add c.disc_3 minecraft.crafted:minecraft.music_disc_blocks
scoreboard objectives add c.disc_4 minecraft.crafted:minecraft.music_disc_cat
scoreboard objectives add c.disc_5 minecraft.crafted:minecraft.music_disc_chirp
scoreboard objectives add c.disc_6 minecraft.crafted:minecraft.music_disc_far
scoreboard objectives add c.disc_7 minecraft.crafted:minecraft.music_disc_mall
scoreboard objectives add c.disc_8 minecraft.crafted:minecraft.music_disc_mellohi
scoreboard objectives add c.disc_9 minecraft.crafted:minecraft.music_disc_stal
scoreboard objectives add c.disc_10 minecraft.crafted:minecraft.music_disc_strad
scoreboard objectives add c.disc_11 minecraft.crafted:minecraft.music_disc_wait
scoreboard objectives add c.disc_12 minecraft.crafted:minecraft.music_disc_ward
scoreboard objectives add c.mutton minecraft.crafted:minecraft.mutton
scoreboard objectives add c.mycelium minecraft.crafted:minecraft.mycelium
scoreboard objectives add c.name_tag minecraft.crafted:minecraft.name_tag
scoreboard objectives add c.nautilus minecraft.crafted:minecraft.nautilus_shell
scoreboard objectives add c.nether_brick minecraft.crafted:minecraft.nether_brick
scoreboard objectives add c.nether_qua_ore minecraft.crafted:minecraft.nether_quartz_ore
scoreboard objectives add c.nether_star minecraft.crafted:minecraft.nether_star
scoreboard objectives add c.nether_wart minecraft.crafted:minecraft.nether_wart
scoreboard objectives add c.netherrack minecraft.crafted:minecraft.netherrack
scoreboard objectives add c.oak_leaves minecraft.crafted:minecraft.oak_leaves
scoreboard objectives add c.oak_log minecraft.crafted:minecraft.oak_log
scoreboard objectives add c.oak_sapling minecraft.crafted:minecraft.oak_sapling
scoreboard objectives add c.obsidian minecraft.crafted:minecraft.obsidian
scoreboard objectives add c.ocelot_egg minecraft.crafted:minecraft.ocelot_spawn_egg
scoreboard objectives add c.orange_gterrac minecraft.crafted:minecraft.orange_glazed_terracotta
scoreboard objectives add c.orange_box minecraft.crafted:minecraft.orange_shulker_box
scoreboard objectives add c.orange_tulip minecraft.crafted:minecraft.orange_tulip
scoreboard objectives add c.oxeye_daisy minecraft.crafted:minecraft.oxeye_daisy
scoreboard objectives add c.panda_egg minecraft.crafted:minecraft.panda_spawn_egg
scoreboard objectives add c.parrot_egg minecraft.crafted:minecraft.parrot_spawn_egg
scoreboard objectives add c.peony minecraft.crafted:minecraft.peony
scoreboard objectives add c.phantom_membra minecraft.crafted:minecraft.phantom_membrane
scoreboard objectives add c.phantom_egg minecraft.crafted:minecraft.phantom_spawn_egg
scoreboard objectives add c.pig_egg minecraft.crafted:minecraft.pig_spawn_egg
scoreboard objectives add c.pillager_egg minecraft.crafted:minecraft.pillager_spawn_egg
scoreboard objectives add c.pink_gterracot minecraft.crafted:minecraft.pink_glazed_terracotta
scoreboard objectives add c.pink_box minecraft.crafted:minecraft.pink_shulker_box
scoreboard objectives add c.pink_tulip minecraft.crafted:minecraft.pink_tulip
scoreboard objectives add c.pink_wool minecraft.crafted:minecraft.pink_wool
scoreboard objectives add c.player_head minecraft.crafted:minecraft.player_head
scoreboard objectives add c.podzol minecraft.crafted:minecraft.podzol
scoreboard objectives add c.bad_potato minecraft.crafted:minecraft.poisonous_potato
scoreboard objectives add c.bear_egg minecraft.crafted:minecraft.polar_bear_spawn_egg
scoreboard objectives add c.popp_chorus minecraft.crafted:minecraft.popped_chorus_fruit
scoreboard objectives add c.poppy minecraft.crafted:minecraft.poppy
scoreboard objectives add c.porkchop minecraft.crafted:minecraft.porkchop
scoreboard objectives add c.potato minecraft.crafted:minecraft.potato
scoreboard objectives add c.potion minecraft.crafted:minecraft.potion
scoreboard objectives add c.pufferfish minecraft.crafted:minecraft.pufferfish
scoreboard objectives add c.pufferfish_buc minecraft.crafted:minecraft.pufferfish_bucket
scoreboard objectives add c.pufferfish_egg minecraft.crafted:minecraft.pufferfish_spawn_egg
scoreboard objectives add c.pumpkin minecraft.crafted:minecraft.pumpkin
scoreboard objectives add c.purple_gterrac minecraft.crafted:minecraft.purple_glazed_terracotta
scoreboard objectives add c.purple_box minecraft.crafted:minecraft.purple_shulker_box
scoreboard objectives add c.rabbit minecraft.crafted:minecraft.rabbit
scoreboard objectives add c.rabbit_foot minecraft.crafted:minecraft.rabbit_foot
scoreboard objectives add c.rabbit_hide minecraft.crafted:minecraft.rabbit_hide
scoreboard objectives add c.rabbit_egg minecraft.crafted:minecraft.rabbit_spawn_egg
scoreboard objectives add c.rabbit_stew minecraft.crafted:minecraft.rabbit_stew
scoreboard objectives add c.ravager_egg minecraft.crafted:minecraft.ravager_spawn_egg
scoreboard objectives add c.red_gterracot minecraft.crafted:minecraft.red_glazed_terracotta
scoreboard objectives add c.red_mushroom minecraft.crafted:minecraft.red_mushroom
scoreboard objectives add c.red_mushroom_b minecraft.crafted:minecraft.red_mushroom_block
scoreboard objectives add c.red_box minecraft.crafted:minecraft.red_shulker_box
scoreboard objectives add c.red_tulip minecraft.crafted:minecraft.red_tulip
scoreboard objectives add c.redstone_ore minecraft.crafted:minecraft.redstone_ore
scoreboard objectives add c.repeat_command minecraft.crafted:minecraft.repeating_command_block
scoreboard objectives add c.rose_bush minecraft.crafted:minecraft.rose_bush
scoreboard objectives add c.rotten_flesh minecraft.crafted:minecraft.rotten_flesh
scoreboard objectives add c.saddle minecraft.crafted:minecraft.saddle
scoreboard objectives add c.salmon minecraft.crafted:minecraft.salmon
scoreboard objectives add c.salmon_bucket minecraft.crafted:minecraft.salmon_bucket
scoreboard objectives add c.salmon_egg minecraft.crafted:minecraft.salmon_spawn_egg
scoreboard objectives add c.sand minecraft.crafted:minecraft.sand
scoreboard objectives add c.scute minecraft.crafted:minecraft.scute
scoreboard objectives add c.sea_pickle minecraft.crafted:minecraft.sea_pickle
scoreboard objectives add c.seagrass minecraft.crafted:minecraft.seagrass
scoreboard objectives add c.sheep_egg minecraft.crafted:minecraft.sheep_spawn_egg
scoreboard objectives add c.shulker_shell minecraft.crafted:minecraft.shulker_shell
scoreboard objectives add c.shulker_egg minecraft.crafted:minecraft.shulker_spawn_egg
scoreboard objectives add c.silverfish_egg minecraft.crafted:minecraft.silverfish_spawn_egg
scoreboard objectives add c.skeleton_skull minecraft.crafted:minecraft.skeleton_skull
scoreboard objectives add c.skeleton_egg minecraft.crafted:minecraft.skeleton_spawn_egg
scoreboard objectives add c.skull_banner minecraft.crafted:minecraft.skull_banner_pattern
scoreboard objectives add c.slime_egg minecraft.crafted:minecraft.slime_spawn_egg
scoreboard objectives add c.smoh_ston minecraft.crafted:minecraft.smooth_stone
scoreboard objectives add c.soul_sand minecraft.crafted:minecraft.soul_sand
scoreboard objectives add c.spawner minecraft.crafted:minecraft.spawner
scoreboard objectives add c.spider_eye minecraft.crafted:minecraft.spider_eye
scoreboard objectives add c.spider_egg minecraft.crafted:minecraft.spider_spawn_egg
scoreboard objectives add c.splash_potion minecraft.crafted:minecraft.splash_potion
scoreboard objectives add c.sponge minecraft.crafted:minecraft.sponge
scoreboard objectives add c.spru_leaves minecraft.crafted:minecraft.spruce_leaves
scoreboard objectives add c.spru_log minecraft.crafted:minecraft.spruce_log
scoreboard objectives add c.spru_sapling minecraft.crafted:minecraft.spruce_sapling
scoreboard objectives add c.squid_egg minecraft.crafted:minecraft.squid_spawn_egg
scoreboard objectives add c.stray_egg minecraft.crafted:minecraft.stray_spawn_egg
scoreboard objectives add c.string minecraft.crafted:minecraft.string
scoreboard objectives add c.stip_1_log minecraft.crafted:minecraft.stripped_acacia_log
scoreboard objectives add c.stip_1_wood minecraft.crafted:minecraft.stripped_acacia_wood
scoreboard objectives add c.stip_2_log minecraft.crafted:minecraft.stripped_birch_log
scoreboard objectives add c.stip_2_wood minecraft.crafted:minecraft.stripped_birch_wood
scoreboard objectives add c.stip_3_log minecraft.crafted:minecraft.stripped_dark_oak_log
scoreboard objectives add c.stip_3_wood minecraft.crafted:minecraft.stripped_dark_oak_wood
scoreboard objectives add c.stip_4_log minecraft.crafted:minecraft.stripped_jungle_log
scoreboard objectives add c.stip_4_wood minecraft.crafted:minecraft.stripped_jungle_wood
scoreboard objectives add c.stip_5_log minecraft.crafted:minecraft.stripped_oak_log
scoreboard objectives add c.stip_5_wood minecraft.crafted:minecraft.stripped_oak_wood
scoreboard objectives add c.stip_6_log minecraft.crafted:minecraft.stripped_spruce_log
scoreboard objectives add c.stip_6_wood minecraft.crafted:minecraft.stripped_spruce_wood
scoreboard objectives add c.structure_bloc minecraft.crafted:minecraft.structure_block
scoreboard objectives add c.structure_void minecraft.crafted:minecraft.structure_void
scoreboard objectives add c.sugar_cane minecraft.crafted:minecraft.sugar_cane
scoreboard objectives add c.sunflower minecraft.crafted:minecraft.sunflower
scoreboard objectives add c.sweet_berries minecraft.crafted:minecraft.sweet_berries
scoreboard objectives add c.tall_grass minecraft.crafted:minecraft.tall_grass
scoreboard objectives add c.terracotta minecraft.crafted:minecraft.terracotta
scoreboard objectives add c.tipp_arrow minecraft.crafted:minecraft.tipped_arrow
scoreboard objectives add c.totem minecraft.crafted:minecraft.totem_of_undying
scoreboard objectives add c.trade_lama_egg minecraft.crafted:minecraft.trader_llama_spawn_egg
scoreboard objectives add c.trident minecraft.crafted:minecraft.trident
scoreboard objectives add c.trop_fish minecraft.crafted:minecraft.tropical_fish
scoreboard objectives add c.trop_fish_buck minecraft.crafted:minecraft.tropical_fish_bucket
scoreboard objectives add c.trop_fish_egg minecraft.crafted:minecraft.tropical_fish_spawn_egg
scoreboard objectives add c.tube_coral minecraft.crafted:minecraft.tube_coral
scoreboard objectives add c.tube_coral_b minecraft.crafted:minecraft.tube_coral_block
scoreboard objectives add c.tube_coral_f minecraft.crafted:minecraft.tube_coral_fan
scoreboard objectives add c.turtle_egg minecraft.crafted:minecraft.turtle_egg
scoreboard objectives add c.turtle_s_egg minecraft.crafted:minecraft.turtle_spawn_egg
scoreboard objectives add c.vex_egg minecraft.crafted:minecraft.vex_spawn_egg
scoreboard objectives add c.villager_egg minecraft.crafted:minecraft.villager_spawn_egg
scoreboard objectives add c.vindicator_egg minecraft.crafted:minecraft.vindicator_spawn_egg
scoreboard objectives add c.vine minecraft.crafted:minecraft.vine
scoreboard objectives add c.trader_egg minecraft.crafted:minecraft.wandering_trader_spawn_egg
scoreboard objectives add c.water_bucket minecraft.crafted:minecraft.water_bucket
scoreboard objectives add c.wet_sponge minecraft.crafted:minecraft.wet_sponge
scoreboard objectives add c.seeds minecraft.crafted:minecraft.wheat_seeds
scoreboard objectives add c.white_gterraco minecraft.crafted:minecraft.white_glazed_terracotta
scoreboard objectives add c.white_box minecraft.crafted:minecraft.white_shulker_box
scoreboard objectives add c.white_tulip minecraft.crafted:minecraft.white_tulip
scoreboard objectives add c.white_wool minecraft.crafted:minecraft.white_wool
scoreboard objectives add c.witch_egg minecraft.crafted:minecraft.witch_spawn_egg
scoreboard objectives add c.wither_rose minecraft.crafted:minecraft.wither_rose
scoreboard objectives add c.wither_skull minecraft.crafted:minecraft.wither_skeleton_skull
scoreboard objectives add c.wither_egg minecraft.crafted:minecraft.wither_skeleton_spawn_egg
scoreboard objectives add c.wolf_egg minecraft.crafted:minecraft.wolf_spawn_egg
scoreboard objectives add c.written_book minecraft.crafted:minecraft.written_book
scoreboard objectives add c.yellow_gterrac minecraft.crafted:minecraft.yellow_glazed_terracotta
scoreboard objectives add c.yellow_box minecraft.crafted:minecraft.yellow_shulker_box
scoreboard objectives add c.zombie_head minecraft.crafted:minecraft.zombie_head
scoreboard objectives add c.zhorse_egg minecraft.crafted:minecraft.zombie_horse_spawn_egg
scoreboard objectives add c.pigman_egg minecraft.crafted:minecraft.zombie_pigman_spawn_egg
scoreboard objectives add c.zombie_egg minecraft.crafted:minecraft.zombie_spawn_egg
scoreboard objectives add c.zvillager_egg minecraft.crafted:minecraft.zombie_villager_spawn_egg
tellraw @a [{"text":"[CraftSound] ","color":"gold"},{"text":"successfully installed addon.","color":"green"}]