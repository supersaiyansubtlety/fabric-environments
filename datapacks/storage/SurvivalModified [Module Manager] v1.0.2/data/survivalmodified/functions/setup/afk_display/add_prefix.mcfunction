# If called, set Setup to "complete"
scoreboard players set #sm_base_afk_team_prefix SurvivalModified 1

execute if score #sm_base_afk_prefix_color SurvivalModified matches 1 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"aqua"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 2 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"black"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 3 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"blue"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 4 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_aqua"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 5 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_blue"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 6 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_gray"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 7 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_green"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 8 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_purple"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 9 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_red"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 10 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"gold"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 11 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"gray"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 12 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"green"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 13 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"light_purple"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 14 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"red"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 15 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"white"}
execute if score #sm_base_afk_prefix_color SurvivalModified matches 16 run team modify sm_team_afk prefix {"text":" [AFK] ","color":"yellow"}

# Message
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Players will have a prefix.","color":"green"}]
