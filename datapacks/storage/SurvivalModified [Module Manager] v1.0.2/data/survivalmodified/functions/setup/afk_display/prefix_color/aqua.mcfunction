# If called, set Setup to "complete"
scoreboard players set #sm_base_afk_prefix_color SurvivalModified 1
team modify sm_team_afk prefix {"text":" [AFK] ","color":"aqua"}

#Message
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Prefix: ","color":"green"},{"text":"[AFK]","color":"aqua"},{"text":".","color":"green"}]
