# load anchor chunk

#summon AEC
execute in minecraft:the_nether run summon minecraft:area_effect_cloud 0 0 0 {Age:-2147483648,Duration:-1,WaitTime:-2147483648,Tags:["dan.cra_find_cra_chunk","global.ignore"]}

#give stored Anchor's Pos
execute in minecraft:the_nether run data modify entity @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_cra_chunk,limit=1] Pos set from storage dan.cra_pos Pos

#forceload chunk at AEC
execute in minecraft:the_nether at @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_cra_chunk,limit=1] run forceload add ~ ~ ~ ~

#kill AEC
kill @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_cra_chunk]