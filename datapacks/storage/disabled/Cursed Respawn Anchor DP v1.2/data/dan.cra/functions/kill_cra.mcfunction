# kills Cursed Respawn Anchor when it is detected that it was broken

#kills obsidian item (broken Cursed Respawn Anchor block item)
kill @e[type=minecraft:item,tag=dan.item_obsidian,sort=nearest,limit=1]

#reset spawnpoint for players
execute positioned ~ ~ ~ in minecraft:overworld run spawnpoint @a ~ ~ ~

#set Cursed Respawn Anchor global broken score to true
scoreboard players set $CRABroken dan.debug 1

#play sound
playsound minecraft:ambient.nether_wastes.mood block @a ~ ~ ~ 1.2 2
playsound minecraft:entity.ender_dragon.growl block @a ~ ~ ~ 1.2 .2

#particles
particle dust 1.0 0 0 1.0 ~ ~ ~ 0.5 0.5 0.5 1 50

#kills self
kill @s