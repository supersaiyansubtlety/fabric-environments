# runs every load/reload

#debug command (required)
scoreboard objectives add dan.debug dummy

##on reload
#count reloads
scoreboard players add $CRAReloadCount dan.debug 1

##on first load
#install (runs once on first install)
execute if score $CRAReloadCount dan.debug matches 1 run function dan.cra:on_event/on_install

##forceload
execute in minecraft:the_nether run forceload add 0 0 0 0
execute unless score $CRABroken dan.debug matches 1.. if score $CRAReloadCount dan.debug matches 2.. run function dan.cra:load_anchor_chunk

#begin loops
schedule function dan.cra:loop/main 1t replace
schedule function dan.cra:loop/per_10_ticks 1t replace