# init players

#if cursed respawn anchor is not set
execute as @s at @s in minecraft:the_nether unless entity @e[type=minecraft:armor_stand,tag=dan.cra_CRA] run function dan.cra:spawn_cra

#take player to Cursed Respawn Anchor
execute as @s in minecraft:the_nether at @e[type=minecraft:armor_stand,tag=dan.cra_CRA] positioned ~ ~1.188 ~ run tp ~ ~1 ~

#set spawnpoint
execute as @s in minecraft:the_nether at @e[type=minecraft:armor_stand,tag=dan.cra_CRA] positioned ~ ~1.188 ~ run spawnpoint @s ~ ~1 ~

#tag player
tag @s add dan.cra_init_player