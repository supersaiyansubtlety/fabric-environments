#find air block (goes upward from start position) (accounts for lava)

#remove lavafound tag
tag @s[tag=dan.cra_lavafound] remove dan.cra_lavafound

#load current chunk
execute positioned ~ ~ ~ run forceload add ~ ~ ~ ~

#tp to given position
execute unless block ~ ~ ~ air run tp @s ~ ~ ~

#air found, but there is lava underneath
execute if block ~ ~ ~ air if block ~ ~-1 ~ lava run tag @s add dan.cra_lavafound
execute as @s if entity @s[tag=dan.cra_lavafound] run spreadplayers ~ ~ 5 8 false @s

#reset y position
execute at @s positioned ~-0.1 ~ ~-0.1 if entity @s[y=120,dy=256] run tp @s ~ 0 ~

#load current chunk
execute positioned ~ ~ ~ run forceload add ~ ~ ~ ~

#run recursively if air not found (make sure at least 3 blocks of air vertically found)
execute unless entity @s[tag=dan.cra_lavafound] unless block ~ ~ ~ air positioned ~ ~1 ~ run function dan.cra:utility/find_air_bottom_to_top_in_nether
execute if entity @s[tag=dan.cra_lavafound] at @s run function dan.cra:utility/find_air_bottom_to_top_in_nether