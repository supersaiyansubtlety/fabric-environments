# creates the Cursed Respawn Anchor

#forceload nether chunk before executing in it
execute at @s in minecraft:the_nether run forceload add ~ ~ ~ ~ 

#create air finding AEC
execute at @s in minecraft:the_nether run summon minecraft:area_effect_cloud ~ 0 ~ {Age:-2147483648,Duration:-1,WaitTime:-2147483648,Tags:["dan.cra_find_air","global.ignore"]}

#find air in nether
execute in minecraft:the_nether as @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_air] at @s run function dan.cra:utility/find_air_bottom_to_top_in_nether

#place Cursed Respawn Anchor
execute in minecraft:the_nether as @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_air] at @s positioned ~ ~1 ~ run setblock ~ ~ ~ minecraft:obsidian replace
execute in minecraft:the_nether as @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_air] at @s align xyz positioned ~0.5 ~1 ~0.5 run summon minecraft:armor_stand ~ ~-1.188 ~ {CustomName:'"Cursed Respawn Anchor"',Tags:["dan.cra_CRA"],ArmorItems:[{},{},{},{id:"minecraft:obsidian",Count:1b,tag:{CustomModelData:4150000}}],Invisible:1b,Invulnerable:1b,NoGravity:1b,DisabledSlots:2039583}

#save Pos to forceload chunks
execute in minecraft:the_nether run data modify storage dan.cra_pos Pos set from entity @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_air,limit=1] Pos

#place platform
execute in minecraft:the_nether at @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_air] run fill ~-2 ~ ~-2 ~2 ~ ~2 minecraft:netherrack replace minecraft:air

#kill AEC
kill @e[type=minecraft:area_effect_cloud,tag=dan.cra_find_air]

#unload all chunks
execute in minecraft:the_nether run forceload remove all

#reload to reload chunks
reload