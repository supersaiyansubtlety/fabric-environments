# main - runs every tick

#initialize new players
execute as @a[tag=!dan.cra_init_player] unless score $CRABroken dan.debug matches 1 run function dan.cra:on_event/on_init_player

#item nbt tagging
execute as @e[type=minecraft:item,tag=!dan.cra_item_nbt_tagged] run function dan.cra:utility/item_nbt_tagging

#run Cursed Respawn Anchor
execute in minecraft:the_nether as @e[type=minecraft:armor_stand,tag=dan.cra_CRA] run function dan.cra:run_cra

#continue loop
schedule function dan.cra:loop/main 1t replace