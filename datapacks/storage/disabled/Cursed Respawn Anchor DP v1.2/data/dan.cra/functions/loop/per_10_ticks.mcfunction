#runs every 10 ticks

#set player's spawnpoint to Cursed Respawn Anchor
execute in minecraft:the_nether at @e[type=minecraft:armor_stand,tag=dan.cra_CRA,limit=1] positioned ~ ~1.188 ~ run spawnpoint @a ~ ~1 ~

#Cursed Respawn Anchor particles
execute in minecraft:the_nether at @e[type=minecraft:armor_stand,tag=dan.cra_CRA,limit=1] positioned ~ ~1.188 ~ run particle dust 1.0 0 0 1.0 ~ ~1 ~ .22 .05 .22 .05 2

#continue loop
schedule function dan.cra:loop/per_10_ticks 10t replace