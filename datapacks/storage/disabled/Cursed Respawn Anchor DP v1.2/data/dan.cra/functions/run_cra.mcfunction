# runs as Cursed Respawn Anchor

#detect that the Cursed Respawn Anchor was broken (with a proper pickaxe by dropping the Obsidian block item)
execute as @s at @s positioned ~ ~1.188 ~ if entity @e[type=minecraft:item,tag=dan.item_obsidian,distance=..1] if entity @a[distance=..6,scores={dan.cra_mineObsi=1..}] run function dan.cra:kill_cra

#if the block was broken but didnt drop the Obsidian block item, then it must have not been broken properly
execute unless score $CRABroken dan.debug matches 1 at @s positioned ~ ~1.188 ~ unless block ~ ~ ~ minecraft:obsidian run setblock ~ ~ ~ minecraft:obsidian replace

#reset scores
scoreboard players reset @a dan.cra_mineObsi