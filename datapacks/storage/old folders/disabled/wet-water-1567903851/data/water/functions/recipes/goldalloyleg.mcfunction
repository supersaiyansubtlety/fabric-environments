clear @s minecraft:structure_void 1
give @s minecraft:diamond_leggings{display:{Name:"{\"text\":\"Gold alloy Leggings\",\"italic\":false}",Lore:["\"Crafted artifact\""]},AttributeModifiers:[{AttributeName:"generic.armor",Name:"generic.armor",Slot:"legs",Amount:10,Operation:0,UUIDMost:67524,UUIDLeast:101615},{AttributeName:"generic.armorToughness",Name:"generic.armorToughness",Slot:"legs",Amount:12,Operation:0,UUIDMost:23983,UUIDLeast:141008}]} 1
schedule function legacy:recipes/resetrecipe 1s
