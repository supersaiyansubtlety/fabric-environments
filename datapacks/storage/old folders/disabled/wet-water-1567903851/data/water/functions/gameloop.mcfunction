execute as @a at @s if block ~ ~ ~ air run scoreboard players set @s Wet 0
execute as @a at @s if block ~ ~ ~ water run scoreboard players set @s Wet 1


execute as @a[scores={Wet=1}] run scoreboard players set @s out 1200
execute as @a[scores={Wet=0}] unless score @s out matches 0 run scoreboard players remove @s out 1
execute as @a[scores={Wet=0}] at @s unless score @s out matches 0 run particle minecraft:dripping_water ~ ~1 ~ 0.2 0.3 0.2 1 1 normal


#Delete everything after this comment to enable aesthetic mode (Or add # to the front if you're planning on disabling it in the future)
#execute as @a[scores={Wet=1}] run effect give @s minecraft:slowness 60 0 false
#effect give @a[scores={Wet=1},nbt={Inventory:[{Slot:100b,id:"minecraft:leather_boots"}]}] minecraft:slowness 70 1 false
#effect give @a[scores={Wet=1},nbt={Inventory:[{Slot:102b,id:"minecraft:leather_chestplate"}]}] minecraft:slowness 120 2 false
#effect give @a[scores={Wet=1},nbt={Inventory:[{Slot:101b,id:"minecraft:leather_leggings"}]}] minecraft:slowness 100 2 false
#effect give @a[scores={Wet=1},nbt={Inventory:[{Slot:103b,id:"minecraft:leather_helmet"}]}] minecraft:slowness 80 1 false
