clear @s minecraft:structure_void 1
give @s minecraft:diamond_pickaxe{display:{Name:"{\"text\":\"Obsidian Pickaxe\",\"color\":\"gold\",\"italic\":false}",Lore:["\"Crafted artifact\""]},Unbreakable:1,AttributeModifiers:[{AttributeName:"generic.attackSpeed",Name:"generic.attackSpeed",Slot:"mainhand",Amount:0.25,Operation:1,UUIDMost:71334,UUIDLeast:126291},{AttributeName:"generic.movementSpeed",Name:"generic.movementSpeed",Slot:"mainhand",Amount:-0.25,Operation:1,UUIDMost:55936,UUIDLeast:128498}],Enchantments:[{id:"minecraft:efficiency",lvl:6}]} 1
schedule function legacy:recipes/resetrecipe 1s

