clear @s minecraft:structure_void 1
give @s minecraft:diamond_helmet{display:{Name:"{\"text\":\"Gold alloy helmet\",\"italic\":false}",Lore:["\"Crafted artifact\""]},AttributeModifiers:[{AttributeName:"generic.armor",Name:"generic.armor",Slot:"head",Amount:7,Operation:0,UUIDMost:21492,UUIDLeast:144052},{AttributeName:"generic.armorToughness",Name:"generic.armorToughness",Slot:"head",Amount:9,Operation:0,UUIDMost:78251,UUIDLeast:158077}]} 1
schedule function legacy:recipes/resetrecipe 1s

