execute as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1

tellraw @a[scores={vs_mh_adminlist=1}] [{"text":"\nModularHUD","color":"gold","bold":true},{"text":" "},{"text":"uninstallation","color":"white"},{"text":"\n - ","color":"white"},{"text":"ModularHUD","color":"white"},{"text":" "},{"text":"uninstalled\n","color":"green"}]

scoreboard objectives remove modularhud
scoreboard objectives remove vs_mh_d_session
scoreboard objectives remove vs_mh_sessticks
scoreboard objectives remove vs_mh_sessmins
scoreboard objectives remove vs_mh_sesshours
scoreboard objectives remove vs_mh_sesscalc
scoreboard objectives remove vs_mh_sesscheck
scoreboard objectives remove vs_mh_d_position
scoreboard objectives remove vs_mh_positionx
scoreboard objectives remove vs_mh_positiony
scoreboard objectives remove vs_mh_positionz
scoreboard objectives remove vs_mh_d_hmobs
scoreboard objectives remove vs_mh_hmobs
scoreboard objectives remove vs_mh_d_pitch
scoreboard objectives remove vs_mh_pitch
scoreboard objectives remove vs_mh_d_time
scoreboard objectives remove vs_mh_time
scoreboard objectives remove vs_mh_d_dir
scoreboard objectives remove vs_mh_direction
scoreboard objectives remove vs_mh_dir_n
scoreboard objectives remove vs_mh_dir_ne
scoreboard objectives remove vs_mh_dir_e
scoreboard objectives remove vs_mh_dir_se
scoreboard objectives remove vs_mh_dir_s
scoreboard objectives remove vs_mh_dir_sw
scoreboard objectives remove vs_mh_dir_w
scoreboard objectives remove vs_mh_dir_nw
scoreboard objectives remove vs_mh_d_compact
scoreboard objectives remove vs_mh_d_display
scoreboard objectives remove vs_mh_timer
scoreboard objectives remove vs_mh_constants
scoreboard objectives remove vs_mh_config
scoreboard objectives remove vs_mh_configedit
scoreboard objectives remove vs_mh_uninstall
scoreboard objectives remove vs_mh_adminlist
scoreboard objectives remove vs_mh_hi_map
scoreboard objectives remove vs_mh_hi_clock
scoreboard objectives remove vs_mh_hi_compass
scoreboard objectives remove vs_mh_hi_beacon

schedule function modularhud:uninstall/disable 1t