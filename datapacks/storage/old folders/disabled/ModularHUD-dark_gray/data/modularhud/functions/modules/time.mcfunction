execute store result score -hours vs_mh_time run time query daytime
scoreboard players operation -hours vs_mh_time /= -1000 vs_mh_constants
scoreboard players operation -hours vs_mh_time *= -1000 vs_mh_constants

execute store result score -minutes vs_mh_time run time query daytime
scoreboard players operation -minutes vs_mh_time -= -hours vs_mh_time
scoreboard players operation -minutes vs_mh_time *= -6 vs_mh_constants
scoreboard players operation -minutes vs_mh_time /= -100 vs_mh_constants

scoreboard players operation -hours vs_mh_time += -6000 vs_mh_constants
scoreboard players operation -hours vs_mh_time /= -1000 vs_mh_constants
execute if score -hours vs_mh_time matches 24.. run scoreboard players operation -hours vs_mh_time -= -24 vs_mh_constants