scoreboard players enable @a modularhud

execute as @a[scores={modularhud=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1

execute as @a[scores={modularhud=1}] run tellraw @s [{"text":"\nModularHUD ","color":"gold","bold":true},{"text":"customization","color":"white","bold":true},{"text":"\n - ","color":"white","bold":true},{"text":"Customize your HUD using the toggle buttons","color":"white","bold":true}]



execute if score -session vs_mh_config matches 1 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Session","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"[TOGGLE]","color":"dark_aqua","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Toggle the session module"}},"clickEvent":{"action":"run_command","value":"/trigger modularhud set 100"}}]

execute if score -session vs_mh_config matches 2 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Session","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"FORCED ON","color":"yellow","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"You are not allowed to\ntoggle this module off"}}}]

execute if score -session vs_mh_config matches 3 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Session","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"REQUIRES ITEM","color":"light_purple","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"Required item: Clock\nHave the item in your inventory\nto display the module"}}}]



execute if score -position vs_mh_config matches 1 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Position","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"[TOGGLE]","color":"dark_aqua","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Toggle the position module"}},"clickEvent":{"action":"run_command","value":"/trigger modularhud set 200"}}]

execute if score -position vs_mh_config matches 2 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Position","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"FORCED ON","color":"yellow","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"You are not allowed to\ntoggle this module off"}}}]

execute if score -position vs_mh_config matches 3 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Position","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"REQUIRES ITEM","color":"light_purple","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"Required item: Map\nHave the item in your inventory\nto display the module"}}}]




execute if score -hmobs vs_mh_config matches 1 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Hostile Mobs","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"[TOGGLE]","color":"dark_aqua","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Toggle the hostile mobs module"}},"clickEvent":{"action":"run_command","value":"/trigger modularhud set 300"}}]

execute if score -hmobs vs_mh_config matches 2 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Hostile Mobs","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"FORCED ON","color":"yellow","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"You are not allowed to\ntoggle this module off"}}}]

execute if score -hmobs vs_mh_config matches 3 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Hostile Mobs","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"REQUIRES ITEM","color":"light_purple","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"Required item: Beacon\nHave the item in your inventory\nto display the module"}}}]



execute if score -pitch vs_mh_config matches 1 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Pitch","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"[TOGGLE]","color":"dark_aqua","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Toggle the pitch module"}},"clickEvent":{"action":"run_command","value":"/trigger modularhud set 400"}}]

execute if score -pitch vs_mh_config matches 2 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Pitch","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"FORCED ON","color":"yellow","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"You are not allowed to\ntoggle this module off"}}}]

execute if score -pitch vs_mh_config matches 3 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Pitch","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"REQUIRES ITEM","color":"light_purple","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"Required item: Compass\nHave the item in your inventory\nto display the module"}}}]



execute if score -time vs_mh_config matches 1 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Time","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"[TOGGLE]","color":"dark_aqua","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Toggle the time module"}},"clickEvent":{"action":"run_command","value":"/trigger modularhud set 500"}}]

execute if score -time vs_mh_config matches 2 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Time","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"FORCED ON","color":"yellow","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"You are not allowed to\ntoggle this module off"}}}]

execute if score -time vs_mh_config matches 3 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Time","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"REQUIRES ITEM","color":"light_purple","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"Required item: Clock\nHave the item in your inventory\nto display the module"}}}]



execute if score -direction vs_mh_config matches 1 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Direction","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"[TOGGLE]","color":"dark_aqua","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Toggle the direction module"}},"clickEvent":{"action":"run_command","value":"/trigger modularhud set 600"}}]

execute if score -direction vs_mh_config matches 2 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Direction","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"FORCED ON","color":"yellow","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"You are not allowed to\ntoggle this module off"}}}]

execute if score -direction vs_mh_config matches 3 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Direction","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"REQUIRES ITEM","color":"light_purple","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"Required item: Compass\nHave the item in your inventory\nto display the module"}}}]



execute if score -compact vs_mh_config matches 1 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Compact Mode","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"[TOGGLE]","color":"dark_aqua","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Toggle compact mode"}},"clickEvent":{"action":"run_command","value":"/trigger modularhud set 700"}}]

execute if score -compact vs_mh_config matches 2 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Compact Mode","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"FORCED ON","color":"yellow","bold":true,"hoverEvent":{"action":"show_text","value":{"text":"You are not allowed to\ntoggle compact mode off"}}}]



execute if score -displayhud vs_mh_config matches 1 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Display","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"[TOGGLE]","color":"dark_aqua","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Toggle HUD visibility"}},"clickEvent":{"action":"run_command","value":"/trigger modularhud set 800"}}]

execute if score -displayhud vs_mh_config matches 2 as @a[scores={modularhud=1}] run tellraw @s [{"text":"    - ","color":"white","bold":true},{"text":"Display","color":"white","bold":true},{"text":"\n       - ","color":"white","bold":true},{"text":"FORCED ON","color":"yellow","bold":true,"italic":true,"hoverEvent":{"action":"show_text","value":{"text":"You are not allowed to\ntoggle the HUD off"}}}]



execute as @a[scores={modularhud=1}] run tellraw @s [{"text":""}]

execute if score @a[limit=1] modularhud matches 1.. run function modularhud:trigger/process
execute if score @a[limit=1] modularhud matches ..1 run function modularhud:trigger/process