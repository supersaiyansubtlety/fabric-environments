############################################################
# Description: Makes apple pies work
# Creator: CreeperMagnet_
############################################################

execute as @a[tag=tcc.scheduled.apple_pie_half.offhand,nbt=!{Inventory:[{Slot:-106b}]}] run loot replace entity @s weapon.offhand loot tcc:items/apple_pie_slice
execute as @a[tag=tcc.scheduled.apple_pie_half.mainhand,nbt=!{SelectedItem:{}}] run loot replace entity @s weapon.mainhand loot tcc:items/apple_pie_slice
tag @a[tag=tcc.scheduled.apple_pie_half.offhand] remove tcc.scheduled.apple_pie_half.offhand
tag @a[tag=tcc.scheduled.apple_pie_half.mainhand] remove tcc.scheduled.apple_pie_half.mainhand
