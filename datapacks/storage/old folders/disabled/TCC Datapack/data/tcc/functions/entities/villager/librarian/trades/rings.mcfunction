############################################################
# Description: Chooses a random kind of item to add to charm trades
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/2
execute as @s[scores={tcc.random=1}] run function tcc:entities/villager/librarian/trades/rings/golden_ring
execute as @s[scores={tcc.random=2}] run function tcc:entities/villager/librarian/trades/rings/iron_ring
