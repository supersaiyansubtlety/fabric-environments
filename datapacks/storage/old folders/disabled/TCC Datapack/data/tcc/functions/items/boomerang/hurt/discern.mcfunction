############################################################
# Description: Commands for the boomerang hitting entities
# Creator: CreeperMagnet_
############################################################

execute as @s[type=phantom] positioned ~-0.4 ~-0.5 ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.2 ~0.5 ~-0.2 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt_undead

execute as @s[type=player] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.95] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.95] run function tcc:items/boomerang/hurt/hurt_player

execute as @s[type=ghast] positioned ~-2 ~ ~-2 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=3,dz=3,dy=3] run function tcc:items/boomerang/hurt/hurt

execute as @e[type=#tcc:boomerang_damage/player_sized,type=#tcc:boomerang_damage/undead,nbt=!{IsBaby:1b}] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.95] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.95] run function tcc:items/boomerang/hurt/hurt_undead
execute as @e[type=#tcc:boomerang_damage/player_sized,type=#tcc:boomerang_damage/undead,nbt={IsBaby:1b}] positioned ~-0.15 ~-0.025 ~-0.15 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.7 ~0.025 ~-0.7 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt_undead

execute as @e[type=#tcc:boomerang_damage/player_sized,type=!#tcc:boomerang_damage/undead] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.95] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.95] run function tcc:items/boomerang/hurt/hurt


execute as @s[type=creeper] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.7] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.7] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=stray] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.99] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.99] run function tcc:items/boomerang/hurt/hurt_undead
execute as @s[type=skeleton] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.99] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.99] run function tcc:items/boomerang/hurt/hurt_undead
execute as @s[type=blaze] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.8] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.8] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=enderman] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=1.9] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=1.9] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=spider] positioned ~-0.7 ~-0.1 ~-0.7 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.4,dz=0.4,dy=0] positioned ~ ~0.1 ~ if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.4,dz=0.4,dy=0] run function tcc:items/boomerang/hurt/hurt


execute as @s[type=shulker,nbt=!{Peek:0b}] positioned ~-0.5 ~ ~-0.5 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=1] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=slime,nbt={Size:1}] positioned ~-0.51 ~ ~-0.51 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.02,dz=0.02,dy=0.02] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=magma_cube,nbt={Size:1}] positioned ~-0.51 ~ ~-0.51 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.02,dz=0.02,dy=0.02] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=slime,nbt={Size:2}] positioned ~-1.02 ~ ~-1.02 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=1.04,dz=1.04,dy=1.04] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=magma_cube,nbt={Size:2}] positioned ~-1.02 ~ ~-1.02 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=1.04,dz=1.04,dy=1.04] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=slime,nbt={Size:1}] positioned ~-0.51 ~ ~-0.51 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.02,dz=0.02,dy=0.02] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=slime,nbt={Size:0}] positioned ~-0.255 ~-0.49 ~-0.255 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.49 ~0.49 ~-0.49 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=magma_cube,nbt={Size:0}] positioned ~-0.255 ~-0.49 ~-0.255 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.49 ~0.49 ~-0.49 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=iron_golem] positioned ~-0.7 ~ ~-0.7 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.4,dz=0.4,dy=1.7] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=elder_guardian] positioned ~-1 ~ ~-1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=1,dz=1,dy=1] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=snow_golem] positioned ~-0.35 ~ ~-0.35 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.9] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.9] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=wither_skeleton] positioned ~-0.35 ~ ~-0.35 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=1.4] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=1.4] run function tcc:items/boomerang/hurt/hurt_undead
execute as @s[type=guardian] positioned ~-0.425 ~-0.15 ~-0.425 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.15 ~0.15 ~-0.15 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=squid] positioned ~-0.4 ~-0.2 ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.2 ~0.2 ~-0.2 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=bat] positioned ~-0.25 ~-0.1 ~-0.25 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.5 ~0.1 ~-0.5 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=parrot] positioned ~-0.25 ~-0.1 ~-0.25 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.5 ~0.1 ~-0.5 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=vex] positioned ~-0.2 ~-0.2 ~-0.2 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.6 ~0.2 ~-0.6 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=pufferfish,nbt={PuffState:2}] positioned ~-0.35 ~-0.3 ~-0.35 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.3 ~0.3 ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=pufferfish,nbt={PuffState:1}] positioned ~-0.25 ~-0.5 ~-0.25 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.5 ~0.5 ~-0.5 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=pufferfish,nbt={PuffState:0}] positioned ~-0.16 ~-0.65 ~-0.16 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.68 ~0.65 ~-0.68 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=tropical_fish] positioned ~-0.25 ~-0.6 ~-0.25 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.5 ~0.6 ~-0.5 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=cod] positioned ~-0.25 ~-0.7 ~-0.25 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.5 ~0.7 ~-0.5 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=salmon] positioned ~-0.35 ~-0.6 ~-0.35 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.3 ~0.6 ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=silverfish] positioned ~-0.2 ~-0.7 ~-0.2 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.6 ~0.7 ~-0.6 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=endermite] positioned ~-0.2 ~-0.7 ~-0.2 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.6 ~0.7 ~-0.6 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=cave_spider] positioned ~-0.35 ~-0.5 ~-0.35 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.3 ~0.5 ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=dolphin] positioned ~-0.45 ~-0.4 ~-0.45 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.1 ~0.4 ~-0.1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=ravager] positioned ~-0.975 ~ ~-0.975 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.95,dz=0.95,dy=1.2] run function tcc:items/boomerang/hurt/hurt

## Mobs with Age tag
execute store result score @s tcc.math run data get entity @s Age

execute as @s[type=villager,scores={tcc.math=0..}] positioned ~-0.3 ~ ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.95] positioned ~-0.4 ~ ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.95] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=villager,scores={tcc.math=..-1}] positioned ~-0.15 ~-0.025 ~-0.15 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.7 ~0.025 ~-0.7 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=#tcc:boomerang_damage/cat_sized,scores={tcc.math=0..}] positioned ~-0.3 ~-0.3 ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.4 ~0.3 ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=#tcc:boomerang_damage/cat_sized,scores={tcc.math=..-1}] positioned ~-0.15 ~-0.65 ~-0.15 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.7 ~0.65 ~-0.7 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=wolf,scores={tcc.math=0..}] positioned ~-0.3 ~-0.15 ~-0.3 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.4 ~0.15 ~-0.4 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=wolf,scores={tcc.math=..-1}] positioned ~-0.15 ~-0.575 ~-0.15 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.7 ~0.575 ~-0.7 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=panda,scores={tcc.math=0..}] positioned ~-0.65 ~ ~-0.65 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.3,dz=0.3,dy=0.25] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=panda,scores={tcc.math=..-1}] positioned ~-0.325 ~-0.375 ~-0.325 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.675 ~0.375 ~-0.675 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=polar_bear,scores={tcc.math=0..}] positioned ~-0.65 ~ ~-0.65 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.3,dz=0.3,dy=0.4] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=polar_bear,scores={tcc.math=..-1}] positioned ~-0.325 ~-0.3 ~-0.325 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.675 ~0.3 ~-0.675 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=turtle,scores={tcc.math=0..}] positioned ~-0.55 ~-0.6 ~-0.55 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.1,dz=0.1,dy=0] positioned ~ ~0.6 ~ if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.1,dz=0.1,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=turtle,scores={tcc.math=..-1}] positioned ~-0.16 ~-0.88 ~-0.16 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.68 ~0.88 ~-0.68 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=#tcc:boomerang_damage/horse_sized,type=!#tcc:boomerang_damage/undead,scores={tcc.math=0..}] positioned ~-0.6982 ~ ~-0.6982 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.3964,dz=0.3964,dy=0.6] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=#tcc:boomerang_damage/horse_sized,type=#tcc:boomerang_damage/undead,scores={tcc.math=0..}] positioned ~-0.6982 ~ ~-0.6982 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0.3964,dz=0.3964,dy=0.6] run function tcc:items/boomerang/hurt/hurt_undead

execute as @s[type=#tcc:boomerang_damage/horse_sized,type=!#tcc:boomerang_damage/undead,scores={tcc.math=..-1}] positioned ~-0.3491 ~-0.2 ~-0.3491 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.3018 ~0.2 ~-0.3018 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=#tcc:boomerang_damage/horse_sized,type=#tcc:boomerang_damage/undead,scores={tcc.math=..-1}] positioned ~-0.3491 ~-0.2 ~-0.3491 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.3018 ~0.2 ~-0.3018 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt_undead

execute as @s[type=cow,scores={tcc.math=0..}] positioned ~-0.45 ~ ~-0.45 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.4] positioned ~-0.1 ~ ~-0.1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.4] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=mooshroom,scores={tcc.math=0..}] positioned ~-0.45 ~ ~-0.45 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.4] positioned ~-0.1 ~ ~-0.1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.4] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=sheep,scores={tcc.math=0..}] positioned ~-0.45 ~ ~-0.45 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.3] positioned ~-0.1 ~ ~-0.1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.3] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=#tcc:boomerang_damage/llama_sized,scores={tcc.math=0..}] positioned ~-0.45 ~ ~-0.45 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.875] positioned ~-0.1 ~ ~-0.1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0.875] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=pig,scores={tcc.math=0..}] positioned ~-0.45 ~-0.1 ~-0.45 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.1 ~0.1 ~-0.1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=chicken,scores={tcc.math=0..}] positioned ~-0.2 ~-0.3 ~-0.2 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.6 ~0.3 ~-0.6 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=rabbit,scores={tcc.math=0..}] positioned ~-0.2 ~-0.5 ~-0.2 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.6 ~0.5 ~-0.6 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

execute as @s[type=cow,scores={tcc.math=..-1}] positioned ~-0.225 ~-0.3 ~-0.225 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.55 ~0.3 ~-0.55 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=mooshroom,scores={tcc.math=..-1}] positioned ~-0.225 ~-0.3 ~-0.225 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.55 ~0.3 ~-0.55 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=sheep,scores={tcc.math=..-1}] positioned ~-0.225 ~-0.325 ~-0.225 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.55 ~0.325 ~-0.55 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=#tcc:boomerang_damage/llama_sized,scores={tcc.math=..-1}] positioned ~-0.225 ~-0.0625 ~-0.225 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.55 ~0.0625 ~-0.55 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=pig,scores={tcc.math=..-1}] positioned ~-0.225 ~-0.55 ~-0.225 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.55 ~0.55 ~-0.55 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=chicken,scores={tcc.math=..-1}] positioned ~-0.1 ~-0.65 ~-0.1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.8 ~0.65 ~-0.8 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt
execute as @s[type=rabbit,scores={tcc.math=..-1}] positioned ~-0.1 ~-0.65 ~-0.1 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] positioned ~-0.8 ~0.65 ~-0.8 if entity @e[tag=tcc.boomerang,limit=1,sort=nearest,dx=0,dz=0,dy=0] run function tcc:items/boomerang/hurt/hurt

scoreboard players set @s tcc.math 0
