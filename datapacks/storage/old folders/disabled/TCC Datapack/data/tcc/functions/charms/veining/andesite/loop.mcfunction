############################################################
# Description: Makes the veining enchantment find ore around
# Creator: CreeperMagnet_
############################################################

particle block minecraft:andesite ~ ~ ~ 0.5 0.5 0.5 1 1 force
execute as @a[scores={tcc.mineandesite=1..},distance=..10] run loot spawn ~ ~ ~ mine ~ ~ ~ mainhand
setblock ~ ~ ~ air
execute positioned ~ ~1 ~ if block ~ ~ ~ andesite if entity @a[scores={tcc.mineandesite=1..},distance=..10] run function tcc:charms/veining/andesite/loop
execute positioned ~ ~-1 ~ if block ~ ~ ~ andesite if entity @a[scores={tcc.mineandesite=1..},distance=..10] run function tcc:charms/veining/andesite/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ andesite if entity @a[scores={tcc.mineandesite=1..},distance=..10] run function tcc:charms/veining/andesite/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ andesite if entity @a[scores={tcc.mineandesite=1..},distance=..10] run function tcc:charms/veining/andesite/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ andesite if entity @a[scores={tcc.mineandesite=1..},distance=..10] run function tcc:charms/veining/andesite/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ andesite if entity @a[scores={tcc.mineandesite=1..},distance=..10] run function tcc:charms/veining/andesite/loop