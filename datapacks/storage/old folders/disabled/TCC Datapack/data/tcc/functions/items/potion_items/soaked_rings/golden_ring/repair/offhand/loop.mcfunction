############################################################
# Description: Repairs a soaked golden ring
# Creator: CreeperMagnet_
############################################################

execute as @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:25,Item:"soaked_golden_ring"}}}]}] run function tcc:items/potion_items/soaked_rings/golden_ring/repair/offhand/add_durability
xp add @s -1 points
scoreboard players add @s tcc.old_xp 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:25,Item:"soaked_golden_ring"}}}]}] run scoreboard players set @s tcc.old_xp 0
execute as @s[scores={tcc.old_xp=0}] run function tcc:items/potion_items/soaked_rings/golden_ring/repair/offhand/merge_lore
execute as @s[scores={tcc.old_xp=0}] run scoreboard players reset @s tcc.old_xp
execute as @s[scores={tcc.old_xp=..-1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:25,Item:"soaked_golden_ring"}}}]}] run function tcc:items/potion_items/soaked_rings/golden_ring/repair/offhand/loop
