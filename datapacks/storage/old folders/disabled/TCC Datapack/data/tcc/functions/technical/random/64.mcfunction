############################################################
# Description: Generates a random number, either 1 or 2
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/32
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome1.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome1.2
execute as @s[scores={tcc.random=3}] run tag @s add tcc.random.outcome1.3
execute as @s[scores={tcc.random=4}] run tag @s add tcc.random.outcome1.4
execute as @s[scores={tcc.random=5}] run tag @s add tcc.random.outcome1.5
execute as @s[scores={tcc.random=6}] run tag @s add tcc.random.outcome1.6
execute as @s[scores={tcc.random=7}] run tag @s add tcc.random.outcome1.7
execute as @s[scores={tcc.random=8}] run tag @s add tcc.random.outcome1.8
execute as @s[scores={tcc.random=9}] run tag @s add tcc.random.outcome1.9
execute as @s[scores={tcc.random=10}] run tag @s add tcc.random.outcome1.10
execute as @s[scores={tcc.random=11}] run tag @s add tcc.random.outcome1.11
execute as @s[scores={tcc.random=12}] run tag @s add tcc.random.outcome1.12
execute as @s[scores={tcc.random=13}] run tag @s add tcc.random.outcome1.13
execute as @s[scores={tcc.random=14}] run tag @s add tcc.random.outcome1.14
execute as @s[scores={tcc.random=15}] run tag @s add tcc.random.outcome1.15
execute as @s[scores={tcc.random=16}] run tag @s add tcc.random.outcome1.16
execute as @s[scores={tcc.random=17}] run tag @s add tcc.random.outcome1.17
execute as @s[scores={tcc.random=18}] run tag @s add tcc.random.outcome1.18
execute as @s[scores={tcc.random=19}] run tag @s add tcc.random.outcome1.19
execute as @s[scores={tcc.random=20}] run tag @s add tcc.random.outcome1.20
execute as @s[scores={tcc.random=21}] run tag @s add tcc.random.outcome1.21
execute as @s[scores={tcc.random=22}] run tag @s add tcc.random.outcome1.22
execute as @s[scores={tcc.random=23}] run tag @s add tcc.random.outcome1.23
execute as @s[scores={tcc.random=24}] run tag @s add tcc.random.outcome1.24
execute as @s[scores={tcc.random=25}] run tag @s add tcc.random.outcome1.25
execute as @s[scores={tcc.random=26}] run tag @s add tcc.random.outcome1.26
execute as @s[scores={tcc.random=27}] run tag @s add tcc.random.outcome1.27
execute as @s[scores={tcc.random=28}] run tag @s add tcc.random.outcome1.28
execute as @s[scores={tcc.random=29}] run tag @s add tcc.random.outcome1.29
execute as @s[scores={tcc.random=30}] run tag @s add tcc.random.outcome1.30
execute as @s[scores={tcc.random=31}] run tag @s add tcc.random.outcome1.31
execute as @s[scores={tcc.random=32}] run tag @s add tcc.random.outcome1.32
function tcc:technical/random/2
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome2.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome2.2
scoreboard players reset @s tcc.random

execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 1
execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 2
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 3
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 4
execute as @s[tag=tcc.random.outcome1.3,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 5
execute as @s[tag=tcc.random.outcome1.3,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 6
execute as @s[tag=tcc.random.outcome1.4,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 7
execute as @s[tag=tcc.random.outcome1.4,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 8
execute as @s[tag=tcc.random.outcome1.5,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 9
execute as @s[tag=tcc.random.outcome1.5,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 10
execute as @s[tag=tcc.random.outcome1.6,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 11
execute as @s[tag=tcc.random.outcome1.6,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 12
execute as @s[tag=tcc.random.outcome1.7,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 13
execute as @s[tag=tcc.random.outcome1.7,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 14
execute as @s[tag=tcc.random.outcome1.8,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 15
execute as @s[tag=tcc.random.outcome1.8,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 16
execute as @s[tag=tcc.random.outcome1.9,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 17
execute as @s[tag=tcc.random.outcome1.9,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 18
execute as @s[tag=tcc.random.outcome1.10,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 19
execute as @s[tag=tcc.random.outcome1.10,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 20
execute as @s[tag=tcc.random.outcome1.11,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 21
execute as @s[tag=tcc.random.outcome1.11,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 22
execute as @s[tag=tcc.random.outcome1.12,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 23
execute as @s[tag=tcc.random.outcome1.12,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 24
execute as @s[tag=tcc.random.outcome1.13,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 25
execute as @s[tag=tcc.random.outcome1.13,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 26
execute as @s[tag=tcc.random.outcome1.14,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 27
execute as @s[tag=tcc.random.outcome1.14,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 28
execute as @s[tag=tcc.random.outcome1.15,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 29
execute as @s[tag=tcc.random.outcome1.15,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 30
execute as @s[tag=tcc.random.outcome1.16,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 31
execute as @s[tag=tcc.random.outcome1.16,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 32
execute as @s[tag=tcc.random.outcome1.17,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 33
execute as @s[tag=tcc.random.outcome1.17,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 34
execute as @s[tag=tcc.random.outcome1.18,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 35
execute as @s[tag=tcc.random.outcome1.18,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 36
execute as @s[tag=tcc.random.outcome1.19,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 37
execute as @s[tag=tcc.random.outcome1.19,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 38
execute as @s[tag=tcc.random.outcome1.20,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 39
execute as @s[tag=tcc.random.outcome1.20,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 40
execute as @s[tag=tcc.random.outcome1.21,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 41
execute as @s[tag=tcc.random.outcome1.21,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 42
execute as @s[tag=tcc.random.outcome1.22,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 43
execute as @s[tag=tcc.random.outcome1.22,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 44
execute as @s[tag=tcc.random.outcome1.23,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 45
execute as @s[tag=tcc.random.outcome1.23,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 46
execute as @s[tag=tcc.random.outcome1.24,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 47
execute as @s[tag=tcc.random.outcome1.24,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 48
execute as @s[tag=tcc.random.outcome1.25,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 49
execute as @s[tag=tcc.random.outcome1.25,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 50
execute as @s[tag=tcc.random.outcome1.26,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 51
execute as @s[tag=tcc.random.outcome1.26,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 52
execute as @s[tag=tcc.random.outcome1.27,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 53
execute as @s[tag=tcc.random.outcome1.27,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 54
execute as @s[tag=tcc.random.outcome1.28,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 55
execute as @s[tag=tcc.random.outcome1.28,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 56
execute as @s[tag=tcc.random.outcome1.29,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 57
execute as @s[tag=tcc.random.outcome1.29,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 58
execute as @s[tag=tcc.random.outcome1.30,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 59
execute as @s[tag=tcc.random.outcome1.30,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 60
execute as @s[tag=tcc.random.outcome1.31,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 61
execute as @s[tag=tcc.random.outcome1.31,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 62
execute as @s[tag=tcc.random.outcome1.32,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 63
execute as @s[tag=tcc.random.outcome1.32,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 64

tag @s remove tcc.random.outcome1.1
tag @s remove tcc.random.outcome1.2
tag @s remove tcc.random.outcome1.3
tag @s remove tcc.random.outcome1.4
tag @s remove tcc.random.outcome1.5
tag @s remove tcc.random.outcome1.6
tag @s remove tcc.random.outcome1.7
tag @s remove tcc.random.outcome1.8
tag @s remove tcc.random.outcome1.9
tag @s remove tcc.random.outcome1.10
tag @s remove tcc.random.outcome1.11
tag @s remove tcc.random.outcome1.12
tag @s remove tcc.random.outcome1.13
tag @s remove tcc.random.outcome1.14
tag @s remove tcc.random.outcome1.15
tag @s remove tcc.random.outcome1.16
tag @s remove tcc.random.outcome1.17
tag @s remove tcc.random.outcome1.18
tag @s remove tcc.random.outcome1.19
tag @s remove tcc.random.outcome1.20
tag @s remove tcc.random.outcome1.21
tag @s remove tcc.random.outcome1.22
tag @s remove tcc.random.outcome1.23
tag @s remove tcc.random.outcome1.24
tag @s remove tcc.random.outcome1.25
tag @s remove tcc.random.outcome1.26
tag @s remove tcc.random.outcome1.27
tag @s remove tcc.random.outcome1.28
tag @s remove tcc.random.outcome1.29
tag @s remove tcc.random.outcome1.30
tag @s remove tcc.random.outcome1.31
tag @s remove tcc.random.outcome1.32
tag @s remove tcc.random.outcome2.1
tag @s remove tcc.random.outcome2.2