############################################################
# Description: Tracking for the boomerang
# Creator: CreeperMagnet_
############################################################

tag @s remove tcc.boomerang.charms.seeking.track.failed

scoreboard players set @a tcc.math 0

execute as @e[type=player,gamemode=!spectator] at @s store result score @s tcc.math run data get entity @s UUIDLeast 0.0000000001
scoreboard players operation @s tcc.math = @s tcc.boomerangu
scoreboard players operation @e[type=player,gamemode=!spectator] tcc.math -= @s tcc.math

#scoreboard players add @a tcc.math 100

tag @e[type=player,scores={tcc.math=0},gamemode=!spectator] add tcc.boomerang.player.uuid_match.track

execute as @s[scores={tcc.boomerang=50..119}] unless entity @e[type=player,tag=tcc.boomerang.player.uuid_match.track] at @s run tag @s add tcc.boomerang.charms.seeking.track.failed
execute if entity @p[tag=tcc.boomerang.player.uuid_match.track] run tag @s add tcc.boomerang.tracked_player
execute as @s anchored feet at @s run teleport @s ~ ~ ~ facing entity @p[tag=tcc.boomerang.player.uuid_match.track] eyes

tag @a remove tcc.boomerang.player.uuid_match.track