############################################################
# Description: Makes the obsidian mirror damage function
# Creator: CreeperMagnet_
############################################################
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:1s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/2
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:2s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/3
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:3s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/4
execute as @s[nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:unbreaking"}]}}]}] run scoreboard players set @s tcc.random 1



execute as @s[scores={tcc.random=1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"obsidian_mirror"}}}]}] store result score @s tcc.math2 run data get entity @s Inventory[{Slot:-106b}].tag.tcc.Durability 1
scoreboard players operation @s tcc.math2 -= @s tcc.math

execute as @s[scores={tcc.random=1,tcc.math2=..0}] run tag @s add tcc.tag
execute as @s[scores={tcc.random=1},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:1}}}]}] run tag @s add tcc.tag
execute as @s[tag=tcc.tag] run playsound minecraft:entity.item.break player @a[distance=..16]
execute as @s[tag=tcc.tag] run particle item minecraft:carrot_on_a_stick{CustomModelData:12} ~ ~1 ~ 0.2 0.2 0.2 0.1 10 force
execute as @s[tag=tcc.tag] run replaceitem entity @s weapon.offhand air


execute as @s[scores={tcc.random=1},tag=!tcc.tag,nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"obsidian_mirror"}}}]}] store result entity @s Inventory[{Slot:-106b}].tag.tcc.Durability int 1 run scoreboard players get @s tcc.math2

execute as @s[scores={tcc.random=1},tag=!tcc.tag,nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"obsidian_mirror"}}}]}] run loot replace entity @s weapon.offhand loot tcc:technical/durability/obsidian_mirror/offhand/store
execute as @s[scores={tcc.random=1},tag=!tcc.tag,nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"obsidian_mirror"}}}]}] run loot replace entity @s weapon.offhand loot tcc:technical/durability/obsidian_mirror/offhand/reorder

execute as @s[tag=tcc.tag] run tag @s remove tcc.tag

scoreboard players reset @s tcc.random
scoreboard players reset @s tcc.math
scoreboard players reset @s tcc.math2
