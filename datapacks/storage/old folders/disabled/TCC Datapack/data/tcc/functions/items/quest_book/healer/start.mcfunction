############################################################
# Description: Starts a quest
# Creator: CreeperMagnet_
############################################################

advancement grant @s only tcc:minecraft/adventure/begin_quest
tag @s add tcc.player.quest.healer.started
advancement grant @s only tcc:technical/items/quest_book/healer/display/root
tellraw @a ["",{"selector":"@s"},{"translate":"tcc.tellraws.quests.start"},{"translate":"tcc.tellraws.quests.healer.title","hoverEvent":{"action":"show_text","value":{"text":"","extra":[{"translate":"advancements.tcc.quests.healer.title"},{"text":"\n"},{"translate":"tcc.tellraws.quests.healer.subtitle"}]}}}]