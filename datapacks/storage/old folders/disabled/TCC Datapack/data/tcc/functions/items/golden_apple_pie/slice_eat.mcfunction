############################################################
# Description: Makes apple pie slices work
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/items/golden_apple_pie_slice
effect give @s regeneration 5 1
effect give @s absorption 120 0
effect give @s saturation 1 2 true
