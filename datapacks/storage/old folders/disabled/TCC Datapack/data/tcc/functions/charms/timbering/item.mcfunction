############################################################
# Description: Commands to run when you're holding a charmed item
# Creator: CreeperMagnet_
############################################################


### Logs
execute as @s[scores={tcc.logbreak1=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:acacia_log"}}] at @s align xyz run function tcc:charms/timbering/acacia_log/main
execute as @s[scores={tcc.logbreak2=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:dark_oak_log"}}] at @s align xyz run function tcc:charms/timbering/dark_oak_log/main
execute as @s[scores={tcc.logbreak3=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:birch_log"}}] at @s align xyz run function tcc:charms/timbering/birch_log/main
execute as @s[scores={tcc.logbreak4=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:oak_log"}}] at @s align xyz run function tcc:charms/timbering/oak_log/main
execute as @s[scores={tcc.logbreak5=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:spruce_log"}}] at @s align xyz run function tcc:charms/timbering/spruce_log/main
execute as @s[scores={tcc.logbreak6=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:jungle_log"}}] at @s align xyz run function tcc:charms/timbering/jungle_log/main
### Stripped Logs
execute as @s[scores={tcc.slogbreak1=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_acacia_log"}}] at @s align xyz run function tcc:charms/timbering/stripped_acacia_log/main
execute as @s[scores={tcc.slogbreak2=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_dark_oak_log"}}] at @s align xyz run function tcc:charms/timbering/stripped_dark_oak_log/main
execute as @s[scores={tcc.slogbreak3=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_birch_log"}}] at @s align xyz run function tcc:charms/timbering/stripped_birch_log/main
execute as @s[scores={tcc.slogbreak4=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_oak_log"}}] at @s align xyz run function tcc:charms/timbering/stripped_oak_log/main
execute as @s[scores={tcc.slogbreak5=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_spruce_log"}}] at @s align xyz run function tcc:charms/timbering/stripped_spruce_log/main
execute as @s[scores={tcc.slogbreak6=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_jungle_log"}}] at @s align xyz run function tcc:charms/timbering/stripped_jungle_log/main
### Bark
execute as @s[scores={tcc.barkbreak1=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:acacia_wood"}}] at @s align xyz run function tcc:charms/timbering/acacia_wood/main
execute as @s[scores={tcc.barkbreak2=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:dark_oak_wood"}}] at @s align xyz run function tcc:charms/timbering/dark_oak_wood/main
execute as @s[scores={tcc.barkbreak3=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:birch_wood"}}] at @s align xyz run function tcc:charms/timbering/birch_wood/main
execute as @s[scores={tcc.barkbreak4=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:oak_wood"}}] at @s align xyz run function tcc:charms/timbering/oak_wood/main
execute as @s[scores={tcc.barkbreak5=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:spruce_wood"}}] at @s align xyz run function tcc:charms/timbering/spruce_wood/main
execute as @s[scores={tcc.barkbreak6=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:jungle_wood"}}] at @s align xyz run function tcc:charms/timbering/jungle_wood/main
### Stripped Bark
execute as @s[scores={tcc.sbarkbreak1=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_acacia_wood"}}] at @s align xyz run function tcc:charms/timbering/stripped_acacia_wood/main
execute as @s[scores={tcc.sbarkbreak2=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_dark_oak_wood"}}] at @s align xyz run function tcc:charms/timbering/stripped_dark_oak_wood/main
execute as @s[scores={tcc.sbarkbreak3=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_birch_wood"}}] at @s align xyz run function tcc:charms/timbering/stripped_birch_wood/main
execute as @s[scores={tcc.sbarkbreak4=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_oak_wood"}}] at @s align xyz run function tcc:charms/timbering/stripped_oak_wood/main
execute as @s[scores={tcc.sbarkbreak5=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_spruce_wood"}}] at @s align xyz run function tcc:charms/timbering/stripped_spruce_wood/main
execute as @s[scores={tcc.sbarkbreak6=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:stripped_jungle_wood"}}] at @s align xyz run function tcc:charms/timbering/stripped_jungle_wood/main
