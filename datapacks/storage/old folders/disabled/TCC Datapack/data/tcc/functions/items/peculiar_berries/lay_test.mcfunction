############################################################
# Description: Tests if a peculiar chickens needs to lay an egg
# Creator: CreeperMagnet_
############################################################

execute store result score @s tcc.math run data get entity @s EggLayTime 1
execute as @s[scores={tcc.math=0..60}] run function tcc:items/peculiar_berries/lay
scoreboard players reset @s tcc.math
