############################################################
# Description: Makes the timbering enchantment give the correct amount of logs
# Creator: CreeperMagnet_
############################################################

execute as @s[scores={tcc.logcount=65..}] at @s run summon item ~ ~ ~ {Item:{id:"minecraft:acacia_log",Count:64}}
execute as @s[scores={tcc.logcount=65..}] run scoreboard players remove @s tcc.logcount 64
execute as @s[scores={tcc.logcount=..64}] store result entity @s Item.Count short 1 run scoreboard players get @s tcc.logcount
execute as @s[scores={tcc.logcount=65..}] run function tcc:charms/timbering/acacia_log/count