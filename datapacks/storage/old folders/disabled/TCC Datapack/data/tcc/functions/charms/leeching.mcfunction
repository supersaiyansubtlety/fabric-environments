############################################################
# Description: Runs when leeching scythe deals more than 4 damage (critical hit)
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/charms/leeching
effect give @s minecraft:regeneration 2 1 true
