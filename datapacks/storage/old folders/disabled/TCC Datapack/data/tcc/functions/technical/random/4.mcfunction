############################################################
# Description: Generates a random number, either 1 or 2
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/2
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome1.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome1.2
function tcc:technical/random/2
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome2.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome2.2
scoreboard players reset @s tcc.random

execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 1
execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 2
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 3
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 4


tag @s remove tcc.random.outcome1.1
tag @s remove tcc.random.outcome1.2
tag @s remove tcc.random.outcome2.1
tag @s remove tcc.random.outcome2.2