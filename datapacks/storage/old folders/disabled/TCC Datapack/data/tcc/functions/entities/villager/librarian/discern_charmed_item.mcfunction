############################################################
# Description: Adds a charmed item trade to a villager
# Creator: CreeperMagnet_
############################################################

tag @s add tcc.villager.librarian.tagged
function tcc:entities/villager/librarian/add_trade
function tcc:entities/villager/librarian/add_trade
function tcc:technical/random/33_64
execute store result entity @s Offers.Recipes[2].buy.Count byte 1 run scoreboard players get @s tcc.random
scoreboard players reset @s tcc.random
function tcc:technical/random/33_64
execute store result entity @s Offers.Recipes[3].buy.Count byte 1 run scoreboard players get @s tcc.random
scoreboard players reset @s tcc.random
