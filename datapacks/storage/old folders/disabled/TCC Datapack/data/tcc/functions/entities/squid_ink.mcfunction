############################################################
# Description: Makes squid spit ink at those who hit them
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/entities/squid_hit
execute as @e[type=squid,nbt={HurtTime:10s}] at @s as @e[distance=..3,type=!squid] at @s if block ~ ~ ~ water run effect give @s blindness 3 0