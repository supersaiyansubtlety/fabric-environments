############################################################
# Description: Damages potion-soaked iron rings
# Creator: CreeperMagnet_
############################################################

execute store result score @s tcc.math run data get entity @s Inventory[{Slot:-106b}].tag.tcc.Durability 1
scoreboard players remove @s tcc.math 1
execute store result entity @s Inventory[{Slot:-106b}].tag.tcc.Durability int 1 run scoreboard players get @s tcc.math
scoreboard players reset @s tcc.math

loot replace entity @s weapon.offhand loot tcc:technical/durability/soaked_rings/iron_ring/offhand/store
loot replace entity @s weapon.offhand loot tcc:technical/durability/soaked_rings/iron_ring/offhand/reorder
