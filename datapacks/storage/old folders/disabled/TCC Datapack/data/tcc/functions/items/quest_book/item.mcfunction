############################################################
# Description: Functions for the quest book item
# Creator: CreeperMagnet_
############################################################

execute as @s[tag=!tcc.player.quest.mage.started] as @s[nbt={SelectedItem:{tag:{tcc:{Quest:"mage"}}}}] run function tcc:items/quest_book/mage/start
execute as @s[tag=!tcc.player.quest.rogue.started] as @s[nbt={SelectedItem:{tag:{tcc:{Quest:"rogue"}}}}] run function tcc:items/quest_book/rogue/start
execute as @s[tag=!tcc.player.quest.warrior.started] as @s[nbt={SelectedItem:{tag:{tcc:{Quest:"warrior"}}}}] run function tcc:items/quest_book/warrior/start
execute as @s[tag=!tcc.player.quest.healer.started] as @s[nbt={SelectedItem:{tag:{tcc:{Quest:"healer"}}}}] run function tcc:items/quest_book/healer/start
execute as @s[tag=!tcc.player.quest.mage.started] as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Quest:"mage"}}}]},nbt=!{SelectedItem:{tag:{tcc:{Item:"quest_book"}}}}] run function tcc:items/quest_book/mage/start
execute as @s[tag=!tcc.player.quest.rogue.started] as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Quest:"rogue"}}}]},nbt=!{SelectedItem:{tag:{tcc:{Item:"quest_book"}}}}] run function tcc:items/quest_book/rogue/start
execute as @s[tag=!tcc.player.quest.warrior.started] as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Quest:"warrior"}}}]},nbt=!{SelectedItem:{tag:{tcc:{Item:"quest_book"}}}}] run function tcc:items/quest_book/warrior/start
execute as @s[tag=!tcc.player.quest.healer.started] as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Quest:"healer"}}}]},nbt=!{SelectedItem:{tag:{tcc:{Item:"quest_book"}}}}] run function tcc:items/quest_book/healer/start

replaceitem entity @s[nbt={SelectedItem:{tag:{tcc:{Item:"quest_book"}}}}] weapon.mainhand air
replaceitem entity @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"quest_book"}}}]},nbt=!{SelectedItem:{tag:{tcc:{Item:"quest_book"}}}}] weapon.mainhand air
playsound minecraft:item.armor.equip_elytra master @a[distance=..16]
