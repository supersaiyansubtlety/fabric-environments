############################################################
# Description: Makes the momentum function work
# Creator: CreeperMagnet_ & dragoncommands
############################################################

playsound minecraft:block.lava.extinguish master @s[scores={tcc.runtime=100..}] ~ ~ ~ 1000 1
execute as @s[scores={tcc.runtime=1000..},gamemode=!creative,gamemode=!spectator] rotated ~ 0 positioned ~ ~1 ~ unless block ^ ^ ^1 #tcc:momentum_no_damage run function tcc:charms/momentum/damage
scoreboard players reset @s tcc.runtime
scoreboard players reset @s tcc.run
scoreboard players reset @s tcc.walk
scoreboard players reset @s tcc.crouch
execute store result entity @s Inventory[{Slot:-106b}].tag.AttributeModifiers[0].Amount int 0 run data get entity @s FoodLevel
