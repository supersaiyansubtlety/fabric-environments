############################################################
# Description: Gets rid of all scoreboards and other things
# Creator: CreeperMagnet_
############################################################

tellraw @a ["",{"text":"\n"},{"translate":"tcc.tellraws.terminate.1","hoverEvent":{"action":"show_text","value":{"translate":"tcc.tellraws.initiate.5"}},"clickEvent":{"action":"open_url","value":"https://thecreeperscode.com"},"color":"dark_green","underlined":"true"},{"translate":"tcc.tellraws.terminate.2","color":"dark_red","clickEvent":{"action":"open_url","value":"http://thecreeperscode.com/download"}},{"text":"\n\n"},{"translate":"tcc.tellraws.terminate.3","color":"dark_aqua"},{"text":"\n"}]


scoreboard objectives remove tcc.carrotstick


scoreboard objectives remove tcc.boomerang
scoreboard objectives remove tcc.boomerangu
scoreboard objectives remove tcc.boomerangm1
scoreboard objectives remove tcc.boomerangm2

scoreboard objectives remove tcc.wrench
scoreboard objectives remove tcc.logcount

scoreboard objectives remove tcc.logbreak1
scoreboard objectives remove tcc.logbreak2
scoreboard objectives remove tcc.logbreak3
scoreboard objectives remove tcc.logbreak4
scoreboard objectives remove tcc.logbreak5
scoreboard objectives remove tcc.logbreak6
scoreboard objectives remove tcc.slogbreak1
scoreboard objectives remove tcc.slogbreak2
scoreboard objectives remove tcc.slogbreak3
scoreboard objectives remove tcc.slogbreak4
scoreboard objectives remove tcc.slogbreak5
scoreboard objectives remove tcc.slogbreak6
scoreboard objectives remove tcc.barkbreak1
scoreboard objectives remove tcc.barkbreak2
scoreboard objectives remove tcc.barkbreak3
scoreboard objectives remove tcc.barkbreak4
scoreboard objectives remove tcc.barkbreak5
scoreboard objectives remove tcc.barkbreak6
scoreboard objectives remove tcc.sbarkbreak1
scoreboard objectives remove tcc.sbarkbreak2
scoreboard objectives remove tcc.sbarkbreak3
scoreboard objectives remove tcc.sbarkbreak4
scoreboard objectives remove tcc.sbarkbreak5
scoreboard objectives remove tcc.sbarkbreak6
scoreboard objectives remove tcc.mineironore
scoreboard objectives remove tcc.minegoldore
scoreboard objectives remove tcc.minediamond
scoreboard objectives remove tcc.minecoalore
scoreboard objectives remove tcc.mineemerald
scoreboard objectives remove tcc.minequartz
scoreboard objectives remove tcc.mineredstone
scoreboard objectives remove tcc.minelapisore
scoreboard objectives remove tcc.minediorite
scoreboard objectives remove tcc.mineandesite
scoreboard objectives remove tcc.minegranite
scoreboard objectives remove tcc.minewheat
scoreboard objectives remove tcc.minebeets
scoreboard objectives remove tcc.minecarrots
scoreboard objectives remove tcc.minepotatoes
scoreboard objectives remove tcc.minechorus1
scoreboard objectives remove tcc.minechorus2

scoreboard objectives remove tcc.random
scoreboard objectives remove tcc.math
scoreboard objectives remove tcc.math2
scoreboard objectives remove tcc.installed

scoreboard objectives remove tcc.useelytra
scoreboard objectives remove tcc.shifttime
scoreboard objectives remove tcc.hunger
scoreboard objectives remove tcc.health
scoreboard objectives remove tcc.run
scoreboard objectives remove tcc.walk
scoreboard objectives remove tcc.crouch
scoreboard objectives remove tcc.runtime

scoreboard objectives remove tcc.current_xp
scoreboard objectives remove tcc.old_xp

datapack disable "file/TCC Datapack"
