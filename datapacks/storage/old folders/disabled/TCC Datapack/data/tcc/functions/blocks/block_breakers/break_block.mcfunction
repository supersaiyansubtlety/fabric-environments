############################################################
# Description: Tells what type of block breaker is powered
# Creator: CreeperMagnet_
############################################################

execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.up,type=armor_stand] at @s if block ~ ~ ~ #tcc:excavator_breakable run loot insert ~ ~-1 ~ mine ~ ~ ~ minecraft:diamond_pickaxe
execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.up,type=armor_stand] at @s if block ~ ~ ~ #tcc:excavator_breakable run setblock ~ ~ ~ air destroy

execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.down,type=armor_stand] at @s if block ~ ~-2 ~ #tcc:excavator_breakable run loot insert ~ ~-1 ~ mine ~ ~-2 ~ minecraft:diamond_pickaxe
execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.down,type=armor_stand] at @s if block ~ ~-2 ~ #tcc:excavator_breakable run setblock ~ ~-2 ~ air destroy

execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.north,type=armor_stand] at @s if block ~ ~-1 ~-1 #tcc:excavator_breakable run loot insert ~ ~-1 ~ mine ~ ~-1 ~-1 minecraft:diamond_pickaxe
execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.north,type=armor_stand] at @s if block ~ ~-1 ~-1 #tcc:excavator_breakable run setblock ~ ~-1 ~-1 air destroy

execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.south,type=armor_stand] at @s if block ~ ~-1 ~1 #tcc:excavator_breakable run loot insert ~ ~-1 ~ mine ~ ~-1 ~1 minecraft:diamond_pickaxe
execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.south,type=armor_stand] at @s if block ~ ~-1 ~1 #tcc:excavator_breakable run setblock ~ ~-1 ~1 air destroy

execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.east,type=armor_stand] at @s if block ~1 ~-1 ~ #tcc:excavator_breakable run loot insert ~ ~-1 ~ mine ~1 ~-1 ~ minecraft:diamond_pickaxe
execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.east,type=armor_stand] at @s if block ~1 ~-1 ~ #tcc:excavator_breakable run setblock ~1 ~-1 ~ air destroy

execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.west,type=armor_stand] at @s if block ~-1 ~-1 ~ #tcc:excavator_breakable run loot insert ~ ~-1 ~ mine ~-1 ~-1 ~ minecraft:diamond_pickaxe
execute as @e[tag=tcc.blocks.excavator,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.west,type=armor_stand] at @s if block ~-1 ~-1 ~ #tcc:excavator_breakable run setblock ~-1 ~-1 ~ air destroy



execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.up,type=armor_stand] at @s if block ~ ~ ~ #tcc:sifter_breakable run loot insert ~ ~-1 ~ mine ~ ~ ~ minecraft:diamond_shovel
execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.up,type=armor_stand] at @s if block ~ ~ ~ #tcc:sifter_breakable run setblock ~ ~ ~ air destroy

execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.down,type=armor_stand] at @s if block ~ ~-2 ~ #tcc:sifter_breakable run loot insert ~ ~-1 ~ mine ~ ~-2 ~ minecraft:diamond_shovel
execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.down,type=armor_stand] at @s if block ~ ~-2 ~ #tcc:sifter_breakable run setblock ~ ~-2 ~ air destroy

execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.north,type=armor_stand] at @s if block ~ ~-1 ~-1 #tcc:sifter_breakable run loot insert ~ ~-1 ~ mine ~ ~-1 ~-1 minecraft:diamond_shovel
execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.north,type=armor_stand] at @s if block ~ ~-1 ~-1 #tcc:sifter_breakable run setblock ~ ~-1 ~-1 air destroy

execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.south,type=armor_stand] at @s if block ~ ~-1 ~1 #tcc:sifter_breakable run loot insert ~ ~-1 ~ mine ~ ~-1 ~1 minecraft:diamond_shovel
execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.south,type=armor_stand] at @s if block ~ ~-1 ~1 #tcc:sifter_breakable run setblock ~ ~-1 ~1 air destroy

execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.east,type=armor_stand] at @s if block ~1 ~-1 ~ #tcc:sifter_breakable run loot insert ~ ~-1 ~ mine ~1 ~-1 ~ minecraft:diamond_shovel
execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.east,type=armor_stand] at @s if block ~1 ~-1 ~ #tcc:sifter_breakable run setblock ~1 ~-1 ~ air destroy

execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.west,type=armor_stand] at @s if block ~-1 ~-1 ~ #tcc:sifter_breakable run loot insert ~ ~-1 ~ mine ~-1 ~-1 ~ minecraft:diamond_shovel
execute as @e[tag=tcc.blocks.sifter,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.west,type=armor_stand] at @s if block ~-1 ~-1 ~ #tcc:sifter_breakable run setblock ~-1 ~-1 ~ air destroy


execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.up,type=armor_stand] at @s if block ~ ~ ~ #tcc:chopper_breakable run loot insert ~ ~-1 ~ mine ~ ~ ~ minecraft:diamond_axe
execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.up,type=armor_stand] at @s if block ~ ~ ~ #tcc:chopper_breakable run setblock ~ ~ ~ air destroy

execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.down,type=armor_stand] at @s if block ~ ~-2 ~ #tcc:chopper_breakable run loot insert ~ ~-1 ~ mine ~ ~-2 ~ minecraft:diamond_axe
execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.down,type=armor_stand] at @s if block ~ ~-2 ~ #tcc:chopper_breakable run setblock ~ ~-2 ~ air destroy

execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.north,type=armor_stand] at @s if block ~ ~-1 ~-1 #tcc:chopper_breakable run loot insert ~ ~-1 ~ mine ~ ~-1 ~-1 minecraft:diamond_axe
execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.north,type=armor_stand] at @s if block ~ ~-1 ~-1 #tcc:chopper_breakable run setblock ~ ~-1 ~-1 air destroy

execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.south,type=armor_stand] at @s if block ~ ~-1 ~1 #tcc:chopper_breakable run loot insert ~ ~-1 ~ mine ~ ~-1 ~1 minecraft:diamond_axe
execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.south,type=armor_stand] at @s if block ~ ~-1 ~1 #tcc:chopper_breakable run setblock ~ ~-1 ~1 air destroy

execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.east,type=armor_stand] at @s if block ~1 ~-1 ~ #tcc:chopper_breakable run loot insert ~ ~-1 ~ mine ~1 ~-1 ~ minecraft:diamond_axe
execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.east,type=armor_stand] at @s if block ~1 ~-1 ~ #tcc:chopper_breakable run setblock ~1 ~-1 ~ air destroy

execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.west,type=armor_stand] at @s if block ~-1 ~-1 ~ #tcc:chopper_breakable run loot insert ~ ~-1 ~ mine ~-1 ~-1 ~ minecraft:diamond_axe
execute as @e[tag=tcc.blocks.chopper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.west,type=armor_stand] at @s if block ~-1 ~-1 ~ #tcc:chopper_breakable run setblock ~-1 ~-1 ~ air destroy


execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.up,type=armor_stand] at @s if block ~ ~ ~ #tcc:snipper_breakable run loot insert ~ ~-1 ~ mine ~ ~ ~ minecraft:shears
execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.up,type=armor_stand] at @s if block ~ ~ ~ #tcc:snipper_breakable run setblock ~ ~ ~ air destroy

execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.down,type=armor_stand] at @s if block ~ ~-2 ~ #tcc:snipper_breakable run loot insert ~ ~-1 ~ mine ~ ~-2 ~ minecraft:shears
execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.down,type=armor_stand] at @s if block ~ ~-2 ~ #tcc:snipper_breakable run setblock ~ ~-2 ~ air destroy

execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.north,type=armor_stand] at @s if block ~ ~-1 ~-1 #tcc:snipper_breakable run loot insert ~ ~-1 ~ mine ~ ~-1 ~-1 minecraft:shears
execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.north,type=armor_stand] at @s if block ~ ~-1 ~-1 #tcc:snipper_breakable run setblock ~ ~-1 ~-1 air destroy

execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.south,type=armor_stand] at @s if block ~ ~-1 ~1 #tcc:snipper_breakable run loot insert ~ ~-1 ~ mine ~ ~-1 ~1 minecraft:shears
execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.south,type=armor_stand] at @s if block ~ ~-1 ~1 #tcc:snipper_breakable run setblock ~ ~-1 ~1 air destroy

execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.east,type=armor_stand] at @s if block ~1 ~-1 ~ #tcc:snipper_breakable run loot insert ~ ~-1 ~ mine ~1 ~-1 ~ minecraft:shears
execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.east,type=armor_stand] at @s if block ~1 ~-1 ~ #tcc:snipper_breakable run setblock ~1 ~-1 ~ air destroy

execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.west,type=armor_stand] at @s if block ~-1 ~-1 ~ #tcc:snipper_breakable run loot insert ~ ~-1 ~ mine ~-1 ~-1 ~ minecraft:shears
execute as @e[tag=tcc.blocks.snipper,tag=tcc.scheduled.block_breaker,tag=tcc.block_breakers.west,type=armor_stand] at @s if block ~-1 ~-1 ~ #tcc:snipper_breakable run setblock ~-1 ~-1 ~ air destroy



execute as @e[tag=tcc.scheduled.block_breaker,type=armor_stand] at @s run kill @e[type=item,sort=nearest,distance=..4,nbt={PickupDelay:10s}]
tag @e[tag=tcc.scheduled.block_breaker,type=armor_stand] remove tcc.scheduled.block_breaker
