############################################################
# Description: Makes the 'Creeper Magnet' advancement work.
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:minecraft/story/creeper_magnet_trigger
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Charms:["attraction"]}}}]},advancements={tcc:minecraft/story/creeper_magnet_display=false}] run advancement grant @s only tcc:minecraft/story/creeper_magnet_display