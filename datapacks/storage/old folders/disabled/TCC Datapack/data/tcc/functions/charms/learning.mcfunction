############################################################
# Description: Commands to run whenever a player with a learning sword kills something
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/charms/learning
xp add @s 1 points
playsound minecraft:entity.player.levelup player @s
