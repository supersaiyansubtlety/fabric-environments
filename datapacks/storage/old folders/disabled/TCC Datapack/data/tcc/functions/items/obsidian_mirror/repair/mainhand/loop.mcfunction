############################################################
# Description: Repairs an obsidian mirror
# Creator: CreeperMagnet_
############################################################

execute as @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:20,Item:"obsidian_mirror"}}}}] run function tcc:items/obsidian_mirror/repair/mainhand/add_durability
xp add @s -1 points
scoreboard players add @s tcc.old_xp 1
execute as @s[nbt={SelectedItem:{tag:{tcc:{Durability:20,Item:"obsidian_mirror"}}}}] run scoreboard players set @s tcc.old_xp 0
execute as @s[scores={tcc.old_xp=0}] run function tcc:items/obsidian_mirror/repair/mainhand/merge_lore
execute as @s[scores={tcc.old_xp=0}] run scoreboard players reset @s tcc.old_xp
execute as @s[scores={tcc.old_xp=..-1},nbt=!{SelectedItem:{tag:{tcc:{Durability:20,Item:"obsidian_mirror"}}}}] run function tcc:items/obsidian_mirror/repair/mainhand/loop
