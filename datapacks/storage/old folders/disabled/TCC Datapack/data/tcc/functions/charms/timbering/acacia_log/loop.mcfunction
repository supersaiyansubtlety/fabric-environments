############################################################
# Description: Makes the timbering enchantment find logs above it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:acacia_log ~ ~ ~ 0.5 0.5 0.5 1 1 force
setblock ~ ~ ~ air
scoreboard players add @s tcc.logcount 1
execute positioned ~ ~1 ~ if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ acacia_log if entity @a[scores={tcc.logbreak1=1..},distance=..10] run function tcc:charms/timbering/acacia_log/loop
execute unless block ~ ~1 ~-1 acacia_log unless block ~ ~1 ~1 acacia_log unless block ~ ~1 ~ acacia_log unless block ~1 ~1 ~ acacia_log unless block ~-1 ~1 ~ acacia_log unless block ~1 ~ ~ acacia_log unless block ~-1 ~ ~ acacia_log unless block ~ ~ ~1 acacia_log unless block ~ ~ ~-1 acacia_log run function tcc:charms/timbering/acacia_log/count