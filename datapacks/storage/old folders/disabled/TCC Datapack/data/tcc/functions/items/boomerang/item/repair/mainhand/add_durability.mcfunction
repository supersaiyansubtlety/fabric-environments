############################################################
# Description: Repairs an item
# Creator: CreeperMagnet_
############################################################

execute store result score @s tcc.math run data get entity @s SelectedItem.tag.tcc.Durability 1
scoreboard players add @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:299}}}},nbt=!{SelectedItem:{tag:{tcc:{Durability:300}}}}] tcc.math 2
scoreboard players add @s[nbt={SelectedItem:{tag:{tcc:{Durability:299}}}},nbt=!{SelectedItem:{tag:{tcc:{Durability:300}}}}] tcc.math 1
execute store result entity @s SelectedItem.tag.tcc.Durability int 1 run scoreboard players get @s tcc.math
scoreboard players reset @s tcc.math
