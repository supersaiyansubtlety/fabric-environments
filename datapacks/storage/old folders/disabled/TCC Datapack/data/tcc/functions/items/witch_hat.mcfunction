############################################################
# Description: Commands for witch hat
# Creator: CreeperMagnet_
############################################################

replaceitem entity @s[gamemode=!creative,nbt=!{SelectedItem:{tag:{tcc:{Item:"witch_hat"}}}},nbt={Inventory:[{tag:{tcc:{Item:"witch_hat"}},Slot:-106b}]}] weapon.offhand air
replaceitem entity @s[gamemode=!creative,nbt={SelectedItem:{tag:{tcc:{Item:"witch_hat"}}}}] weapon.mainhand air

playsound minecraft:item.armor.equip_elytra master @a[distance=..16]
loot replace entity @s armor.head loot tcc:items/witch_hat
advancement grant @s only tcc:minecraft/nether/witch_hat
