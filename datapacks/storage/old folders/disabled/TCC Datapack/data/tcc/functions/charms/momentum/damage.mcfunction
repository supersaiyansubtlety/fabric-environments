############################################################
# Description: Damages you when you hit a wall using the momentum ring
# Creator: CreeperMagnet_
############################################################
execute as @s[scores={tcc.health=13..}] run effect give @s instant_damage 1 1 true
execute as @s[scores={tcc.health=..12}] run gamerule showDeathMessages false
execute as @s[scores={tcc.health=..12}] run tellraw @a {"translate":"death.tcc.momentum_ring","with":[{"selector":"@s"}]}
execute as @s[scores={tcc.health=..12}] run kill @s
execute as @s[scores={tcc.health=..12}] run gamerule showDeathMessages true
