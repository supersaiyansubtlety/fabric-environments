############################################################
# Description: Starts a quest
# Creator: CreeperMagnet_
############################################################

advancement grant @s only tcc:minecraft/adventure/begin_quest
tag @s add tcc.player.quest.rogue.started
advancement grant @s only tcc:technical/items/quest_book/rogue/display/root
tellraw @a ["",{"selector":"@s"},{"translate":"tcc.tellraws.quests.start"},{"translate":"tcc.tellraws.quests.rogue.title","hoverEvent":{"action":"show_text","value":{"text":"","extra":[{"translate":"advancements.tcc.quests.rogue.title"},{"text":"\n"},{"translate":"tcc.tellraws.quests.rogue.subtitle"}]}}}]