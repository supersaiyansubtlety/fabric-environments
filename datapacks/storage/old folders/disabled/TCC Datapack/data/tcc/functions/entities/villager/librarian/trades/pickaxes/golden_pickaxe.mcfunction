############################################################
# Description: Sets a random pickaxe charm
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/3
execute as @s[scores={tcc.random=1}] run data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:golden_pickaxe",Count:1b},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:golden_pickaxe",tag:{tcc:{Charms:["veining"],Tags:["charmed_item_tick"]},CustomModelData:1,display:{Lore:["{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.veining\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}
execute as @s[scores={tcc.random=2}] run data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:golden_pickaxe",Count:1b},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:golden_pickaxe",tag:{tcc:{Charms:["smoldering"]},CustomModelData:1,display:{Lore:["{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.smoldering\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}
execute as @s[scores={tcc.random=3}] run data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:golden_pickaxe",Count:1b},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:golden_pickaxe",tag:{tcc:{Charms:["smoldering","veining"],Tags:["charmed_item_tick"]},CustomModelData:1,display:{Lore:["{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.smoldering\"}]}","{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.veining\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}
scoreboard players reset @s tcc.random
