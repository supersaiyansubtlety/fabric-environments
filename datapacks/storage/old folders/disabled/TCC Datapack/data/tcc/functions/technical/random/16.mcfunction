############################################################
# Description: Generates a random number, either 1 or 2
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/8
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome1.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome1.2
execute as @s[scores={tcc.random=3}] run tag @s add tcc.random.outcome1.3
execute as @s[scores={tcc.random=4}] run tag @s add tcc.random.outcome1.4
execute as @s[scores={tcc.random=5}] run tag @s add tcc.random.outcome1.5
execute as @s[scores={tcc.random=6}] run tag @s add tcc.random.outcome1.6
execute as @s[scores={tcc.random=7}] run tag @s add tcc.random.outcome1.7
execute as @s[scores={tcc.random=8}] run tag @s add tcc.random.outcome1.8
function tcc:technical/random/2
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome2.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome2.2
scoreboard players reset @s tcc.random

execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 1
execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 2
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 3
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 4
execute as @s[tag=tcc.random.outcome1.3,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 5
execute as @s[tag=tcc.random.outcome1.3,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 6
execute as @s[tag=tcc.random.outcome1.4,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 7
execute as @s[tag=tcc.random.outcome1.4,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 8
execute as @s[tag=tcc.random.outcome1.5,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 9
execute as @s[tag=tcc.random.outcome1.5,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 10
execute as @s[tag=tcc.random.outcome1.6,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 11
execute as @s[tag=tcc.random.outcome1.6,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 12
execute as @s[tag=tcc.random.outcome1.7,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 13
execute as @s[tag=tcc.random.outcome1.7,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 14
execute as @s[tag=tcc.random.outcome1.8,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 15
execute as @s[tag=tcc.random.outcome1.8,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 16


tag @s remove tcc.random.outcome1.1
tag @s remove tcc.random.outcome1.2
tag @s remove tcc.random.outcome1.3
tag @s remove tcc.random.outcome1.4
tag @s remove tcc.random.outcome1.5
tag @s remove tcc.random.outcome1.6
tag @s remove tcc.random.outcome1.7
tag @s remove tcc.random.outcome1.8
tag @s remove tcc.random.outcome2.1
tag @s remove tcc.random.outcome2.2