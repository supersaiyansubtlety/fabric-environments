############################################################
# Description: Commands to run every tick for charms
# Creator: CreeperMagnet_
############################################################

## Timbering
execute as @s[nbt={SelectedItem:{tag:{tcc:{Charms:["timbering"]}}}}] run function tcc:charms/timbering/item

## Veining
execute as @s[nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] run function tcc:charms/veining/item

## Obsidian Scythes
execute as @s[scores={tcc.minebeets=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["reaping"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:beetroot"}},distance=..20] at @s align xyz run function tcc:charms/reaping/beetroots/main
execute as @s[scores={tcc.minewheat=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["reaping"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:wheat"}},distance=..20] at @s align xyz run function tcc:charms/reaping/wheat/main
execute as @s[scores={tcc.minecarrots=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["reaping"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:carrot"}},distance=..20] at @s align xyz run function tcc:charms/reaping/carrots/main
execute as @s[scores={tcc.minepotatoes=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["reaping"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:potato"}},distance=..20] at @s align xyz run function tcc:charms/reaping/potatoes/main
