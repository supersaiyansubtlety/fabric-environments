############################################################
# Description: Helps the area effect cloud see what types of items it's dealing with
# Creator: CreeperMagnet_
############################################################

execute as @e[type=item,dx=4,dz=4,dy=0,nbt={Item:{id:"minecraft:diamond_sword"}},nbt=!{Item:{tag:{tcc:{Item:"obsidian_scythe"}}}},nbt=!{Item:{tag:{tcc:{Item:"tipped_sword"}}}}] run tag @s add tcc.tipped_swords.can_tip
execute as @e[type=item,dx=4,dz=4,dy=0,nbt={Item:{id:"minecraft:golden_sword"}},nbt=!{Item:{tag:{tcc:{Item:"tipped_sword"}}}}] run tag @s add tcc.tipped_swords.can_tip
execute as @e[type=item,dx=4,dz=4,dy=0,nbt={Item:{id:"minecraft:wooden_sword"}},nbt=!{Item:{tag:{tcc:{Item:"tipped_sword"}}}}] run tag @s add tcc.tipped_swords.can_tip
execute as @e[type=item,dx=4,dz=4,dy=0,nbt={Item:{id:"minecraft:iron_sword"}},nbt=!{Item:{tag:{tcc:{Item:"tipped_sword"}}}}] run tag @s add tcc.tipped_swords.can_tip
execute as @e[type=item,dx=4,dz=4,dy=0,nbt={Item:{id:"minecraft:stone_sword"}},nbt=!{Item:{tag:{tcc:{Item:"tipped_sword"}}}}] run tag @s add tcc.tipped_swords.can_tip

execute as @e[type=item,dx=4,dz=4,dy=0,nbt={Item:{id:"minecraft:repeating_command_block",Count:1b}},nbt=!{Item:{tag:{tcc:{Item:"soaked_iron_ring"}}}}] run tag @s add tcc.soaked_iron_rings.can_soak
execute as @e[type=item,dx=4,dz=4,dy=0,nbt={Item:{tag:{tcc:{Item:"golden_ring"}}}},nbt=!{Item:{tag:{tcc:{Item:"soaked_golden_ring"}}}}] run tag @s add tcc.soaked_golden_rings.can_soak

execute as @e[type=item,dx=4,dz=4,dy=0,nbt={Item:{id:"minecraft:potato"}},nbt=!{Item:{tag:{tcc:{Item:"potion_potato"}}}}] run tag @s add tcc.potion_potatoes.can_soak




execute if entity @e[type=item,dx=4,dz=4,dy=0,tag=tcc.potion_potatoes.can_soak,nbt=!{Item:{tag:{tcc:{Item:"potion_potato"}}}}] run function tcc:items/potion_items/potion_potatoes/soak_item

execute if entity @e[type=item,dx=4,dz=4,dy=0,tag=tcc.soaked_iron_rings.can_soak] run function tcc:items/potion_items/soaked_rings/iron_ring/soak_item
execute if entity @e[type=item,dx=4,dz=4,dy=0,tag=tcc.soaked_golden_rings.can_soak] run function tcc:items/potion_items/soaked_rings/golden_ring/soak_item

execute if entity @e[type=item,dx=4,dz=4,dy=0,tag=tcc.tipped_swords.can_tip,nbt=!{Item:{tag:{tcc:{Item:"tipped_sword"}}}}] run function tcc:items/potion_items/tipped_swords/tip_item
