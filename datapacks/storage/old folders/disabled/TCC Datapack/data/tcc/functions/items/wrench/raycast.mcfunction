############################################################
# Description: Finds the block you are looking at
# Creator: CreeperMagnet_
############################################################

scoreboard players add @s[scores={tcc.wrench=..499}] tcc.wrench 1
execute as @s[scores={tcc.wrench=..499}] anchored eyes unless block ^ ^ ^0.01 #tcc:wrench_ignore positioned ^ ^ ^0.01 run function tcc:items/wrench/rotate
execute as @s[scores={tcc.wrench=..499}] anchored eyes if block ^ ^ ^0.01 #tcc:wrench_ignore anchored feet positioned ^ ^ ^0.01 run function tcc:items/wrench/raycast