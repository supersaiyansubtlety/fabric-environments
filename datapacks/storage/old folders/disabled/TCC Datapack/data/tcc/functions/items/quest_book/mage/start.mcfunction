############################################################
# Description: Starts a quest
# Creator: CreeperMagnet_
############################################################

advancement grant @s only tcc:minecraft/adventure/begin_quest
tag @s add tcc.player.quest.mage.started
advancement grant @s only tcc:technical/items/quest_book/mage/display/root
tellraw @a ["",{"selector":"@s"},{"translate":"tcc.tellraws.quests.start"},{"translate":"tcc.tellraws.quests.mage.title","hoverEvent":{"action":"show_text","value":{"text":"","extra":[{"translate":"advancements.tcc.quests.mage.title"},{"text":"\n"},{"translate":"tcc.tellraws.quests.mage.subtitle"}]}}}]