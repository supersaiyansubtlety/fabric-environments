############################################################
# Description: Sets a random sword charm
# Creator: CreeperMagnet_
############################################################

data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:golden_sword",Count:1b},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:golden_sword",tag:{tcc:{Charms:["learning"]},CustomModelData:1,display:{Lore:["{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.learning\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}
scoreboard players reset @s tcc.random
