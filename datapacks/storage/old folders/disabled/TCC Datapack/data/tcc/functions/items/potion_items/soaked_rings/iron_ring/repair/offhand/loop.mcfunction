############################################################
# Description: Repairs a soaked iron ring
# Creator: CreeperMagnet_
############################################################

execute as @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:50,Item:"soaked_iron_ring"}}}]}] run function tcc:items/potion_items/soaked_rings/iron_ring/repair/offhand/add_durability
xp add @s -1 points
scoreboard players add @s tcc.old_xp 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:50,Item:"soaked_iron_ring"}}}]}] run scoreboard players set @s tcc.old_xp 0
execute as @s[scores={tcc.old_xp=0}] run function tcc:items/potion_items/soaked_rings/iron_ring/repair/offhand/merge_lore
execute as @s[scores={tcc.old_xp=0}] run scoreboard players reset @s tcc.old_xp
execute as @s[scores={tcc.old_xp=..-1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:50,Item:"soaked_iron_ring"}}}]}] run function tcc:items/potion_items/soaked_rings/iron_ring/repair/offhand/loop
