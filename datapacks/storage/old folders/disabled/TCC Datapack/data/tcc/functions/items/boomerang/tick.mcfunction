############################################################
# Description: Commands for boomerang moving and things that need to run per tick
# Creator: CreeperMagnet_
############################################################

## Flying
scoreboard players add @s tcc.boomerang 0

execute as @s[scores={tcc.boomerang=..50}] at @s run function tcc:items/boomerang/move/forward
execute as @s[scores={tcc.boomerang=50..119}] at @s run function tcc:items/boomerang/move/backward

## Time Out
execute as @s[scores={tcc.boomerang=120}] at @s run function tcc:items/boomerang/time_out

## Sound
execute as @s[scores={tcc.boomerang=1}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=4}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=7}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=10}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=13}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=16}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=19}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=22}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=25}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=28}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=31}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=34}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=37}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=40}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=43}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=46}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=49}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=52}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=55}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=58}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=61}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=64}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=67}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=70}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=73}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=75}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=78}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=81}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=84}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=87}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=90}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=93}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=96}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=99}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=102}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=105}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=108}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=111}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=113}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=116}] at @s run function tcc:items/boomerang/sound
execute as @s[scores={tcc.boomerang=119}] at @s run function tcc:items/boomerang/sound