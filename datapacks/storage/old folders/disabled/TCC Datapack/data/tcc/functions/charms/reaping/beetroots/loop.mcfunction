############################################################
# Description: Makes the reaping enchantment find beets around it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:beetroots ~ ~ ~ 0.5 0.5 0.5 1 1 force
execute as @p[scores={tcc.minebeets=1..},distance=..4] run loot spawn ~ ~ ~ mine ~ ~ ~ mainhand
setblock ~ ~ ~ air
execute positioned ~ ~1 ~ if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ minecraft:beetroots[age=3] if entity @a[scores={tcc.minebeets=1..},distance=..4] run function tcc:charms/reaping/beetroots/loop
