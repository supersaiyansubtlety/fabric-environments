############################################################
# Description: Commands for picking up a boomerang
# Creator: CreeperMagnet_
############################################################

tag @s[tag=tcc.boomerang.player.uuid_match] remove tcc.boomerang.player.uuid_match
scoreboard players set @s tcc.math 0

execute store result score @s tcc.math run data get entity @s UUIDLeast 0.0000000001
scoreboard players operation @e[type=armor_stand,distance=..1,limit=1,sort=nearest,tag=tcc.boomerang] tcc.math = @e[type=armor_stand,distance=..1,limit=1,sort=nearest,tag=tcc.boomerang] tcc.boomerangu
scoreboard players operation @s tcc.math -= @e[distance=..1,limit=1,sort=nearest,tag=tcc.boomerang] tcc.math

tag @s[scores={tcc.math=0}] add tcc.boomerang.player.uuid_match


execute as @s[tag=tcc.boomerang.player.uuid_match] as @e[distance=..1,limit=1,sort=nearest,tag=tcc.boomerang,scores={tcc.boomerang=50..119}] at @s run function tcc:items/boomerang/item/catch/item
execute as @s[tag=tcc.boomerang.player.uuid_match] run kill @e[distance=..1,limit=1,sort=nearest,tag=tcc.boomerang,scores={tcc.boomerang=50..119}]
