############################################################
# Description: Moves the boomerang forward
# Creator: CreeperMagnet_
############################################################

scoreboard players add @s tcc.boomerangm1 0

execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]}] at @s if block ^ ^ ^0.05 #tcc:not_solid unless entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run tp @s ^ ^ ^0.05
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] at @s unless block ^ ^ ^0.05 #tcc:not_solid positioned ^ ^ ^-0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] at @s unless block ^ ^ ^0.05 #tcc:not_solid positioned ^ ^ ^ run function tcc:items/boomerang/charms/bouncing/hit_block
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] at @s positioned ^ ^ ^0.05 if entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] positioned ^ ^ ^-0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] at @s positioned ^ ^ ^0.05 if entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run function tcc:items/boomerang/charms/bouncing/hit_block


execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]}] at @s if block ^ ^ ^0.05 #tcc:boomerang_flowing unless entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run tp @s ^ ^ ^0.05
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] at @s unless block ^ ^ ^0.05 #tcc:boomerang_flowing positioned ^ ^ ^-0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] at @s unless block ^ ^ ^0.05 #tcc:boomerang_flowing positioned ^ ^ ^ run function tcc:items/boomerang/charms/bouncing/hit_block
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt=!{ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] at @s positioned ^ ^ ^0.05 if entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] positioned ^ ^ ^-0.5 run function tcc:items/boomerang/hit_block
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]},nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] at @s positioned ^ ^ ^0.05 if entity @e[type=shulker,nbt={Peek:0b},dx=0,dz=0,dy=0,limit=1,sort=nearest] run function tcc:items/boomerang/charms/bouncing/hit_block


execute as @e[type=!#tcc:boomerang_damage/ignore,nbt={HurtTime:0s},distance=..10,tag=!tcc.boomerang.player.uuid_match] at @s run function tcc:items/boomerang/hurt/discern


execute as @s[scores={tcc.boomerangm1=0..19}] run scoreboard players add @s tcc.boomerangm1 1
execute as @s[scores={tcc.boomerangm1=0..19}] run function tcc:items/boomerang/move/forward
execute as @s[scores={tcc.boomerangm1=20}] run scoreboard players add @s tcc.boomerang 1
execute as @s[scores={tcc.boomerangm1=20}] run scoreboard players reset @s tcc.boomerangm1
