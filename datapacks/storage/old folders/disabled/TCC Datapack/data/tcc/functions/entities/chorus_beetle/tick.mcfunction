############################################################
# Description: Commands to run every tick for the chorus beetle
# Creator: CreeperMagnet_
############################################################

replaceitem entity @s[nbt={HurtTime:0s},nbt={Motion:[0.0,0.0,0.0]}] armor.head structure_block{CustomModelData:14}
replaceitem entity @s[nbt={HurtTime:0s},nbt=!{Motion:[0.0,0.0,0.0],OnGround:1b}] armor.head structure_block{CustomModelData:13}
execute as @s[nbt=!{HurtTime:0s}] run replaceitem entity @s armor.head structure_block{CustomModelData:12}
execute as @s[nbt={HurtTime:10s}] run playsound entity.endermite.hurt hostile @a[distance=..16] ~ ~ ~ 16 0
execute as @s[nbt={Anger:399s}] run function tcc:entities/chorus_beetle/charge
execute as @s[nbt={Anger:499s}] run function tcc:entities/chorus_beetle/charge
execute as @s[nbt={Anger:599s}] run function tcc:entities/chorus_beetle/charge
execute as @s[nbt={Anger:699s}] run function tcc:entities/chorus_beetle/charge
execute as @s[nbt={Anger:799s}] run function tcc:entities/chorus_beetle/charge
execute as @s[nbt={ActiveEffects:[{Id:1b,Duration:10,Amplifier:4b,ShowParticles:0b,Ambient:0b}]}] run function tcc:entities/chorus_beetle/charge_done
