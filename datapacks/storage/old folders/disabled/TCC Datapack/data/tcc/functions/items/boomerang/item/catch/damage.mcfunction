############################################################
# Description: Spawns a boomerang item with accurate nbt and damage
# Creator: CreeperMagnet_
############################################################

execute as @s[nbt=!{ArmorItems:[{},{},{},{tag:{Enchantments:[{id:"minecraft:unbreaking"}]}}]}] run scoreboard players set @s tcc.random 1
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{Enchantments:[{lvl:1s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/2
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{Enchantments:[{lvl:2s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/3
execute as @s[nbt={ArmorItems:[{},{},{},{tag:{Enchantments:[{lvl:3s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/4

execute as @s[scores={tcc.random=1}] store result score @s tcc.math run data get entity @s ArmorItems[3].tag.tcc.Durability 1
execute as @s[scores={tcc.random=1}] run scoreboard players remove @s tcc.math 1
execute as @s[scores={tcc.random=1}] store result entity @s ArmorItems[3].tag.tcc.Durability int 1 run scoreboard players get @s tcc.math

execute as @s[scores={tcc.random=1}] run function tcc:items/boomerang/item/catch/set_lore

