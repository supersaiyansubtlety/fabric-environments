############################################################
# Description: Damages potion-tipped swords
# Creator: CreeperMagnet_
############################################################

execute as @s[nbt={SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{id:"minecraft:diamond_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/diamond_sword/break
execute as @s[nbt={SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{id:"minecraft:golden_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/golden_sword/break
execute as @s[nbt={SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{id:"minecraft:iron_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/iron_sword/break
execute as @s[nbt={SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{id:"minecraft:stone_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/stone_sword/break
execute as @s[nbt={SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{id:"minecraft:wooden_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/wooden_sword/break

execute as @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:1}}}},nbt={SelectedItem:{tag:{tcc:{Item:"tipped_sword"}}}}] run function tcc:items/potion_items/tipped_swords/damage
