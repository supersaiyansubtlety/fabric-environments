############################################################
# Description: Makes apple pies work
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/items/apple_pie_full
tag @s[nbt={SelectedItem:{tag:{tcc:{Item:"apple_pie_full"}}}}] add tcc.scheduled.apple_pie_full.mainhand
tag @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"apple_pie_full"}}}]}] add tcc.scheduled.apple_pie_full.offhand
schedule function tcc:items/apple_pie/half_insert 1t
effect give @s saturation 1 6 true
