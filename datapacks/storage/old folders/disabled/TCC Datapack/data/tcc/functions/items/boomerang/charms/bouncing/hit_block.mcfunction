############################################################
# Description: Commands for the boomerang hitting a block and bouncing
# Creator: CreeperMagnet_
############################################################
scoreboard players set @s tcc.boomerangm1 -1
scoreboard players set @s[scores={tcc.boomerang=..50}] tcc.math 100
scoreboard players operation @s[scores={tcc.boomerang=..50}] tcc.math -= @s tcc.boomerang
scoreboard players operation @s[scores={tcc.boomerang=..50}] tcc.boomerang = @s tcc.math
scoreboard players set @s tcc.math 0