############################################################
# Description: Sets a random golden ring charm
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/2
execute as @s[scores={tcc.random=1}] run data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:structure_block",Count:1b,tag:{CustomModelData:8,tcc:{Item:"golden_ring"},display:{Name:'{"translate":"item.tcc.golden_ring","italic":"false"}'}}},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:structure_block",tag:{tcc:{Charms:["attraction"],Item:"golden_ring"},CustomModelData:9,display:{Name:'{"translate":"item.tcc.golden_ring","italic":"false"}',Lore:["{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.attraction\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}
execute as @s[scores={tcc.random=2}] run data modify entity @s Offers.Recipes append value {maxUses:8,buyB:{id:"minecraft:structure_block",Count:1b,tag:{CustomModelData:8,tcc:{Item:"golden_ring"},display:{Name:'{"translate":"item.tcc.golden_ring","italic":"false"}'}}},buy:{id:"minecraft:emerald",Count:1b},sell:{Count:1b,id:"minecraft:structure_block",tag:{HideFlags:6,AttributeModifiers:[{Name:"generic.movementSpeed",AttributeName:"generic.movementSpeed",Amount:0,UUIDMost:167487,UUIDLeast:186746,Operation:1,Slot:"offhand"}],tcc:{Charms:["momentum"],Item:"golden_ring"},CustomModelData:9,display:{Name:'{"translate":"item.tcc.golden_ring","italic":"false"}',Lore:["{\"color\":\"dark_green\",\"italic\":\"false\",\"translate\":\"tcc.lore.charms.prefix\",\"with\":[{\"translate\":\"tcc.lore.charms.momentum\"}]}"]}}},xp:1,uses:0,priceMultipler:0.2f,specialPrice:0,demand:0}

scoreboard players reset @s tcc.random
