############################################################
# Description: Finds if you're looking at a chicken
# Creator: CreeperMagnet_
############################################################

scoreboard players add @s[scores={tcc.math=..499}] tcc.math 1
execute as @s[scores={tcc.math=..499}] anchored eyes positioned ^ ^ ^0.01 if entity @e[type=chicken,tag=!tcc.peculiar_berries.chicken,dx=0,dy=0,dz=0,limit=1,nbt={Age:0}] run function tcc:items/peculiar_berries/feed
execute as @s[scores={tcc.math=..499}] anchored eyes if block ^ ^ ^0.01 #tcc:not_solid anchored feet positioned ^ ^ ^0.01 run function tcc:items/peculiar_berries/raycast