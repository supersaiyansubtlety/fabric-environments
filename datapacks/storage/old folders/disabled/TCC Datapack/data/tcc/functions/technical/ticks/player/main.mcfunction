############################################################
# Description: Functions to run off players every single tick
# Creator: CreeperMagnet_
############################################################

## Elytra Closing When Shifting
execute as @s[scores={tcc.shifttime=1..,tcc.useelytra=1..},nbt={Inventory:[{Slot:102b,id:"minecraft:elytra"}]},nbt=!{Inventory:[{Slot:102b,id:"minecraft:elytra",tag:{Damage:431}}]}] run function tcc:items/elytra_close/close

## Commands for holding items
execute unless entity @s[nbt=!{Inventory:[{Slot:-106b}]},nbt=!{SelectedItem:{}}] as @s[gamemode=!spectator] run function tcc:technical/ticks/player/holding_item

## Ender dragon head healing
execute as @s[nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]},gamemode=!spectator] at @s if entity @e[type=end_crystal,distance=..40,nbt=!{ShowBottom:1b},nbt=!{Dimension:1}] at @s run function tcc:entities/end_crystal/tick
execute unless entity @s[gamemode=!spectator,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] as @e[type=end_crystal,distance=..50,nbt=!{ShowBottom:1b},nbt=!{Dimension:1},nbt={BeamTarget:{}}] at @s unless entity @e[type=player,distance=..30,gamemode=!spectator,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] run function tcc:entities/end_crystal/reset

## Cracked Nether Star Creation
execute as @e[type=item,limit=1,sort=random,distance=..20,nbt={Item:{id:"minecraft:nether_star",Count:1b}}] at @s if entity @e[type=falling_block,sort=nearest,limit=1,dx=0,dy=0,dz=0,nbt={BlockState:{Name:"minecraft:anvil"}}] run data modify entity @s Item set value {Count:1b,id:"minecraft:carrot_on_a_stick",tag:{Unbreakable:1b,CustomModelData:6,HideFlags:4,display:{Name:"{\"translate\":\"item.tcc.cracked_nether_star\",\"italic\":\"false\",\"color\":\"yellow\"}",Lore:["{\"translate\":\"tcc.lore.tooltip\"}"]},tcc:{Item:"cracked_nether_star"}}}

## Horses with Frost Trotting
execute as @s[nbt=!{Motion:[0.0d,-0.0784000015258789d,0.0d]},nbt={RootVehicle:{Entity:{OnGround:1b,SaddleItem:{tag:{tcc:{Charms:["frost_trotting"]}}}}}}] run fill ~2 ~-1 ~2 ~-2 ~-1 ~-2 minecraft:frosted_ice replace water

## Score reset
execute as @s[gamemode=!creative,gamemode=!spectator] run function tcc:technical/ticks/player/score_reset
