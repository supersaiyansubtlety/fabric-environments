############################################################
# Description: Makes the end crystal function tickly
# Creator: CreeperMagnet_
############################################################

execute as @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] store result score @s tcc.math2 run data get entity @s Pos[1]
scoreboard players set @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,limit=1,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] tcc.math 2
execute as @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] run scoreboard players operation @s tcc.math2 -= @s tcc.math
scoreboard players reset @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] tcc.math
execute store result entity @s BeamTarget.Y double 1 run scoreboard players get @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,limit=1,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] tcc.math2
data modify entity @s BeamTarget.X set from entity @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,limit=1,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] Pos[0]
data modify entity @s BeamTarget.Z set from entity @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,limit=1,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] Pos[2]
scoreboard players reset @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,limit=1,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] tcc.math2
execute as @p[gamemode=!spectator,tag=!tcc.end_crystal.player_used,limit=1,distance=..30,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] as @s[nbt=!{ActiveEffects:[{Id:10b}]}] run effect give @s regeneration 10 0 true
tag @s add tcc.end_crystal.has_targeted
