############################################################
# Description: Makes apple pie halves work
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/items/apple_pie_half
tag @s[nbt={SelectedItem:{tag:{tcc:{Item:"apple_pie_half"}}}}] add tcc.scheduled.apple_pie_half.mainhand
tag @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"apple_pie_half"}}}]}] add tcc.scheduled.apple_pie_half.offhand
schedule function tcc:items/apple_pie/slice_insert 1t
effect give @s saturation 1 4 true
