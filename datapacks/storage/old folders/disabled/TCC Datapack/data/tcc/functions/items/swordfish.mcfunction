############################################################
# Description: Makes swordfish work
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/items/swordfish
effect give @s instant_damage 1 0
effect give @s saturation 1 2

execute as @s[scores={tcc.health=7..}] run effect give @s instant_damage 1 0
execute as @s[scores={tcc.health=1..6}] run gamerule showDeathMessages false
execute as @s[scores={tcc.health=1..6}] run tellraw @a {"translate":"death.tcc.swordfish","with":[{"selector":"@s"}]}
execute as @s[scores={tcc.health=1..6}] run kill @s
execute as @s[scores={tcc.health=1..6}] run gamerule showDeathMessages true