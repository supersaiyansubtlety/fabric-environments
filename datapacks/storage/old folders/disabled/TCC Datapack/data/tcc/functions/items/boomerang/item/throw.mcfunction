############################################################
# Description: Sees what type of boomerang you're throwing
# Creator: CreeperMagnet_
############################################################

summon armor_stand ^ ^ ^ {ArmorItems:[{id:"minecraft:stone",Count:1b,tag:{tcc:{UUIDMost:0L,UUIDLeast:0L}}},{},{},{id:"minecraft:structure_block",Count:1b,tag:{CustomModelData:1}}],DisabledSlots:2096896,Invisible:1b,Marker:1b,NoGravity:1b,Tags:["tcc.boomerang","tcc.boomerang.start"]}
data modify entity @e[type=armor_stand,limit=1,tag=tcc.boomerang.start] ArmorItems[3].tag set from entity @s SelectedItem.tag
data modify entity @e[type=armor_stand,limit=1,tag=tcc.boomerang.start] ArmorItems[0].tag.tcc.UUIDMost set from entity @s UUIDMost
data modify entity @e[type=armor_stand,limit=1,tag=tcc.boomerang.start] ArmorItems[0].tag.tcc.UUIDLeast set from entity @s UUIDLeast

tag @s add tcc.boomerang.player.uuid_match
replaceitem entity @s[gamemode=!creative,gamemode=!spectator] weapon.mainhand air

execute at @s anchored eyes run teleport @e[type=armor_stand,tag=tcc.boomerang.start,sort=arbitrary] ^ ^ ^ ~ ~
execute at @s store result score @e[type=armor_stand,tag=tcc.boomerang.start,sort=arbitrary] tcc.boomerangu run data get entity @s UUIDLeast 0.0000000001
execute as @e[type=armor_stand,tag=tcc.boomerang.start] run function tcc:items/boomerang/assign_tags

tag @e[type=armor_stand,tag=tcc.boomerang.start,sort=arbitrary] remove tcc.boomerang.start
