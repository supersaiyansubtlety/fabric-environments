############################################################
# Description: Commands to break a block breaker
# Creator: CreeperMagnet_
############################################################

kill @s
data merge entity @e[limit=1,type=item,distance=..2,nbt={PickupDelay:10s,Item:{id:"minecraft:dropper",tag:{display:{Name:"{\"translate\":\"block.tcc.excavator\"}"}}}}] {Item:{id:"minecraft:barrier",tag:{CustomModelData:1,display:{Lore:["{\"translate\":\"tcc.lore.tooltip\"}"],Name:"{\"translate\":\"block.tcc.excavator\",\"italic\":\"false\"}"},tcc:{Item:"excavator"}}}}
