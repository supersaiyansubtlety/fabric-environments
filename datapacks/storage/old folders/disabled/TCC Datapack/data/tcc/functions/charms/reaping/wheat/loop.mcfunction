############################################################
# Description: Makes the reaping enchantment find wheat around it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:wheat ~ ~ ~ 0.5 0.5 0.5 1 1 force
execute as @p[scores={tcc.minewheat=1..},distance=..4] run loot spawn ~ ~ ~ mine ~ ~ ~ mainhand
setblock ~ ~ ~ air
execute positioned ~ ~1 ~ if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ minecraft:wheat[age=7] if entity @a[scores={tcc.minewheat=1..},distance=..4] run function tcc:charms/reaping/wheat/loop
