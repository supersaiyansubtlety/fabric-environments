############################################################
# Description: Makes the timbering enchantment find logs above it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:stripped_dark_oak_log ~ ~ ~ 0.5 0.5 0.5 1 1 force
setblock ~ ~ ~ air
scoreboard players add @s tcc.logcount 1
execute positioned ~ ~1 ~ if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ stripped_dark_oak_log if entity @a[scores={tcc.slogbreak2=1..},distance=..10] run function tcc:charms/timbering/stripped_dark_oak_log/loop
execute unless block ~ ~1 ~-1 stripped_dark_oak_log unless block ~ ~1 ~1 stripped_dark_oak_log unless block ~ ~1 ~ stripped_dark_oak_log unless block ~1 ~1 ~ stripped_dark_oak_log unless block ~-1 ~1 ~ stripped_dark_oak_log unless block ~1 ~ ~ stripped_dark_oak_log unless block ~-1 ~ ~ stripped_dark_oak_log unless block ~ ~ ~1 stripped_dark_oak_log unless block ~ ~ ~-1 stripped_dark_oak_log run function tcc:charms/timbering/stripped_dark_oak_log/count