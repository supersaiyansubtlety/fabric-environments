############################################################
# Description: Starts a quest
# Creator: CreeperMagnet_
############################################################

advancement grant @s only tcc:minecraft/adventure/begin_quest
tag @s add tcc.player.quest.warrior.started
advancement grant @s only tcc:technical/items/quest_book/warrior/display/root
tellraw @a ["",{"selector":"@s"},{"translate":"tcc.tellraws.quests.start"},{"translate":"tcc.tellraws.quests.warrior.title","hoverEvent":{"action":"show_text","value":{"text":"","extra":[{"translate":"advancements.tcc.quests.warrior.title"},{"text":"\n"},{"translate":"tcc.tellraws.quests.warrior.subtitle"}]}}}]