############################################################
# Description: Commands for hitting things with tipped swords
# Creator: CreeperMagnet_
############################################################
advancement revoke @s only tcc:technical/items/player_with_tipped_sword

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:regeneration"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] regeneration 5 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_regeneration"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] regeneration 10 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_regeneration"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] regeneration 2 1

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:swiftness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] speed 8 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_swiftness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] speed 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_swiftness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] speed 4 1

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strength"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] strength 8 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_strength"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] strength 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_strength"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] strength 4 1

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:leaping"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] jump_boost 8 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_leaping"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] jump_boost 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_leaping"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] jump_boost 4 1

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:poison"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] poison 5 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_poison"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] poison 10 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_poison"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] poison 2 1

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:slowness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] slowness 5 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_slowness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] slowness 10 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_slowness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] slowness 2 3

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:turtle_master"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] slowness 5 3
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_turtle_master"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] slowness 10 3
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_turtle_master"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] slowness 2 5
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:turtle_master"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] resistance 5 3
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_turtle_master"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] resistance 10 3
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_turtle_master"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] resistance 2 5

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:healing"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] instant_health 1 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_healing"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] instant_health 1 1

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:harming"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] instant_damage 1 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:strong_harming"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] instant_damage 1 1

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:night_vision"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] night_vision 8 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_night_vision"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] night_vision 16 0

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:fire_resistance"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] fire_resistance 8 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_fire_resistance"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] fire_resistance 16 0

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:water_breathing"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] water_breathing 8 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_water_breathing"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] water_breathing 16 0

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:invisibility"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] invisibility 8 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_invisibility"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] invisibility 16 0

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:weakness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] weakness 2 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_weakness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] weakness 5 0

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:slow_falling"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] slow_falling 2 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:long_slow_falling"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] slow_falling 5 0

execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:haste"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] haste 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:blindness"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] blindness 5 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:nausea"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] nausea 5 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:health_boost"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] health_boost 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:absorption"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] absorption 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:hunger"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] hunger 5 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:resistance"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] resistance 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:saturation"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] saturation 1 3
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:glowing"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] glowing 10 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:unluck"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] unluck 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:levitation"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] levitation 5 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:wither"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] wither 10 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:mining_fatigue"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] mining_fatigue 10 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:luck"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] luck 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:conduit_power"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] conduit_power 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:dolphins_grace"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] dolphins_grace 16 0
execute as @s[nbt={SelectedItem:{tag:{tcc:{Potion:"minecraft:bad_omen"}}}}] run effect give @e[nbt={HurtTime:10s},distance=0.0000001..15,sort=arbitrary] bad_omen 30 0

function tcc:items/potion_items/tipped_swords/discern_damage
