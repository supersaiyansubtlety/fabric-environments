############################################################
# Description: Commands for the boomerang hitting players
# Creator: CreeperMagnet_
############################################################

tag @s[tag=tcc.boomerang.player.uuid_match.hurt] remove tcc.boomerang.player.uuid_match.hurt
scoreboard players set @s tcc.math 0

execute store result score @s tcc.math run data get entity @s UUIDLeast 0.0000000001
scoreboard players operation @e[type=armor_stand,distance=..1,limit=1,sort=nearest,tag=tcc.boomerang] tcc.math = @e[type=armor_stand,distance=..1,limit=1,sort=nearest,tag=tcc.boomerang] tcc.boomerangu
scoreboard players operation @s tcc.math -= @e[distance=..1,limit=1,sort=nearest,tag=tcc.boomerang] tcc.math


tag @s[scores={tcc.math=0}] add tcc.boomerang.player.uuid_match.hurt

execute as @s[scores={tcc.health=7..},tag=!tcc.boomerang.player.uuid_match.hurt] run effect give @s instant_damage 1 0
execute as @s[scores={tcc.health=1..6},tag=!tcc.boomerang.player.uuid_match.hurt] run gamerule showDeathMessages false
execute as @s[scores={tcc.health=1..6},tag=!tcc.boomerang.player.uuid_match.hurt] run tellraw @a ["",{"selector":"@s"},{"translate":"death.tcc.boomerang"}]
execute as @s[scores={tcc.health=1..6},tag=!tcc.boomerang.player.uuid_match.hurt] run kill @s
execute as @s[scores={tcc.health=1..6},tag=!tcc.boomerang.player.uuid_match.hurt] run gamerule showDeathMessages true
