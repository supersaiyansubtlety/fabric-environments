############################################################
# Description: Commands for chorus beetle spawning
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/32
execute as @s[scores={tcc.random=32}] run data merge entity @s {DeathTime:19s,Silent:1b,DeathLootTable:"minecraft:empty"}
execute as @s[scores={tcc.random=32}] run kill @s
execute as @s[scores={tcc.random=32}] at @s run function tcc:commands/summon/chorus_beetle
scoreboard players reset @s tcc.random
tag @s add tcc.chorus_beetle.spawn_done
