############################################################
# Description: Makes the timbering enchantment find barks above it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:spruce_wood ~ ~ ~ 0.5 0.5 0.5 1 1 force
setblock ~ ~ ~ air
scoreboard players add @s tcc.logcount 1
execute positioned ~ ~1 ~ if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ spruce_wood if entity @a[scores={tcc.barkbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_wood/loop
execute unless block ~ ~1 ~-1 spruce_wood unless block ~ ~1 ~1 spruce_wood unless block ~ ~1 ~ spruce_wood unless block ~1 ~1 ~ spruce_wood unless block ~-1 ~1 ~ spruce_wood unless block ~1 ~ ~ spruce_wood unless block ~-1 ~ ~ spruce_wood unless block ~ ~ ~1 spruce_wood unless block ~ ~ ~-1 spruce_wood run function tcc:charms/timbering/spruce_wood/count