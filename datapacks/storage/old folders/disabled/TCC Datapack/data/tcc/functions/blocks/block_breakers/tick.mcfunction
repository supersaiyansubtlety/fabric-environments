############################################################
# Description: Commands to run tickly for block breakers
# Creator: CreeperMagnet_
############################################################

execute as @s[tag=!tcc.block_breakers.powered] if block ~ ~-1 ~ dropper[triggered=true] unless block ~ ~-1 ~ minecraft:dropper{Items:[{Slot:0b},{Slot:1b},{Slot:2b},{Slot:3b},{Slot:4b},{Slot:5b},{Slot:6b},{Slot:7b},{Slot:8b}]} run function tcc:blocks/block_breakers/discern_power
execute as @s[tag=tcc.block_breakers.powered] if block ~ ~-1 ~ dropper[triggered=false] unless block ~ ~-1 ~ minecraft:dropper{Items:[{Slot:0b},{Slot:1b},{Slot:2b},{Slot:3b},{Slot:4b},{Slot:5b},{Slot:6b},{Slot:7b},{Slot:8b}]} run function tcc:blocks/block_breakers/discern_unpower
execute if block ~ ~-1 ~ air run function tcc:blocks/block_breakers/discern_break
