############################################################
# Description: Mends TCC items with mending
# Creator: CreeperMagnet_
############################################################

execute as @s[tag=!tcc.mending_repaired] positioned ~ ~0.805 ~ if entity @e[distance=..3,type=experience_orb] run scoreboard players operation @s tcc.old_xp = @s tcc.current_xp
execute as @s positioned ~ ~0.805 ~ if entity @e[distance=..3,type=experience_orb] run tag @s add tcc.mending_repaired
execute as @s[tag=tcc.mending_repaired] positioned ~ ~0.805 ~ unless entity @e[distance=..3,type=experience_orb] run function tcc:items/mend_items/xp_test
