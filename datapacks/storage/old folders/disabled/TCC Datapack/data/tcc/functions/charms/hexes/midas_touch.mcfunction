############################################################
# Description: Commands to run whenever a player with a midas touch sword kills something
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/charms/hexes/midas_touch

effect give @s unluck 480 0
