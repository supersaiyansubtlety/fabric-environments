############################################################
# Description: Assigns correct tags based on boomerang types
# Creator: CreeperMagnet_
############################################################

tag @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["flowing"]}}}]}] add tcc.boomerang.charms.flowing
tag @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["seeking"]}}}]}] add tcc.boomerang.charms.seeking
tag @s[nbt={ArmorItems:[{},{},{},{tag:{tcc:{Charms:["bouncing"]}}}]}] add tcc.boomerang.charms.bouncing