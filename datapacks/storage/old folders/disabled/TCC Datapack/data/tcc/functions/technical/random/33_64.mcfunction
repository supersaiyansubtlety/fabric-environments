############################################################
# Description: Generates a random number, either 1 or 2
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/16
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome1.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome1.2
execute as @s[scores={tcc.random=3}] run tag @s add tcc.random.outcome1.3
execute as @s[scores={tcc.random=4}] run tag @s add tcc.random.outcome1.4
execute as @s[scores={tcc.random=5}] run tag @s add tcc.random.outcome1.5
execute as @s[scores={tcc.random=6}] run tag @s add tcc.random.outcome1.6
execute as @s[scores={tcc.random=7}] run tag @s add tcc.random.outcome1.7
execute as @s[scores={tcc.random=8}] run tag @s add tcc.random.outcome1.8
execute as @s[scores={tcc.random=9}] run tag @s add tcc.random.outcome1.9
execute as @s[scores={tcc.random=10}] run tag @s add tcc.random.outcome1.10
execute as @s[scores={tcc.random=11}] run tag @s add tcc.random.outcome1.11
execute as @s[scores={tcc.random=12}] run tag @s add tcc.random.outcome1.12
execute as @s[scores={tcc.random=13}] run tag @s add tcc.random.outcome1.13
execute as @s[scores={tcc.random=14}] run tag @s add tcc.random.outcome1.14
execute as @s[scores={tcc.random=15}] run tag @s add tcc.random.outcome1.15
execute as @s[scores={tcc.random=16}] run tag @s add tcc.random.outcome1.16
function tcc:technical/random/2
execute as @s[scores={tcc.random=1}] run tag @s add tcc.random.outcome2.1
execute as @s[scores={tcc.random=2}] run tag @s add tcc.random.outcome2.2
scoreboard players reset @s tcc.random

execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 1
execute as @s[tag=tcc.random.outcome1.1,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 2
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 3
execute as @s[tag=tcc.random.outcome1.2,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 4
execute as @s[tag=tcc.random.outcome1.3,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 5
execute as @s[tag=tcc.random.outcome1.3,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 6
execute as @s[tag=tcc.random.outcome1.4,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 7
execute as @s[tag=tcc.random.outcome1.4,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 8
execute as @s[tag=tcc.random.outcome1.5,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 9
execute as @s[tag=tcc.random.outcome1.5,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 10
execute as @s[tag=tcc.random.outcome1.6,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 11
execute as @s[tag=tcc.random.outcome1.6,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 12
execute as @s[tag=tcc.random.outcome1.7,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 13
execute as @s[tag=tcc.random.outcome1.7,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 14
execute as @s[tag=tcc.random.outcome1.8,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 15
execute as @s[tag=tcc.random.outcome1.8,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 16
execute as @s[tag=tcc.random.outcome1.9,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 17
execute as @s[tag=tcc.random.outcome1.9,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 18
execute as @s[tag=tcc.random.outcome1.10,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 19
execute as @s[tag=tcc.random.outcome1.10,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 20
execute as @s[tag=tcc.random.outcome1.11,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 21
execute as @s[tag=tcc.random.outcome1.11,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 22
execute as @s[tag=tcc.random.outcome1.12,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 23
execute as @s[tag=tcc.random.outcome1.12,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 24
execute as @s[tag=tcc.random.outcome1.13,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 25
execute as @s[tag=tcc.random.outcome1.13,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 26
execute as @s[tag=tcc.random.outcome1.14,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 27
execute as @s[tag=tcc.random.outcome1.14,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 28
execute as @s[tag=tcc.random.outcome1.15,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 29
execute as @s[tag=tcc.random.outcome1.15,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 30
execute as @s[tag=tcc.random.outcome1.16,tag=tcc.random.outcome2.1] run scoreboard players set @s tcc.random 31
execute as @s[tag=tcc.random.outcome1.16,tag=tcc.random.outcome2.2] run scoreboard players set @s tcc.random 32
scoreboard players add @s tcc.random 32

tag @s remove tcc.random.outcome1.1
tag @s remove tcc.random.outcome1.2
tag @s remove tcc.random.outcome1.3
tag @s remove tcc.random.outcome1.4
tag @s remove tcc.random.outcome1.5
tag @s remove tcc.random.outcome1.6
tag @s remove tcc.random.outcome1.7
tag @s remove tcc.random.outcome1.8
tag @s remove tcc.random.outcome1.9
tag @s remove tcc.random.outcome1.10
tag @s remove tcc.random.outcome1.11
tag @s remove tcc.random.outcome1.12
tag @s remove tcc.random.outcome1.13
tag @s remove tcc.random.outcome1.14
tag @s remove tcc.random.outcome1.15
tag @s remove tcc.random.outcome1.16
tag @s remove tcc.random.outcome2.1
tag @s remove tcc.random.outcome2.2