############################################################
# Description: Makes potion-soaked potatoes function
# Creator: CreeperMagnet_
############################################################

effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_absorption=true}}] absorption 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_blindness=true}}] blindness 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_fire_resistance=true}}] fire_resistance 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_glowing=true}}] glowing 10 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_harming=true}}] instant_damage 1 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_haste=true}}] haste 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_healing=true}}] instant_health 1 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_health_boost=true}}] health_boost 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_hunger=true}}] hunger 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_invisibility=true}}] invisibility 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_leaping=true}}] jump_boost 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_levitation=true}}] levitation 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_fire_resistance=true}}] fire_resistance 60 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_invisibility=true}}] invisibility 60 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_leaping=true}}] jump_boost 60 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_night_vision=true}}] night_vision 60 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_poison=true}}] poison 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_regeneration=true}}] regeneration 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_slow_falling=true}}] slow_falling 30 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_slowness=true}}] slowness 30 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_strength=true}}] strength 60 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_swiftness=true}}] speed 60 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_turtle_master=true}}] slowness 14 3
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_turtle_master=true}}] resistance 14 2
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_water_breathing=true}}] water_breathing 60 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_weakness=true}}] weakness 30 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_luck=true}}] luck 37 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_mining_fatigue=true}}] mining_fatigue 60 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_long_nausea=true}}] nausea 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_night_vision=true}}] night_vision 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_poison=true}}] poison 5 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_regeneration=true}}] regeneration 5 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_resistance=true}}] resistance 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_saturation=true}}] saturation 1 3
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_slow_falling=true}}] slow_falling 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_slowness=true}}] slowness 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_slow_falling=true}}] slow_falling 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strength=true}}] strength 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_harming=true}}] instant_damage 1 1
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_healing=true}}] instant_health 1 1
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_leaping=true}}] jump_boost 11 1
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_poison=true}}] poison 2 1
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_regeneration=true}}] regeneration 2 1
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_slowness=true}}] slowness 5 3
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_strength=true}}] strength 11 1
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_swiftness=true}}] speed 11 1
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_turtle_master=true}}] slowness 7 5
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_strong_turtle_master=true}}] resistance 7 3
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_swiftness=true}}] speed 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_turtle_master=true}}] slowness 7 3
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_turtle_master=true}}] resistance 7 2
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_unluck=true}}] unluck 37 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_water_breathing=true}}] water_breathing 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_weakness=true}}] weakness 11 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_wither=true}}] wither 10 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_bad_omen=true}}] bad_omen 45 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_conduit_power=true}}] conduit_power 22 0
effect give @s[advancements={tcc:technical/items/potion_potatoes={minecraft_dolphins_grace=true}}] dolphins_grace 22 0


advancement revoke @s only tcc:technical/items/potion_potatoes
