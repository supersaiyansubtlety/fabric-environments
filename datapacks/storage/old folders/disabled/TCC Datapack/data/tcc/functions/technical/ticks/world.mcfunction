############################################################
# Description: Functions to run every single tick
# Creator: CreeperMagnet_
############################################################


## Player Tick
execute as @a[sort=arbitrary] at @s run function tcc:technical/ticks/player/main

## Ender dragon head healing
execute as @r[nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]},gamemode=!spectator] at @s as @e[type=end_crystal,distance=..30,nbt=!{ShowBottom:1b},nbt=!{Dimension:1},tag=!tcc.end_crystal.has_targeted,nbt={BeamTarget:{}}] at @s run function tcc:entities/end_crystal/reset
execute as @a[nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]},gamemode=!spectator] at @s as @e[type=end_crystal,distance=..40,nbt=!{ShowBottom:1b},nbt=!{Dimension:1}] run tag @s remove tcc.end_crystal.has_targeted
execute as @a[nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]},gamemode=!spectator] at @s as @e[type=end_crystal,distance=..40,nbt=!{ShowBottom:1b},nbt=!{Dimension:1}] run tag @s remove tcc.end_crystal.used

## Block breakers
execute as @e[sort=arbitrary,type=armor_stand,tag=tcc.blocks.block_breaker] at @s run function tcc:blocks/block_breakers/tick

## Chorus Beetles
execute as @e[tag=tcc.chorus_beetle,type=zombie_pigman,sort=arbitrary] at @s run function tcc:entities/chorus_beetle/tick

## Boomerangs
execute as @e[tag=tcc.boomerang,type=armor_stand,sort=arbitrary] at @s run function tcc:items/boomerang/tick
