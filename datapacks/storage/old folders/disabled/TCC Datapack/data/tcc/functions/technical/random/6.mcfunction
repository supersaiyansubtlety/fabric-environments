############################################################
# Description: Generates a random number, from 1 to 6
# Creator: CreeperMagnet_
############################################################

function tcc:technical/random/8
execute as @s[scores={tcc.random=7..8}] run function tcc:technical/random/6
