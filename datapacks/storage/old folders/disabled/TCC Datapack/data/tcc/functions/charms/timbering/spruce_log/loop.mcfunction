############################################################
# Description: Makes the timbering enchantment find logs above it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:spruce_log ~ ~ ~ 0.5 0.5 0.5 1 1 force
setblock ~ ~ ~ air
scoreboard players add @s tcc.logcount 1
execute positioned ~ ~1 ~ if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ spruce_log if entity @a[scores={tcc.logbreak5=1..},distance=..10] run function tcc:charms/timbering/spruce_log/loop
execute unless block ~ ~1 ~-1 spruce_log unless block ~ ~1 ~1 spruce_log unless block ~ ~1 ~ spruce_log unless block ~1 ~1 ~ spruce_log unless block ~-1 ~1 ~ spruce_log unless block ~1 ~ ~ spruce_log unless block ~-1 ~ ~ spruce_log unless block ~ ~ ~1 spruce_log unless block ~ ~ ~-1 spruce_log run function tcc:charms/timbering/spruce_log/count