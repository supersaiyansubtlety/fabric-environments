############################################################
# Description: Commands for the "Beet the Beetle" advancement
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:minecraft/end/beet_beetle_trigger
advancement grant @s[nbt={SelectedItem:{id:"minecraft:beetroot"}}] only tcc:minecraft/end/beet_beetle_display