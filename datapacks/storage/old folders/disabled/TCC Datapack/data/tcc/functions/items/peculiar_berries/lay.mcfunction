############################################################
# Description: Makes chickens lay eggs properly
# Creator: CreeperMagnet_
############################################################

execute as @s[nbt={Dimension:-1}] at @s run loot spawn ~ ~ ~ loot tcc:items/peculiar_berries/nether
execute as @s[nbt={Dimension:0}] at @s run loot spawn ~ ~ ~ loot tcc:items/peculiar_berries/overworld
execute as @s[nbt={Dimension:1}] at @s run loot spawn ~ ~ ~ loot tcc:items/peculiar_berries/end


playsound entity.chicken.egg master @a[distance=..16] ~ ~ ~
data merge entity @s {EggLayTime:6000}