############################################################
# Description: Commands for when a player places a block breaker
# Creator: CreeperMagnet_
############################################################

advancement revoke @s only tcc:technical/blocks/snipper_placed
execute as @s[y_rotation=-130..-45,x_rotation=-60..60] run fill ~10 ~10 ~10 ~-10 ~-10 ~-10 command_block{Command:"function tcc:blocks/block_breakers/snipper/place/east",auto:1b} replace barrier
execute as @s[y_rotation=-45..45,x_rotation=-60..60] run fill ~10 ~10 ~10 ~-10 ~-10 ~-10 command_block{Command:"function tcc:blocks/block_breakers/snipper/place/south",auto:1b} replace barrier
execute as @s[y_rotation=45..130,x_rotation=-60..60] run fill ~10 ~10 ~10 ~-10 ~-10 ~-10 command_block{Command:"function tcc:blocks/block_breakers/snipper/place/west",auto:1b} replace barrier
execute as @s[y_rotation=130..-103,x_rotation=-60..60] run fill ~10 ~10 ~10 ~-10 ~-10 ~-10 command_block{Command:"function tcc:blocks/block_breakers/snipper/place/north",auto:1b} replace barrier
execute as @s[x_rotation=-90..-60] run fill ~10 ~10 ~10 ~-10 ~-10 ~-10 command_block{Command:"function tcc:blocks/block_breakers/snipper/place/up",auto:1b} replace barrier
execute as @s[x_rotation=60..90] run fill ~10 ~10 ~10 ~-10 ~-10 ~-10 command_block{Command:"function tcc:blocks/block_breakers/snipper/place/down",auto:1b} replace barrier
