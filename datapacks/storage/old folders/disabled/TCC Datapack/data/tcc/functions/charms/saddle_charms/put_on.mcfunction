############################################################
# Description: Makes horses' stats update when they put on horse armor
# Creator: CreeperMagnet_
############################################################

tag @s add tcc.horse.has_charmed_saddle
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["galloping"]}}}}] store result score @s tcc.math2 run data get entity @s Attributes[{Name:"generic.movementSpeed"}].Base 100
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["galloping"]}}}}] run scoreboard players set @s tcc.math 2
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["galloping"]}}}}] run scoreboard players operation @s tcc.math2 *= @s tcc.math
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["galloping"]}}}}] store result entity @s Attributes[{Name:"generic.movementSpeed"}].Base double 0.01 run scoreboard players get @s tcc.math2
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["galloping"]}}}}] run scoreboard players reset @s tcc.math
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["galloping"]}}}}] run tag @s add tcc.horse.has_charmed_saddle.galloping
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["leaping"]}}}}] store result score @s tcc.math2 run data get entity @s Attributes[{Name:"horse.jumpStrength"}].Base 100
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["leaping"]}}}}] run scoreboard players set @s tcc.math 2
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["leaping"]}}}}] run scoreboard players operation @s tcc.math2 *= @s tcc.math
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["leaping"]}}}}] store result entity @s Attributes[{Name:"horse.jumpStrength"}].Base double 0.01 run scoreboard players get @s tcc.math2
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["leaping"]}}}}] run scoreboard players reset @s tcc.math
execute as @s[nbt={SaddleItem:{tag:{tcc:{Charms:["leaping"]}}}}] run tag @s add tcc.horse.has_charmed_saddle.leaping
scoreboard players reset @s tcc.math2
