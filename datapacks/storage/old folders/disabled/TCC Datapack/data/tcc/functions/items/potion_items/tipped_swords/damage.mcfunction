############################################################
# Description: Damages potion-tipped swords
# Creator: CreeperMagnet_
############################################################

execute store result score @s tcc.math run data get entity @s SelectedItem.tag.tcc.Durability 1
scoreboard players remove @s tcc.math 1
execute store result entity @s SelectedItem.tag.tcc.Durability int 1 run scoreboard players get @s tcc.math
scoreboard players reset @s tcc.math

execute as @s[nbt={SelectedItem:{id:"minecraft:diamond_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/diamond_sword/store
execute as @s[nbt={SelectedItem:{id:"minecraft:diamond_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/diamond_sword/reorder

execute as @s[nbt={SelectedItem:{id:"minecraft:golden_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/golden_sword/store
execute as @s[nbt={SelectedItem:{id:"minecraft:golden_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/golden_sword/reorder

execute as @s[nbt={SelectedItem:{id:"minecraft:stone_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/stone_sword/store
execute as @s[nbt={SelectedItem:{id:"minecraft:stone_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/stone_sword/reorder

execute as @s[nbt={SelectedItem:{id:"minecraft:iron_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/iron_sword/store
execute as @s[nbt={SelectedItem:{id:"minecraft:iron_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/iron_sword/reorder

execute as @s[nbt={SelectedItem:{id:"minecraft:wooden_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/wooden_sword/store
execute as @s[nbt={SelectedItem:{id:"minecraft:wooden_sword"}}] run loot replace entity @s weapon.mainhand loot tcc:technical/durability/tipped_swords/wooden_sword/reorder
