############################################################
# Description: Commands for when you're hit by something with a tipped sword
# Creator: CreeperMagnet_
############################################################

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_absorption=true}}] absorption 10 0

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_regeneration=true}}] regeneration 5 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_regeneration=true}}] regeneration 10 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_regeneration=true}}] regeneration 2 1

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_swiftness=true}}] speed 8 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_swiftness=true}}] speed 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_swiftness=true}}] speed 4 1

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strength=true}}] strength 8 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_strength=true}}] strength 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_strength=true}}] strength 4 1

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_leaping=true}}] jump_boost 8 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_leaping=true}}] jump_boost 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_leaping=true}}] jump_boost 4 1

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_poison=true}}] poison 5 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_poison=true}}] poison 10 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_poison=true}}] poison 2 1

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_slowness=true}}] slowness 5 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_slowness=true}}] slowness 10 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_slowness=true}}] slowness 2 3

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_turtle_master=true}}] slowness 5 3
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_turtle_master=true}}] slowness 10 3
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_turtle_master=true}}] slowness 2 5
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_turtle_master=true}}] resistance 5 3
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_turtle_master=true}}] resistance 10 3
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_turtle_master=true}}] resistance 2 5

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_healing=true}}] instant_health 1 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_healing=true}}] instant_health 1 1

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_harming=true}}] instant_damage 1 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_strong_harming=true}}] instant_damage 1 1

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_night_vision=true}}] night_vision 8 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_night_vision=true}}] night_vision 16 0

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_fire_resistance=true}}] fire_resistance 8 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_fire_resistance=true}}] fire_resistance 16 0

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_water_breathing=true}}] water_breathing 8 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_water_breathing=true}}] water_breathing 16 0

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_invisibility=true}}] invisibility 8 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_invisibility=true}}] invisibility 16 0

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_weakness=true}}] weakness 2 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_weakness=true}}] weakness 5 0

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_slow_falling=true}}] slow_falling 2 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_long_slow_falling=true}}] slow_falling 5 0

effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_haste=true}}] haste 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_blindness=true}}] blindness 5 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_nausea=true}}] nausea 5 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_health_boost=true}}] health_boost 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_absorption=true}}] absorption 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_hunger=true}}] hunger 5 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_resistance=true}}] resistance 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_saturation=true}}] saturation 1 3
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_glowing=true}}] glowing 10 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_unluck=true}}] unluck 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_levitation=true}}] levitation 5 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_wither=true}}] wither 10 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_mining_fatigue=true}}] mining_fatigue 10 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_luck=true}}] luck 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_conduit_power=true}}] conduit_power 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_dolphins_grace=true}}] dolphins_grace 16 0
effect give @s[advancements={tcc:technical/items/entity_with_tipped_sword={minecraft_bad_omen=true}}] bad_omen 30 0

advancement revoke @s only tcc:technical/items/entity_with_tipped_sword
