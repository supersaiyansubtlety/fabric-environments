############################################################
# Description: Makes the end crystal function tickly
# Creator: CreeperMagnet_
############################################################

tag @s remove tcc.end_crystal.player_used
execute as @s[nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]},gamemode=!spectator,tag=!tcc.end_crystal.player_used] at @s as @e[type=end_crystal,distance=..30,nbt=!{ShowBottom:1b},nbt=!{Dimension:1},sort=nearest] at @s run function tcc:entities/end_crystal/choose_targets
execute as @e[type=end_crystal,distance=..40,nbt=!{ShowBottom:1b},nbt=!{Dimension:1},sort=nearest,nbt={BeamTarget:{}}] at @s unless entity @e[type=player,distance=..30,gamemode=!spectator,nbt={Inventory:[{id:"minecraft:dragon_head",Slot:103b}]}] run function tcc:entities/end_crystal/reset
