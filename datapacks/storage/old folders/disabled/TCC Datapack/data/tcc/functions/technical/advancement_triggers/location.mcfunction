############################################################
# Description: Commands to run every second
# Creator: CreeperMagnet_
############################################################

## Chorus Beetle Anger
execute as @a[scores={tcc.minechorus1=1..}] run function tcc:entities/chorus_beetle/anger
execute as @a[scores={tcc.minechorus2=1..}] run function tcc:entities/chorus_beetle/anger

## Cat Water Hissing
execute as @e[type=#tcc:boomerang_damage/cat_sized,tag=!tcc.cat.water_hiss,sort=arbitrary,distance=..16] at @s if block ~ ~ ~ water run function tcc:entities/cat_water_hiss
execute as @e[type=#tcc:boomerang_damage/cat_sized,tag=tcc.cat.water_hiss,sort=arbitrary,distance=..16] at @s unless block ~ ~ ~ water run tag @s remove tcc.cat.water_hiss

## Area Effect Clouds
execute as @e[type=area_effect_cloud,sort=arbitrary,tag=!tcc.area_effect_cloud.no_tip,tag=!tcc.area_effect_cloud.tagged] run function tcc:entities/area_effect_cloud
execute as @e[type=area_effect_cloud,sort=arbitrary,tag=!tcc.area_effect_cloud.no_tip] at @s align xyz positioned ~-2 ~ ~-2 if entity @e[type=item,dx=4,dz=4,dy=0,sort=arbitrary,nbt=!{Item:{tag:{tcc:{Item:"soaked_food"}}}},nbt=!{Item:{tag:{tcc:{Item:"tipped_sword"}}}},nbt=!{Item:{tag:{tcc:{Item:"soaked_iron_ring"}}}}] run function tcc:items/potion_items/discern_item

## Wither Skeleton Swords
data merge entity @e[sort=arbitrary,type=wither_skeleton,tag=!tcc.wither_skeleton.tagged,limit=1,nbt=!{HandDropChances:[0.0f,0.085f]}] {HandDropChances:[-1000000.0f,0.085f],Tags:["tcc.wither_skeleton.tagged"]}

## Drowned Tridents (50% trident drop rate)
data modify entity @e[sort=arbitrary,type=drowned,nbt={HandItems:[{id:"minecraft:trident"}]},limit=1,nbt=!{HandDropChances:[0.5f]}] HandDropChances[0] set value 0.5f

## Villager Types
execute as @e[type=villager,tag=!tcc.villager.librarian.tagged,sort=arbitrary,nbt={VillagerData:{profession:"minecraft:librarian"}}] run function tcc:entities/villager/librarian/discern_charmed_item
execute as @e[type=villager,tag=!tcc.villager.cartographer.tagged,sort=arbitrary,nbt={VillagerData:{profession:"minecraft:cartographer"}}] run function tcc:entities/villager/cartographer/set_quest

## Peculiar Berry Trade with Wandering Traders
execute as @e[type=wandering_trader,tag=!tcc.wandering_trader.tagged,sort=arbitrary] run data modify entity @s Offers.Recipes append value {maxUses:4b,uses:0,buy:{id:"minecraft:emerald",Count:64b},sell:{id:"minecraft:carrot_on_a_stick",tag:{tcc:{Item:"peculiar_berries"},CustomModelData:7,Unbreakable:1b,HideFlags:4,display:{Name:"{\"translate\":\"item.tcc.peculiar_berries\",\"italic\":\"false\",\"color\":\"light_purple\"}",Lore:["{\"translate\":\"tcc.lore.tooltip\"}"]}},Count:1b}}
execute as @e[type=wandering_trader,tag=!tcc.wandering_trader.tagged,sort=arbitrary] run tag @s add tcc.wandering_trader.tagged

## Chorus Beetle Spawning
execute as @e[type=enderman,sort=arbitrary,tag=!tcc.chorus_beetle.spawn_done,nbt={Dimension:1}] positioned 0 128 0 if entity @s[distance=700..] positioned as @s run function tcc:entities/chorus_beetle/summon

## Peculiar Berries
execute as @e[type=chicken,sort=arbitrary,tag=tcc.peculiar_berries.chicken] at @s run function tcc:items/peculiar_berries/lay_test

## Saddle Enchantments
execute as @e[type=#tcc:horse,tag=tcc.horse.has_charmed_saddle.galloping,nbt=!{SaddleItem:{tag:{tcc:{Charms:["galloping"]}}}}] run function tcc:charms/saddle_charms/take_off
execute as @e[type=#tcc:horse,tag=tcc.horse.has_charmed_saddle.leaping,nbt=!{SaddleItem:{tag:{tcc:{Charms:["leaping"]}}}}] run function tcc:charms/saddle_charms/take_off
execute as @e[type=#tcc:horse,tag=!tcc.horse.has_charmed_saddle] if data entity @s SaddleItem unless entity @s[nbt=!{SaddleItem:{tag:{tcc:{Charms:["leaping"]}}}},nbt=!{SaddleItem:{tag:{tcc:{Charms:["galloping"]}}}}] run function tcc:charms/saddle_charms/put_on


## Reset for mending repair
execute as @a[nbt=!{SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Tags:["mendable"]}}}},nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Tags:["mendable"]}}}]}] run tag @s remove tcc.mending_repaired
execute as @a[nbt=!{SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Tags:["mendable"]}}}},nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Tags:["mendable"]}}}]}] run scoreboard players operation @s tcc.old_xp = @s tcc.current_xp


# Elytra Closing
scoreboard players reset @a tcc.shifttime
scoreboard players reset @a tcc.useelytra

# Reset for momentum rings
execute as @a[scores={tcc.runtime=1..},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Charms:["momentum"]}}}]}] run scoreboard players reset @s tcc.runtime

advancement revoke @r[advancements={tcc:technical/advancement_triggers/location=true}] only tcc:technical/advancement_triggers/location
