############################################################
# Description: Tests if your XP level was changed by an XP orb
# Creator: CreeperMagnet_
############################################################

# Calculations for if your xp level changed or not
tag @s[scores={tcc.old_xp=1..}] add tcc.old_xp.not_reset
tag @s[scores={tcc.old_xp=..-1}] add tcc.old_xp.not_reset
execute as @s[tag=tcc.old_xp.not_reset] run scoreboard players operation @s tcc.old_xp -= @s tcc.current_xp
tag @s remove tcc.old_xp.not_reset
tag @s remove tcc.mending_repaired




# Soaked rings in offhand are repaired before *ANYTHING* in mainhand or offhand
execute if score @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:50,Item:"soaked_iron_ring"}}}]},nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"soaked_iron_ring"}}}]}] tcc.old_xp matches ..-1 run function tcc:items/potion_items/soaked_rings/iron_ring/repair/offhand/loop
execute if score @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:25,Item:"soaked_golden_ring"}}}]},nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"soaked_iron_ring"}}}]}] tcc.old_xp matches ..-1 run function tcc:items/potion_items/soaked_rings/golden_ring/repair/offhand/loop

# Then, mainhand things take precedence
execute if score @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:300,Item:"boomerang"}}}},nbt={SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"boomerang"}}}}] tcc.old_xp matches ..-1 run function tcc:items/boomerang/item/repair/mainhand/loop
execute if score @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:50,Item:"soaked_iron_ring"}}}},nbt={SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"soaked_iron_ring"}}}}] tcc.old_xp matches ..-1 run function tcc:items/potion_items/soaked_rings/iron_ring/repair/mainhand/loop
execute if score @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:25,Item:"soaked_iron_ring"}}}},nbt={SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"soaked_golden_ring"}}}}] tcc.old_xp matches ..-1 run function tcc:items/potion_items/soaked_rings/golden_ring/repair/mainhand/loop
execute if score @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:200,Item:"wrench"}}}},nbt={SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"wrench"}}}}] tcc.old_xp matches ..-1 run function tcc:items/wrench/repair/mainhand/loop
execute if score @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:20,Item:"obsidian_mirror"}}}},nbt={SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"obsidian_mirror"}}}}] tcc.old_xp matches ..-1 run function tcc:items/obsidian_mirror/repair/mainhand/loop
execute if score @s[nbt=!{SelectedItem:{tag:{tcc:{Durability:750,Item:"obsidian_scythe"}}}},nbt={SelectedItem:{tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"obsidian_scythe"}}}}] tcc.old_xp matches ..-1 run function tcc:items/obsidian_scythe/repair/mainhand/loop

# After that, the offhand can be repaired if there's nothing to repair in mainhand
execute if score @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:300,Item:"boomerang"}}}]},nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"boomerang"}}}]}] tcc.old_xp matches ..-1 run function tcc:items/boomerang/item/repair/offhand/loop
execute if score @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:200,Item:"wrench"}}}]},nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"wrench"}}}]}] tcc.old_xp matches ..-1 run function tcc:items/wrench/repair/offhand/loop
execute if score @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:20,Item:"obsidian_mirror"}}}]},nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"obsidian_mirror"}}}]}] tcc.old_xp matches ..-1 run function tcc:items/obsidian_mirror/repair/offhand/loop
execute if score @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:750,Item:"obsidian_scythe"}}}]},nbt={Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:mending"}],tcc:{Item:"obsidian_scythe"}}}]}] tcc.old_xp matches ..-1 run function tcc:items/obsidian_scythe/repair/offhand/loop
