############################################################
# Description: Tags area effect clouds for making potion items
# Creator: CreeperMagnet_
############################################################

tag @s[nbt={Potion:"minecraft:slow_falling"}] add tcc.area_effect_cloud.effect.minecraft.slow_falling
tag @s[nbt={Potion:"minecraft:long_slow_falling"}] add tcc.area_effect_cloud.effect.minecraft.long_slow_falling

tag @s[nbt={Potion:"minecraft:fire_resistance"}] add tcc.area_effect_cloud.effect.minecraft.fire_resistance

tag @s[nbt={Potion:"minecraft:weakness"}] add tcc.area_effect_cloud.effect.minecraft.weakness

tag @s[nbt={Potion:"minecraft:water_breathing"}] add tcc.area_effect_cloud.effect.minecraft.water_breathing

tag @s[nbt={Potion:"minecraft:invisibility"}] add tcc.area_effect_cloud.effect.minecraft.invisibility

tag @s[nbt={Potion:"minecraft:healing"}] add tcc.area_effect_cloud.effect.minecraft.healing
tag @s[nbt={Effects:[{Id:6b}]},nbt=!{Effects:[{Id:6b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.healing

tag @s[nbt={Potion:"minecraft:harming"}] add tcc.area_effect_cloud.effect.minecraft.harming
tag @s[nbt={Effects:[{Id:7b}]},nbt=!{Effects:[{Id:7b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.harming

tag @s[nbt={Potion:"minecraft:regeneration"}] add tcc.area_effect_cloud.effect.minecraft.regeneration
tag @s[nbt={Effects:[{Id:10b}]},nbt=!{Effects:[{Id:10b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.regeneration

tag @s[nbt={Potion:"minecraft:swiftness"}] add tcc.area_effect_cloud.effect.minecraft.swiftness
tag @s[nbt={Effects:[{Id:1b}]},nbt=!{Effects:[{Id:1b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.swiftness

tag @s[nbt={Potion:"minecraft:strength"}] add tcc.area_effect_cloud.effect.minecraft.strength
tag @s[nbt={Effects:[{Id:5b}]},nbt=!{Effects:[{Id:5b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strength

tag @s[nbt={Potion:"minecraft:leaping"}] add tcc.area_effect_cloud.effect.minecraft.leaping
tag @s[nbt={Effects:[{Id:8b}]},nbt=!{Effects:[{Id:8b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.leaping

tag @s[nbt={Potion:"minecraft:poison"}] add tcc.area_effect_cloud.effect.minecraft.poison
tag @s[nbt={Effects:[{Id:19b}]},nbt=!{Effects:[{Id:19b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.poison

tag @s[nbt={Potion:"minecraft:slowness"}] add tcc.area_effect_cloud.effect.minecraft.slowness
tag @s[nbt={Effects:[{Id:2b}]},nbt=!{Effects:[{Id:2b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.slowness

tag @s[nbt={Potion:"minecraft:turtle_master"}] add tcc.area_effect_cloud.effect.minecraft.turtle_master
tag @s[nbt={Effects:[{Id:11b},{Id:2b}]},nbt=!{Effects:[{Id:11b,Amplifier:5b},{Id:2b,Amplifier:4b}]}] add tcc.area_effect_cloud.effect.minecraft.turtle_master

tag @s[nbt={Potion:"minecraft:luck"}] add tcc.area_effect_cloud.effect.minecraft.luck

tag @s[nbt={Potion:"minecraft:night_vision"}] add tcc.area_effect_cloud.effect.minecraft.night_vision

tag @s[nbt={Potion:"minecraft:strong_healing"}] add tcc.area_effect_cloud.effect.minecraft.strong_healing
tag @s[nbt={Effects:[{Id:6b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_healing

tag @s[nbt={Potion:"minecraft:strong_swiftness"}] add tcc.area_effect_cloud.effect.minecraft.strong_swiftness
tag @s[nbt={Effects:[{Id:1b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_swiftness

tag @s[nbt={Potion:"minecraft:strong_slowness"}] add tcc.area_effect_cloud.effect.minecraft.strong_slowness
tag @s[nbt={Effects:[{Id:2b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_slowness

tag @s[nbt={Potion:"minecraft:strong_strength"}] add tcc.area_effect_cloud.effect.minecraft.strong_strength
tag @s[nbt={Effects:[{Id:5b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_strength

tag @s[nbt={Potion:"minecraft:strong_leaping"}] add tcc.area_effect_cloud.effect.minecraft.strong_leaping
tag @s[nbt={Effects:[{Id:8b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_leaping

tag @s[nbt={Potion:"minecraft:strong_regeneration"}] add tcc.area_effect_cloud.effect.minecraft.strong_regeneration
tag @s[nbt={Effects:[{Id:10b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_regeneration

tag @s[nbt={Potion:"minecraft:strong_poison"}] add tcc.area_effect_cloud.effect.minecraft.strong_poison
tag @s[nbt={Effects:[{Id:19b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_poison

tag @s[nbt={Potion:"minecraft:strong_harming"}] add tcc.area_effect_cloud.effect.minecraft.strong_harming
tag @s[nbt={Effects:[{Id:7b,Amplifier:1b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_harming

tag @s[nbt={Potion:"minecraft:strong_turtle_master"}] add tcc.area_effect_cloud.effect.minecraft.strong_turtle_master
tag @s[nbt={Effects:[{Id:11b,Amplifier:5b},{Id:2b,Amplifier:3b}]}] add tcc.area_effect_cloud.effect.minecraft.strong_turtle_master

tag @s[nbt={Effects:[{Id:11b}]},nbt=!{Effects:[{Id:2b,Amplifier:5b},{Id:11b,Amplifier:3b}]},nbt=!{Effects:[{Id:2b,Amplifier:3b},{Id:11b,Amplifier:2b}]}] add tcc.area_effect_cloud.effect.minecraft.resistance



tag @s[nbt={Potion:"minecraft:long_turtle_master"}] add tcc.area_effect_cloud.effect.minecraft.long_turtle_master

tag @s[nbt={Potion:"minecraft:long_regeneration"}] add tcc.area_effect_cloud.effect.minecraft.long_regeneration

tag @s[nbt={Potion:"minecraft:long_poison"}] add tcc.area_effect_cloud.effect.minecraft.long_poison

tag @s[nbt={Potion:"minecraft:long_water_breathing"}] add tcc.area_effect_cloud.effect.minecraft.long_water_breathing

tag @s[nbt={Potion:"minecraft:long_fire_resistance"}] add tcc.area_effect_cloud.effect.minecraft.long_fire_resistance

tag @s[nbt={Potion:"minecraft:long_night_vision"}] add tcc.area_effect_cloud.effect.minecraft.long_night_vision

tag @s[nbt={Potion:"minecraft:long_leaping"}] add tcc.area_effect_cloud.effect.minecraft.long_leaping

tag @s[nbt={Potion:"minecraft:long_slowness"}] add tcc.area_effect_cloud.effect.minecraft.long_slowness

tag @s[nbt={Potion:"minecraft:long_swiftness"}] add tcc.area_effect_cloud.effect.minecraft.long_swiftness

tag @s[nbt={Potion:"minecraft:long_strength"}] add tcc.area_effect_cloud.effect.minecraft.long_strength

tag @s[nbt={Potion:"minecraft:long_weakness"}] add tcc.area_effect_cloud.effect.minecraft.long_weakness

tag @s[nbt={Potion:"minecraft:long_invisibility"}] add tcc.area_effect_cloud.effect.minecraft.long_invisibility


# Speed: Has an amplifier
# Slowness: Has an amplifier
tag @s[nbt={Effects:[{Id:3b}]}] add tcc.area_effect_cloud.effect.minecraft.haste
tag @s[nbt={Effects:[{Id:4b}]}] add tcc.area_effect_cloud.effect.minecraft.mining_fatigue
# Strength: Has an amplifier
# Healing: Has an amplifier
# Harming: Has an amplifier
# Jump Boost: Has an amplifier
tag @s[nbt={Effects:[{Id:9b}]}] add tcc.area_effect_cloud.effect.minecraft.nausea
# Regeneration: Has an amplifier
# Resistance: Has an amplifier/variants
tag @s[nbt={Effects:[{Id:12b}]}] add tcc.area_effect_cloud.effect.minecraft.fire_resistance
tag @s[nbt={Effects:[{Id:13b}]}] add tcc.area_effect_cloud.effect.minecraft.water_breathing
tag @s[nbt={Effects:[{Id:14b}]}] add tcc.area_effect_cloud.effect.minecraft.invisibility
tag @s[nbt={Effects:[{Id:15b}]}] add tcc.area_effect_cloud.effect.minecraft.blindness
tag @s[nbt={Effects:[{Id:16b}]}] add tcc.area_effect_cloud.effect.minecraft.night_vision
tag @s[nbt={Effects:[{Id:17b}]}] add tcc.area_effect_cloud.effect.minecraft.hunger
tag @s[nbt={Effects:[{Id:18b}]}] add tcc.area_effect_cloud.effect.minecraft.weakness
# Poison: Has an amplifier
tag @s[nbt={Effects:[{Id:20b}]}] add tcc.area_effect_cloud.effect.minecraft.wither
tag @s[nbt={Effects:[{Id:21b}]}] add tcc.area_effect_cloud.effect.minecraft.health_boost
tag @s[nbt={Effects:[{Id:22b}]}] add tcc.area_effect_cloud.effect.minecraft.absorption
tag @s[nbt={Effects:[{Id:23b}]}] add tcc.area_effect_cloud.effect.minecraft.saturation
tag @s[nbt={Effects:[{Id:24b}]}] add tcc.area_effect_cloud.effect.minecraft.glowing
tag @s[nbt={Effects:[{Id:25b}]}] add tcc.area_effect_cloud.effect.minecraft.levitation
tag @s[nbt={Effects:[{Id:26b}]}] add tcc.area_effect_cloud.effect.minecraft.luck
tag @s[nbt={Effects:[{Id:27b}]}] add tcc.area_effect_cloud.effect.minecraft.unluck
tag @s[nbt={Effects:[{Id:28b}]}] add tcc.area_effect_cloud.effect.minecraft.slow_falling
tag @s[nbt={Effects:[{Id:29b}]}] add tcc.area_effect_cloud.effect.minecraft.conduit_power
tag @s[nbt={Effects:[{Id:30b}]}] add tcc.area_effect_cloud.effect.minecraft.dolphins_grace
tag @s[nbt={Effects:[{Id:31b}]}] add tcc.area_effect_cloud.effect.minecraft.bad_omen

tag @s add tcc.area_effect_cloud.tagged
