############################################################
# Description: Makes the timbering enchantment find barks above it
# Creator: CreeperMagnet_
############################################################

particle block minecraft:jungle_wood ~ ~ ~ 0.5 0.5 0.5 1 1 force
setblock ~ ~ ~ air
scoreboard players add @s tcc.logcount 1
execute positioned ~ ~1 ~ if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute positioned ~1 ~1 ~ if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute positioned ~-1 ~1 ~ if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute positioned ~ ~1 ~1 if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute positioned ~ ~1 ~-1 if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ jungle_wood if entity @a[scores={tcc.barkbreak6=1..},distance=..10] run function tcc:charms/timbering/jungle_wood/loop
execute unless block ~ ~1 ~-1 jungle_wood unless block ~ ~1 ~1 jungle_wood unless block ~ ~1 ~ jungle_wood unless block ~1 ~1 ~ jungle_wood unless block ~-1 ~1 ~ jungle_wood unless block ~1 ~ ~ jungle_wood unless block ~-1 ~ ~ jungle_wood unless block ~ ~ ~1 jungle_wood unless block ~ ~ ~-1 jungle_wood run function tcc:charms/timbering/jungle_wood/count