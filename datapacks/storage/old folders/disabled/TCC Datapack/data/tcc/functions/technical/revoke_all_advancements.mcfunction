############################################################
# Description: Revokes all technical advancements that the player shouldn't have access to
# Creator: CreeperMagnet_
############################################################

advancement revoke @s through tcc:technical/root
title @s title {"translate":"tcc.titles.bad_command","color":"red"}
