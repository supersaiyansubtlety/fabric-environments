############################################################
# Description: Makes apple pies work
# Creator: CreeperMagnet_
############################################################

execute as @a[tag=tcc.scheduled.golden_apple_pie_full.offhand,nbt=!{Inventory:[{Slot:-106b}]}] run loot replace entity @s weapon.offhand loot tcc:items/golden_apple_pie_half
execute as @a[tag=tcc.scheduled.golden_apple_pie_full.mainhand,nbt=!{SelectedItem:{}}] run loot replace entity @s weapon.mainhand loot tcc:items/golden_apple_pie_half
tag @a[tag=tcc.scheduled.golden_apple_pie_full.offhand] remove tcc.scheduled.golden_apple_pie_full.offhand
tag @a[tag=tcc.scheduled.golden_apple_pie_full.mainhand] remove tcc.scheduled.golden_apple_pie_full.mainhand
