############################################################
# Description: A summon command for an entity.
# Creator: CreeperMagnet_
############################################################

summon zombie_pigman ~ ~ ~ {Tags:["tcc.chorus_beetle"],ActiveEffects:[{Id:14,Duration:1000000,Amplifier:0b,ShowParticles:0b}],ArmorItems:[{},{},{},{id:"minecraft:structure_block",tag:{CustomModelData:14},Count:1b}],CustomName:"{\"translate\":\"entity.tcc.chorus_beetle\"}",Silent:1b,Attributes:[{Name:"generic.maxHealth",Base:200},{Name:"generic.attackDamage",Base:10},{Name:"generic.knockbackResistance",Base:1},{Name:"generic.followRange",Base:50},{Name:"generic.movementSpeed",Base:0.22}],Health:200.0f,CanPickUpLoot:0b,ArmorDropChances:[-1000000.0f,-1000000.0f,-1000000.0f,-1000000.0f],HandDropChances:[-1000000.0f,-1000000.0f],DeathLootTable:"tcc:entities/chorus_beetle"}
