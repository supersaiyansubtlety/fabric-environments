############################################################
# Description: Makes the veining enchantment find ore around
# Creator: CreeperMagnet_
############################################################

particle block minecraft:gold_ore ~ ~ ~ 0.5 0.5 0.5 1 1 force
execute as @a[scores={tcc.minegoldore=1..},distance=..10] run loot spawn ~ ~ ~ mine ~ ~ ~ mainhand
setblock ~ ~ ~ air
execute positioned ~ ~1 ~ if block ~ ~ ~ gold_ore if entity @a[scores={tcc.minegoldore=1..},distance=..10] run function tcc:charms/veining/gold_ore/loop
execute positioned ~ ~-1 ~ if block ~ ~ ~ gold_ore if entity @a[scores={tcc.minegoldore=1..},distance=..10] run function tcc:charms/veining/gold_ore/loop
execute positioned ~1 ~ ~ if block ~ ~ ~ gold_ore if entity @a[scores={tcc.minegoldore=1..},distance=..10] run function tcc:charms/veining/gold_ore/loop
execute positioned ~-1 ~ ~ if block ~ ~ ~ gold_ore if entity @a[scores={tcc.minegoldore=1..},distance=..10] run function tcc:charms/veining/gold_ore/loop
execute positioned ~ ~ ~1 if block ~ ~ ~ gold_ore if entity @a[scores={tcc.minegoldore=1..},distance=..10] run function tcc:charms/veining/gold_ore/loop
execute positioned ~ ~ ~-1 if block ~ ~ ~ gold_ore if entity @a[scores={tcc.minegoldore=1..},distance=..10] run function tcc:charms/veining/gold_ore/loop