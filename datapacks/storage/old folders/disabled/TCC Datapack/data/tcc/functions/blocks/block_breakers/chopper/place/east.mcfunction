############################################################
# Description: Commands for placing a block breaker
# Creator: CreeperMagnet_
############################################################

setblock ~ ~ ~ dropper[facing=west]{CustomName:"{\"translate\":\"block.tcc.chopper\"}"}
summon armor_stand ~ ~0.5 ~ {Rotation:[270.0f,0.0f],ArmorItems:[{},{},{},{id:"minecraft:barrier",Count:1b,tag:{CustomModelData:1}}],Tags:["tcc.blocks.chopper","tcc.block_breakers.east","tcc.blocks.block_breaker"],NoGravity:1b,Invisible:1b,Marker:1b}
