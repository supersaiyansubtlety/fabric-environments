############################################################
# Description: Makes obsidian scythe durability work
# Creator: CreeperMagnet_
############################################################

execute as @s[scores={tcc.minebeets=1..}] run function tcc:items/obsidian_scythe/damage
execute as @s[scores={tcc.minepotatoes=1..}] run function tcc:items/obsidian_scythe/damage
execute as @s[scores={tcc.minewheat=1..}] run function tcc:items/obsidian_scythe/damage
execute as @s[scores={tcc.minecarrots=1..}] run function tcc:items/obsidian_scythe/damage
