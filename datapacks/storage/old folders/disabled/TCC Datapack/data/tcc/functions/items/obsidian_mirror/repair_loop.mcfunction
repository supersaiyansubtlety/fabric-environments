############################################################
# Description: Repairs a boomerang
# Creator: CreeperMagnet_
############################################################

loot replace entity @s weapon.mainhand loot tcc:technical/obsidian_mirror_durability/repair
xp add @s -1 points
scoreboard players add @s tcc.old_xp 1
execute as @s[nbt={SelectedItem:{tag:{tcc:{Durability:20,Item:"obsidian_mirror"}}}}] run scoreboard players reset @s tcc.old_xp
execute as @s[scores={tcc.old_xp=0}] run scoreboard players reset @s tcc.old_xp
execute as @s[scores={tcc.old_xp=..-1},nbt=!{SelectedItem:{tag:{tcc:{Durability:20,Item:"obsidian_mirror"}}}}] run function tcc:items/obsidian_mirror/repair_loop
