############################################################
# Description: Commands to run when you're holding a charmed item
# Creator: CreeperMagnet_
############################################################
### Iron Ore
execute as @s[scores={tcc.mineironore=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}},nbt=!{SelectedItem:{tag:{tcc:{Charms:["smoldering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:iron_ore"}}] at @s align xyz run function tcc:charms/veining/iron_ore/main
execute as @s[scores={tcc.mineironore=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}},nbt={SelectedItem:{tag:{tcc:{Charms:["smoldering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:iron_ingot"}}] at @s align xyz run function tcc:charms/veining/iron_ore/main
### Gold Ore
execute as @s[scores={tcc.minegoldore=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}},nbt=!{SelectedItem:{tag:{tcc:{Charms:["smoldering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:gold_ore"}}] at @s align xyz run function tcc:charms/veining/gold_ore/main
execute as @s[scores={tcc.minegoldore=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}},nbt={SelectedItem:{tag:{tcc:{Charms:["smoldering"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:gold_ingot"}}] at @s align xyz run function tcc:charms/veining/gold_ore/main
### Diamond Ore
execute as @s[scores={tcc.minediamond=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:diamond"}},limit=1] at @s align xyz run function tcc:charms/veining/diamond_ore/main
### Coal Ore
execute as @s[scores={tcc.minecoalore=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:coal"}},limit=1] at @s align xyz run function tcc:charms/veining/coal_ore/main
### Emerald Ore
execute as @s[scores={tcc.mineemerald=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:emerald"}},limit=1] at @s align xyz run function tcc:charms/veining/emerald_ore/main
### Quartz Ore
execute as @s[scores={tcc.minequartz=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:quartz"}},limit=1] at @s align xyz run function tcc:charms/veining/quartz_ore/main
### Redstone Ore
execute as @s[scores={tcc.mineredstone=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:redstone"}},limit=1] at @s align xyz run function tcc:charms/veining/redstone_ore/main
### Lapis Ore
execute as @s[scores={tcc.minelapisore=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:lapis_lazuli"}},limit=1] at @s align xyz run function tcc:charms/veining/lapis_ore/main
### Diorite
execute as @s[scores={tcc.minediorite=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:diorite"}},limit=1] at @s align xyz run function tcc:charms/veining/diorite/main
### Granite
execute as @s[scores={tcc.minegranite=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:granite"}},limit=1] at @s align xyz run function tcc:charms/veining/granite/main
### Andesite
execute as @s[scores={tcc.mineandesite=1..},nbt={SelectedItem:{tag:{tcc:{Charms:["veining"]}}}}] as @e[sort=arbitrary,type=item,nbt={PickupDelay:10s,Item:{id:"minecraft:andesite"}},limit=1] at @s align xyz run function tcc:charms/veining/andesite/main
