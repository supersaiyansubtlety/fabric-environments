############################################################
# Description: Finishes a quest
# Creator: CreeperMagnet_
############################################################

tag @s remove tcc.player.quest.warrior.started
advancement revoke @s through tcc:technical/items/quest_book/warrior/display/root
give @s carrot_on_a_stick{CustomModelData:24,Unbreakable:1b,HideFlags:4,tcc:{Item:"quest_book_complete"},display:{Name:"{\"translate\":\"item.tcc.quest_book_complete\",\"italic\":\"false\"}",Lore:["{\"translate\":\"advancements.tcc.quests.warrior.root.title\",\"color\":\"gray\",\"italic\":\"false\"}","{\"translate\":\"tcc.lore.tooltip\"}"]}}
tellraw @a ["",{"selector":"@s"},{"translate":"tcc.tellraws.quests.complete"},{"translate":"tcc.tellraws.quests.warrior.title","hoverEvent":{"action":"show_text","value":{"text":"","extra":[{"translate":"advancements.tcc.quests.warrior.title"},{"text":"\n"},{"translate":"tcc.tellraws.quests.warrior.subtitle"}]}}}]
