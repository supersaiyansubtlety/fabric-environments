############################################################
# Description: Repairs a wrench
# Creator: CreeperMagnet_
############################################################

execute as @s[nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:200,Item:"wrench"}}}]}] run function tcc:items/wrench/repair/offhand/add_durability
xp add @s -1 points
scoreboard players add @s tcc.old_xp 1
execute as @s[nbt={Inventory:[{Slot:-106b,tag:{tcc:{Durability:200,Item:"wrench"}}}]}] run scoreboard players set @s tcc.old_xp 0
execute as @s[scores={tcc.old_xp=0}] run function tcc:items/wrench/repair/offhand/merge_lore
execute as @s[scores={tcc.old_xp=0}] run scoreboard players reset @s tcc.old_xp
execute as @s[scores={tcc.old_xp=..-1},nbt=!{Inventory:[{Slot:-106b,tag:{tcc:{Durability:200,Item:"wrench"}}}]}] run function tcc:items/wrench/repair/offhand/loop
