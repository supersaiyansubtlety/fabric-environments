############################################################
# Description: Finishes a quest
# Creator: CreeperMagnet_
############################################################

tag @s remove tcc.player.quest.healer.started
advancement revoke @s through tcc:technical/items/quest_book/healer/display/root
give @s carrot_on_a_stick{CustomModelData:24,Unbreakable:1b,HideFlags:4,tcc:{Item:"quest_book_complete"},display:{Name:"{\"translate\":\"item.tcc.quest_book_complete\",\"italic\":\"false\"}",Lore:["{\"translate\":\"advancements.tcc.quests.healer.root.title\",\"color\":\"gray\",\"italic\":\"false\"}","{\"translate\":\"tcc.lore.tooltip\"}"]}}
tellraw @a ["",{"selector":"@s"},{"translate":"tcc.tellraws.quests.complete"},{"translate":"tcc.tellraws.quests.healer.title","hoverEvent":{"action":"show_text","value":{"text":"","extra":[{"translate":"advancements.tcc.quests.healer.title"},{"text":"\n"},{"translate":"tcc.tellraws.quests.healer.subtitle"}]}}}]
