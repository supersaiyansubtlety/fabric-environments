############################################################
# Description: Makes obsidian mirrors flip effects
# Creator: CreeperMagnet_
############################################################

execute unless entity @s[nbt=!{SelectedItem:{tag:{Enchantments:[{lvl:1s,id:"minecraft:unbreaking"}]}}},nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:1s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/2
execute unless entity @s[nbt=!{SelectedItem:{tag:{Enchantments:[{lvl:2s,id:"minecraft:unbreaking"}]}}},nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:2s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/3
execute unless entity @s[nbt=!{SelectedItem:{tag:{Enchantments:[{lvl:3s,id:"minecraft:unbreaking"}]}}},nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{lvl:3s,id:"minecraft:unbreaking"}]}}]}] run function tcc:technical/random/4
execute as @s[nbt=!{Inventory:[{Slot:-106b,tag:{Enchantments:[{id:"minecraft:unbreaking"}]}}]},nbt=!{SelectedItem:{tag:{Enchantments:[{id:"minecraft:unbreaking"}]}}}] run scoreboard players set @s tcc.random 1

execute as @s[nbt={ActiveEffects:[{Id:20b,Amplifier:0b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:20b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s regeneration 15 0
execute as @s[scores={tcc.math=300..600}] run effect give @s regeneration 30 0
execute as @s[scores={tcc.math=600..1200}] run effect give @s regeneration 45 0
execute as @s[scores={tcc.math=1200..}] run effect give @s regeneration 60 0
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:20b,Amplifier:1b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:20b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s regeneration 15 1
execute as @s[scores={tcc.math=300..600}] run effect give @s regeneration 30 1
execute as @s[scores={tcc.math=600..1200}] run effect give @s regeneration 45 1
execute as @s[scores={tcc.math=1200..}] run effect give @s regeneration 60 1
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:19b,Amplifier:0b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:19b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s regeneration 15 0
execute as @s[scores={tcc.math=300..600}] run effect give @s regeneration 30 0
execute as @s[scores={tcc.math=600..1200}] run effect give @s regeneration 45 0
execute as @s[scores={tcc.math=1200..}] run effect give @s regeneration 60 0
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:19b,Amplifier:3b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:19b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s regeneration 15 3
execute as @s[scores={tcc.math=300..600}] run effect give @s regeneration 30 3
execute as @s[scores={tcc.math=600..1200}] run effect give @s regeneration 45 3
execute as @s[scores={tcc.math=1200..}] run effect give @s regeneration 60 3
scoreboard players reset @s tcc.math


execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:0b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:2b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s speed 15
execute as @s[scores={tcc.math=300..600}] run effect give @s speed 30
execute as @s[scores={tcc.math=600..1200}] run effect give @s speed 45
execute as @s[scores={tcc.math=1200..1500}] run effect give @s speed 60
execute as @s[scores={tcc.math=1500..1800}] run effect give @s speed 75
execute as @s[scores={tcc.math=1800..2100}] run effect give @s speed 90
execute as @s[scores={tcc.math=2100..2400}] run effect give @s speed 105
execute as @s[scores={tcc.math=2400..2700}] run effect give @s speed 120
execute as @s[scores={tcc.math=2700..3000}] run effect give @s speed 135
execute as @s[scores={tcc.math=3000..3300}] run effect give @s speed 150
execute as @s[scores={tcc.math=3300..3600}] run effect give @s speed 165
execute as @s[scores={tcc.math=3600..3900}] run effect give @s speed 180
execute as @s[scores={tcc.math=3900..4200}] run effect give @s speed 195
execute as @s[scores={tcc.math=4200..4500}] run effect give @s speed 210
execute as @s[scores={tcc.math=4500..4800}] run effect give @s speed 225
execute as @s[scores={tcc.math=4800..5100}] run effect give @s speed 240
execute as @s[scores={tcc.math=5100..5400}] run effect give @s speed 255
execute as @s[scores={tcc.math=5400..5700}] run effect give @s speed 270
execute as @s[scores={tcc.math=5700..6000}] run effect give @s speed 285
execute as @s[scores={tcc.math=6000..}] run effect give @s speed 300
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:3b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:2b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s speed 15 3
execute as @s[scores={tcc.math=300..600}] run effect give @s speed 30 3
execute as @s[scores={tcc.math=600..1200}] run effect give @s speed 45 3
execute as @s[scores={tcc.math=1200..1500}] run effect give @s speed 60 3
execute as @s[scores={tcc.math=1500..1800}] run effect give @s speed 75 3
execute as @s[scores={tcc.math=1800..2100}] run effect give @s speed 90 3
execute as @s[scores={tcc.math=2100..2400}] run effect give @s speed 105 3
execute as @s[scores={tcc.math=2400..2700}] run effect give @s speed 120 3
execute as @s[scores={tcc.math=2700..3000}] run effect give @s speed 135 3
execute as @s[scores={tcc.math=3000..3300}] run effect give @s speed 150 3
execute as @s[scores={tcc.math=3300..3600}] run effect give @s speed 165 3
execute as @s[scores={tcc.math=3600..3900}] run effect give @s speed 180 3
execute as @s[scores={tcc.math=3900..4200}] run effect give @s speed 195 3
execute as @s[scores={tcc.math=4200..4500}] run effect give @s speed 210 3
execute as @s[scores={tcc.math=4500..4800}] run effect give @s speed 225 3
execute as @s[scores={tcc.math=4800..5100}] run effect give @s speed 240 3
execute as @s[scores={tcc.math=5100..5400}] run effect give @s speed 255 3
execute as @s[scores={tcc.math=5400..5700}] run effect give @s speed 270 3
execute as @s[scores={tcc.math=5700..6000}] run effect give @s speed 285 3
execute as @s[scores={tcc.math=6000..}] run effect give @s speed 300 3
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:5b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:2b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s speed 15 5
execute as @s[scores={tcc.math=300..600}] run effect give @s speed 30 5
execute as @s[scores={tcc.math=600..1200}] run effect give @s speed 45 5
execute as @s[scores={tcc.math=1200..1500}] run effect give @s speed 60 5
execute as @s[scores={tcc.math=1500..1800}] run effect give @s speed 75 5
execute as @s[scores={tcc.math=1800..2100}] run effect give @s speed 90 5
execute as @s[scores={tcc.math=2100..2400}] run effect give @s speed 105 5
execute as @s[scores={tcc.math=2400..2700}] run effect give @s speed 120 5
execute as @s[scores={tcc.math=2700..3000}] run effect give @s speed 135 5
execute as @s[scores={tcc.math=3000..3300}] run effect give @s speed 150 5
execute as @s[scores={tcc.math=3300..3600}] run effect give @s speed 165 5
execute as @s[scores={tcc.math=3600..3900}] run effect give @s speed 180 5
execute as @s[scores={tcc.math=3900..4200}] run effect give @s speed 195 5
execute as @s[scores={tcc.math=4200..4500}] run effect give @s speed 210 5
execute as @s[scores={tcc.math=4500..4800}] run effect give @s speed 225 5
execute as @s[scores={tcc.math=4800..5100}] run effect give @s speed 240 5
execute as @s[scores={tcc.math=5100..5400}] run effect give @s speed 255 5
execute as @s[scores={tcc.math=5400..5700}] run effect give @s speed 270 5
execute as @s[scores={tcc.math=5700..6000}] run effect give @s speed 285 5
execute as @s[scores={tcc.math=6000..}] run effect give @s speed 300 5
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:18b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:18b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s strength 15 0
execute as @s[scores={tcc.math=300..600}] run effect give @s strength 30 0
execute as @s[scores={tcc.math=600..1200}] run effect give @s strength 45 0
execute as @s[scores={tcc.math=1200..1500}] run effect give @s strength 60 0
execute as @s[scores={tcc.math=1500..1800}] run effect give @s strength 75 0
execute as @s[scores={tcc.math=1800..2100}] run effect give @s strength 90 0
execute as @s[scores={tcc.math=2100..2400}] run effect give @s strength 105 0
execute as @s[scores={tcc.math=2400..2700}] run effect give @s strength 120 0
execute as @s[scores={tcc.math=2700..3000}] run effect give @s strength 135 0
execute as @s[scores={tcc.math=3000..3300}] run effect give @s strength 150 0
execute as @s[scores={tcc.math=3300..3600}] run effect give @s strength 165 0
execute as @s[scores={tcc.math=3600..}] run effect give @s strength 180 0
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:9b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:9b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s night_vision 15 0
execute as @s[scores={tcc.math=300..600}] run effect give @s night_vision 30 0
execute as @s[scores={tcc.math=600..1200}] run effect give @s night_vision 45 0
execute as @s[scores={tcc.math=1200..1500}] run effect give @s night_vision 60 0
execute as @s[scores={tcc.math=1500..1800}] run effect give @s night_vision 75 0
execute as @s[scores={tcc.math=1800..2100}] run effect give @s night_vision 90 0
execute as @s[scores={tcc.math=2100..2400}] run effect give @s night_vision 105 0
execute as @s[scores={tcc.math=2400..2700}] run effect give @s night_vision 120 0
execute as @s[scores={tcc.math=2700..3000}] run effect give @s night_vision 135 0
execute as @s[scores={tcc.math=3000..3300}] run effect give @s night_vision 150 0
execute as @s[scores={tcc.math=3300..3600}] run effect give @s night_vision 165 0
execute as @s[scores={tcc.math=3600..}] run effect give @s night_vision 180 0
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:15b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:15b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s night_vision 15 0
execute as @s[scores={tcc.math=300..600}] run effect give @s night_vision 30 0
execute as @s[scores={tcc.math=600..1200}] run effect give @s night_vision 45 0
execute as @s[scores={tcc.math=1200..1500}] run effect give @s night_vision 60 0
execute as @s[scores={tcc.math=1500..1800}] run effect give @s night_vision 75 0
execute as @s[scores={tcc.math=1800..2100}] run effect give @s night_vision 90 0
execute as @s[scores={tcc.math=2100..2400}] run effect give @s night_vision 105 0
execute as @s[scores={tcc.math=2400..2700}] run effect give @s night_vision 120 0
execute as @s[scores={tcc.math=2700..3000}] run effect give @s night_vision 135 0
execute as @s[scores={tcc.math=3000..3300}] run effect give @s night_vision 150 0
execute as @s[scores={tcc.math=3300..3600}] run effect give @s night_vision 165 0
execute as @s[scores={tcc.math=3600..}] run effect give @s night_vision 180 0
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:4b,Amplifier:2b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:4b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s haste 15 2
execute as @s[scores={tcc.math=300..600}] run effect give @s haste 30 2
execute as @s[scores={tcc.math=600..1200}] run effect give @s haste 45 2
execute as @s[scores={tcc.math=1200..1500}] run effect give @s haste 60 2
execute as @s[scores={tcc.math=1500..1800}] run effect give @s haste 75 2
execute as @s[scores={tcc.math=1800..2100}] run effect give @s haste 90 2
execute as @s[scores={tcc.math=2100..2400}] run effect give @s haste 105 2
execute as @s[scores={tcc.math=2400..2700}] run effect give @s haste 120 2
execute as @s[scores={tcc.math=2700..3000}] run effect give @s haste 135 2
execute as @s[scores={tcc.math=3000..3300}] run effect give @s haste 150 2
execute as @s[scores={tcc.math=3300..3600}] run effect give @s haste 165 2
execute as @s[scores={tcc.math=3600..3900}] run effect give @s haste 180 2
execute as @s[scores={tcc.math=3900..4200}] run effect give @s haste 195 2
execute as @s[scores={tcc.math=4200..4500}] run effect give @s haste 210 2
execute as @s[scores={tcc.math=4500..4800}] run effect give @s haste 225 2
execute as @s[scores={tcc.math=4800..5100}] run effect give @s haste 240 2
execute as @s[scores={tcc.math=5100..5400}] run effect give @s haste 255 2
execute as @s[scores={tcc.math=5400..5700}] run effect give @s haste 270 2
execute as @s[scores={tcc.math=5700..6000}] run effect give @s haste 285 2
execute as @s[scores={tcc.math=6000..}] run effect give @s haste 300 2
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:27b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:27b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s luck 15
execute as @s[scores={tcc.math=300..600}] run effect give @s luck 30
execute as @s[scores={tcc.math=600..1200}] run effect give @s luck 45
execute as @s[scores={tcc.math=1200..1500}] run effect give @s luck 60
execute as @s[scores={tcc.math=1500..1800}] run effect give @s luck 75
execute as @s[scores={tcc.math=1800..2100}] run effect give @s luck 90
execute as @s[scores={tcc.math=2100..2400}] run effect give @s luck 105
execute as @s[scores={tcc.math=2400..2700}] run effect give @s luck 120
execute as @s[scores={tcc.math=2700..3000}] run effect give @s luck 135
execute as @s[scores={tcc.math=3000..3300}] run effect give @s luck 150
execute as @s[scores={tcc.math=3300..3600}] run effect give @s luck 165
execute as @s[scores={tcc.math=3600..3900}] run effect give @s luck 180
execute as @s[scores={tcc.math=3900..4200}] run effect give @s luck 195
execute as @s[scores={tcc.math=4200..4500}] run effect give @s luck 210
execute as @s[scores={tcc.math=4500..4800}] run effect give @s luck 225
execute as @s[scores={tcc.math=4800..5100}] run effect give @s luck 240
execute as @s[scores={tcc.math=5100..5400}] run effect give @s luck 255
execute as @s[scores={tcc.math=5400..5700}] run effect give @s luck 270
execute as @s[scores={tcc.math=5700..6000}] run effect give @s luck 285
execute as @s[scores={tcc.math=6000..}] run effect give @s luck 300
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:25b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:25b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s slow_falling 15
execute as @s[scores={tcc.math=300..600}] run effect give @s slow_falling 30
execute as @s[scores={tcc.math=600..1200}] run effect give @s slow_falling 45
execute as @s[scores={tcc.math=1200..1500}] run effect give @s slow_falling 60
execute as @s[scores={tcc.math=1500..1800}] run effect give @s slow_falling 75
execute as @s[scores={tcc.math=1800..2100}] run effect give @s slow_falling 90
execute as @s[scores={tcc.math=2100..2400}] run effect give @s slow_falling 105
execute as @s[scores={tcc.math=2400..2700}] run effect give @s slow_falling 120
execute as @s[scores={tcc.math=2700..3000}] run effect give @s slow_falling 135
execute as @s[scores={tcc.math=3000..3300}] run effect give @s slow_falling 150
execute as @s[scores={tcc.math=3300..3600}] run effect give @s slow_falling 165
execute as @s[scores={tcc.math=3600..3900}] run effect give @s slow_falling 180
execute as @s[scores={tcc.math=3900..4200}] run effect give @s slow_falling 195
execute as @s[scores={tcc.math=4200..4500}] run effect give @s slow_falling 210
execute as @s[scores={tcc.math=4500..4800}] run effect give @s slow_falling 225
execute as @s[scores={tcc.math=4800..5100}] run effect give @s slow_falling 240
execute as @s[scores={tcc.math=5100..5400}] run effect give @s slow_falling 255
execute as @s[scores={tcc.math=5400..5700}] run effect give @s slow_falling 270
execute as @s[scores={tcc.math=5700..6000}] run effect give @s slow_falling 285
execute as @s[scores={tcc.math=6000..}] run effect give @s slow_falling 300
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:24b}]}] run execute store result score @s tcc.math run data get entity @s ActiveEffects[{Id:24b}].Duration
execute as @s[scores={tcc.math=..300}] run effect give @s invisibility 15
execute as @s[scores={tcc.math=300..600}] run effect give @s invisibility 30
execute as @s[scores={tcc.math=600..1200}] run effect give @s invisibility 45
execute as @s[scores={tcc.math=1200..1500}] run effect give @s invisibility 60
execute as @s[scores={tcc.math=1500..1800}] run effect give @s invisibility 75
execute as @s[scores={tcc.math=1800..2100}] run effect give @s invisibility 90
execute as @s[scores={tcc.math=2100..2400}] run effect give @s invisibility 105
execute as @s[scores={tcc.math=2400..2700}] run effect give @s invisibility 120
execute as @s[scores={tcc.math=2700..3000}] run effect give @s invisibility 135
execute as @s[scores={tcc.math=3000..3300}] run effect give @s invisibility 150
execute as @s[scores={tcc.math=3300..3600}] run effect give @s invisibility 165
execute as @s[scores={tcc.math=3600..3900}] run effect give @s invisibility 180
execute as @s[scores={tcc.math=3900..4200}] run effect give @s invisibility 195
execute as @s[scores={tcc.math=4200..4500}] run effect give @s invisibility 210
execute as @s[scores={tcc.math=4500..4800}] run effect give @s invisibility 225
execute as @s[scores={tcc.math=4800..5100}] run effect give @s invisibility 240
execute as @s[scores={tcc.math=5100..5400}] run effect give @s invisibility 255
execute as @s[scores={tcc.math=5400..5700}] run effect give @s invisibility 270
execute as @s[scores={tcc.math=5700..6000}] run effect give @s invisibility 285
execute as @s[scores={tcc.math=6000..}] run effect give @s invisibility 300
scoreboard players reset @s tcc.math

execute as @s[nbt={ActiveEffects:[{Id:17b}]}] run effect give @s saturation 1 5 true
playsound minecraft:block.enchantment_table.use player @s ~ ~ ~ 10000 0

execute as @s[nbt={ActiveEffects:[{Id:18b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:2b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:3b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:3b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:3b}]}] run scoreboard players add @s tcc.math 1

execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:5b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:5b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:5b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:5b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:2b,Amplifier:5b}]}] run scoreboard players add @s tcc.math 1

execute as @s[nbt={ActiveEffects:[{Id:17b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:17b,Amplifier:2b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:17b,Amplifier:2b}]}] run scoreboard players add @s tcc.math 1

execute as @s[nbt={ActiveEffects:[{Id:4b,Amplifier:2b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:4b,Amplifier:2b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:4b,Amplifier:2b}]}] run scoreboard players add @s tcc.math 1

execute as @s[nbt={ActiveEffects:[{Id:20b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:20b,Amplifier:1b}]}] run scoreboard players add @s tcc.math 1


execute as @s[nbt={ActiveEffects:[{Id:19b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:19b,Amplifier:3b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:19b,Amplifier:3b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:19b,Amplifier:3b}]}] run scoreboard players add @s tcc.math 1



execute as @s[nbt={ActiveEffects:[{Id:9b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:9b,Amplifier:1b}]}] run scoreboard players add @s tcc.math 1


execute as @s[nbt={ActiveEffects:[{Id:15b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:25b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:24b}]}] run scoreboard players add @s tcc.math 1
execute as @s[nbt={ActiveEffects:[{Id:27b}]}] run scoreboard players add @s tcc.math 1

execute as @s[nbt={SelectedItem:{tag:{tcc:{Item:"obsidian_mirror"}}}}] run function tcc:items/obsidian_mirror/damage/mainhand
execute as @s[nbt=!{SelectedItem:{tag:{tcc:{Item:"obsidian_mirror"}}}},nbt={Inventory:[{Slot:-106b,tag:{tcc:{Item:"obsidian_mirror"}}}]}] run function tcc:items/obsidian_mirror/damage/offhand
advancement grant @s only tcc:minecraft/story/obsidian_mirror


#bad omen goes here? no, too op to flip into hero of village
effect clear @s slowness
effect clear @s mining_fatigue
effect clear @s wither
effect clear @s poison
effect clear @s nausea
effect clear @s weakness
effect clear @s levitation
effect clear @s glowing
effect clear @s blindness
effect clear @s hunger
effect clear @s unluck
effect clear @s bad_omen
