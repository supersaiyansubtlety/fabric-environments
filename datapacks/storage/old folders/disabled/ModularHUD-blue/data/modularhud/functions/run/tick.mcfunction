scoreboard players add -ticks vs_mh_timer 1
execute if score -ticks vs_mh_timer matches 21 run scoreboard players set -ticks vs_mh_timer 1

execute if score -editing vs_mh_configedit matches 1.. run function modularhud:configuration/process
execute if score -ticks vs_mh_timer matches 5 run function modularhud:configuration/enforce

execute unless score -displayhud vs_mh_config matches 0 unless score -trigger vs_mh_config matches 0 run function modularhud:trigger/display

execute unless score -session vs_mh_config matches 0 if score -ticks vs_mh_timer matches 20 run function modularhud:modules/session
execute unless score -displayhud vs_mh_config matches 0 unless score -position vs_mh_config matches 0 run function modularhud:modules/position
execute unless score -displayhud vs_mh_config matches 0 unless score -hmobs vs_mh_config matches 0 if score -ticks vs_mh_timer matches 10 run function modularhud:modules/hostilemobs
execute unless score -displayhud vs_mh_config matches 0 unless score -pitch vs_mh_config matches 0 run function modularhud:modules/pitch
execute unless score -displayhud vs_mh_config matches 0 unless score -time vs_mh_config matches 0 run function modularhud:modules/time
execute unless score -displayhud vs_mh_config matches 0 unless score -direction vs_mh_config matches 0 run function modularhud:modules/direction

execute unless score -displayhud vs_mh_config matches 0 run function modularhud:hud/select

execute if score -uninstalling vs_mh_uninstall matches 1.. run function modularhud:uninstall/process