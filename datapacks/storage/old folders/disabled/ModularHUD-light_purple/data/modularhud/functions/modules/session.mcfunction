execute as @a[scores={vs_mh_sesscheck=1..}] run scoreboard players reset @s vs_mh_sessticks
execute as @a[scores={vs_mh_sesscheck=1..}] run scoreboard players set @s vs_mh_sesscheck 0
execute as @a run scoreboard players add @s vs_mh_sessticks 20

execute as @a store result score @s vs_mh_sessmins run scoreboard players get @s vs_mh_sessticks
execute as @a run scoreboard players operation @s vs_mh_sessmins /= -1200 vs_mh_constants

execute as @a store result score @s vs_mh_sesshours run scoreboard players get @s vs_mh_sessticks
execute as @a run scoreboard players operation @s vs_mh_sesshours /= -72000 vs_mh_constants

execute as @a store result score @s vs_mh_sesscalc run scoreboard players get @s vs_mh_sesshours
execute as @a run scoreboard players operation @s vs_mh_sesscalc *= -60 vs_mh_constants
execute as @a run scoreboard players operation @s vs_mh_sessmins -= @s vs_mh_sesscalc