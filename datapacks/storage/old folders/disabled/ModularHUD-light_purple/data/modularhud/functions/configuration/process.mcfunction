execute if score -session vs_mh_configedit matches 100..103 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute if score -position vs_mh_configedit matches 100..103 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute if score -hmobs vs_mh_configedit matches 100..103 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute if score -pitch vs_mh_configedit matches 100..103 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute if score -time vs_mh_configedit matches 100..103 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute if score -direction vs_mh_configedit matches 100..103 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute if score -compact vs_mh_configedit matches 100..102 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute if score -displayhud vs_mh_configedit matches 100..102 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1
execute if score -trigger vs_mh_configedit matches 100..101 as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1

execute if score -session vs_mh_configedit matches 101 run scoreboard players set -session vs_mh_configedit 1
execute if score -position vs_mh_configedit matches 101 run scoreboard players set -position vs_mh_configedit 1
execute if score -hmobs vs_mh_configedit matches 101 run scoreboard players set -hmobs vs_mh_configedit 1
execute if score -pitch vs_mh_configedit matches 101 run scoreboard players set -pitch vs_mh_configedit 1
execute if score -time vs_mh_configedit matches 101 run scoreboard players set -time vs_mh_configedit 1
execute if score -direction vs_mh_configedit matches 101 run scoreboard players set -direction vs_mh_configedit 1
execute if score -compact vs_mh_configedit matches 101 run scoreboard players set -compact vs_mh_configedit 1
execute if score -displayhud vs_mh_configedit matches 101 run scoreboard players set -displayhud vs_mh_configedit 1
execute if score -trigger vs_mh_configedit matches 101 run scoreboard players set -trigger vs_mh_configedit 1

execute if score -session vs_mh_configedit matches 100 run scoreboard players set -session vs_mh_configedit 0
execute if score -position vs_mh_configedit matches 100 run scoreboard players set -position vs_mh_configedit 0
execute if score -hmobs vs_mh_configedit matches 100 run scoreboard players set -hmobs vs_mh_configedit 0
execute if score -pitch vs_mh_configedit matches 100 run scoreboard players set -pitch vs_mh_configedit 0
execute if score -time vs_mh_configedit matches 100 run scoreboard players set -time vs_mh_configedit 0
execute if score -direction vs_mh_configedit matches 100 run scoreboard players set -direction vs_mh_configedit 0
execute if score -compact vs_mh_configedit matches 100 run scoreboard players set -compact vs_mh_configedit 0
execute if score -displayhud vs_mh_configedit matches 100 run scoreboard players set -displayhud vs_mh_configedit 0
execute if score -trigger vs_mh_configedit matches 100 run scoreboard players set -trigger vs_mh_configedit 0

execute if score -session vs_mh_configedit matches 102 run scoreboard players set -session vs_mh_configedit 2
execute if score -position vs_mh_configedit matches 102 run scoreboard players set -position vs_mh_configedit 2
execute if score -hmobs vs_mh_configedit matches 102 run scoreboard players set -hmobs vs_mh_configedit 2
execute if score -pitch vs_mh_configedit matches 102 run scoreboard players set -pitch vs_mh_configedit 2
execute if score -time vs_mh_configedit matches 102 run scoreboard players set -time vs_mh_configedit 2
execute if score -direction vs_mh_configedit matches 102 run scoreboard players set -direction vs_mh_configedit 2
execute if score -compact vs_mh_configedit matches 102 run scoreboard players set -compact vs_mh_configedit 2
execute if score -displayhud vs_mh_configedit matches 102 run scoreboard players set -displayhud vs_mh_configedit 2

execute if score -session vs_mh_configedit matches 103 run scoreboard players set -session vs_mh_configedit 3
execute if score -position vs_mh_configedit matches 103 run scoreboard players set -position vs_mh_configedit 3
execute if score -hmobs vs_mh_configedit matches 103 run scoreboard players set -hmobs vs_mh_configedit 3
execute if score -pitch vs_mh_configedit matches 103 run scoreboard players set -pitch vs_mh_configedit 3
execute if score -time vs_mh_configedit matches 103 run scoreboard players set -time vs_mh_configedit 3
execute if score -direction vs_mh_configedit matches 103 run scoreboard players set -direction vs_mh_configedit 3


execute if score -editing vs_mh_configedit matches 4 run function modularhud:configuration/view
execute if score -editing vs_mh_configedit matches 2 run function modularhud:configuration/save
execute if score -editing vs_mh_configedit matches 3 run function modularhud:configuration/cancel