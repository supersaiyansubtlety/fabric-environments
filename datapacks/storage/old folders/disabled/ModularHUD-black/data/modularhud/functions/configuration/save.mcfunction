execute as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1

tellraw @a[scores={vs_mh_adminlist=1}] [{"text":"\nModularHUD","color":"gold","bold":true},{"text":" "},{"text":"configuration","color":"white","bold":true},{"color":"white","bold":true,"text":"\n - "},{"color":"white","bold":true,"text":"Configuration changes"},{"color":"white","bold":true,"text":" "},{"color":"green","bold":true,"text":"saved"}]

execute run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":" - "},{"color":"white","bold":true,"text":"Applied configuration changes"}]

execute if score -session vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Session"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ALLOW"}]
execute if score -session vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Session"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISALLOW"}]
execute if score -session vs_mh_configedit matches 2 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Session"},{"color":"white","bold":true,"text":"\n       - "},{"color":"yellow","bold":true,"text":"FORCE ON"}]
execute if score -session vs_mh_configedit matches 3 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Session"},{"color":"white","bold":true,"text":"\n       - "},{"color":"light_purple","bold":true,"text":"REQUIRE ITEM"}]

execute if score -position vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Position"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ALLOW"}]
execute if score -position vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Position"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISALLOW"}]
execute if score -position vs_mh_configedit matches 2 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Position"},{"color":"white","bold":true,"text":"\n       - "},{"color":"yellow","bold":true,"text":"FORCE ON"}]
execute if score -position vs_mh_configedit matches 3 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Position"},{"color":"white","bold":true,"text":"\n       - "},{"color":"light_purple","bold":true,"text":"REQUIRE ITEM"}]

execute if score -hmobs vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Hostile Mobs"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ALLOW"}]
execute if score -hmobs vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Hostile Mobs"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISALLOW"}]
execute if score -hmobs vs_mh_configedit matches 2 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Hostile Mobs"},{"color":"white","bold":true,"text":"\n       - "},{"color":"yellow","bold":true,"text":"FORCE ON"}]
execute if score -hmobs vs_mh_configedit matches 3 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Hostile Mobs"},{"color":"white","bold":true,"text":"\n       - "},{"color":"light_purple","bold":true,"text":"REQUIRE ITEM"}]

execute if score -pitch vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Pitch"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ALLOW"}]
execute if score -pitch vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Pitch"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISALLOW"}]
execute if score -pitch vs_mh_configedit matches 2 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Pitch"},{"color":"white","bold":true,"text":"\n       - "},{"color":"yellow","bold":true,"text":"FORCE ON"}]
execute if score -pitch vs_mh_configedit matches 3 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Pitch"},{"color":"white","bold":true,"text":"\n       - "},{"color":"light_purple","bold":true,"text":"REQUIRE ITEM"}]


execute if score -time vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Time"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ALLOW"}]
execute if score -time vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Time"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISALLOW"}]
execute if score -time vs_mh_configedit matches 2 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Time"},{"color":"white","bold":true,"text":"\n       - "},{"color":"yellow","bold":true,"text":"FORCE ON"}]
execute if score -time vs_mh_configedit matches 3 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Time"},{"color":"white","bold":true,"text":"\n       - "},{"color":"light_purple","bold":true,"text":"REQUIRE ITEM"}]

execute if score -direction vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Direction"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ALLOW"}]
execute if score -direction vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Direction"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISALLOW"}]
execute if score -direction vs_mh_configedit matches 2 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Direction"},{"color":"white","bold":true,"text":"\n       - "},{"color":"yellow","bold":true,"text":"FORCE ON"}]
execute if score -direction vs_mh_configedit matches 3 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Direction"},{"color":"white","bold":true,"text":"\n       - "},{"color":"light_purple","bold":true,"text":"REQUIRE ITEM"}]

execute if score -compact vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Compact Mode"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ALLOW"}]
execute if score -compact vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Compact Mode"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISALLOW"}]
execute if score -compact vs_mh_configedit matches 2 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Compact Mode"},{"color":"white","bold":true,"text":"\n       - "},{"color":"yellow","bold":true,"text":"FORCE ON"}]

execute if score -displayhud vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Display"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ALLOW"}]
execute if score -displayhud vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Display"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISALLOW"}]
execute if score -displayhud vs_mh_configedit matches 2 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Display"},{"color":"white","bold":true,"text":"\n       - "},{"color":"yellow","bold":true,"text":"FORCE ON"}]

execute if score -trigger vs_mh_configedit matches 1 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Trigger Command"},{"color":"white","bold":true,"text":"\n       - "},{"color":"green","bold":true,"text":"ENABLE"}]
execute if score -trigger vs_mh_configedit matches 0 run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"Trigger Command"},{"color":"white","bold":true,"text":"\n       - "},{"color":"red","bold":true,"text":"DISABLE"}]


execute unless score -session vs_mh_configedit matches 0.. unless score -position vs_mh_configedit matches 0.. unless score -hmobs vs_mh_configedit matches 0.. unless score -pitch vs_mh_configedit matches 0.. unless score -time vs_mh_configedit matches 0.. unless score -direction vs_mh_configedit matches 0.. unless score -trigger vs_mh_configedit matches 0.. unless score -displayhud vs_mh_configedit matches 0.. unless score -compact vs_mh_configedit matches 0.. run tellraw @a[scores={vs_mh_adminlist=1}] [{"color":"white","bold":true,"text":"    - "},{"color":"white","bold":true,"text":"None"}]

execute run tellraw @a[scores={vs_mh_adminlist=1}] [{"text":""}]

execute if score -session vs_mh_configedit matches 0.. store result score -session vs_mh_config run scoreboard players get -session vs_mh_configedit
execute if score -position vs_mh_configedit matches 0.. store result score -position vs_mh_config run scoreboard players get -position vs_mh_configedit
execute if score -hmobs vs_mh_configedit matches 0.. store result score -hmobs vs_mh_config run scoreboard players get -hmobs vs_mh_configedit
execute if score -pitch vs_mh_configedit matches 0.. store result score -pitch vs_mh_config run scoreboard players get -pitch vs_mh_configedit
execute if score -time vs_mh_configedit matches 0.. store result score -time vs_mh_config run scoreboard players get -time vs_mh_configedit
execute if score -direction vs_mh_configedit matches 0.. store result score -direction vs_mh_config run scoreboard players get -direction vs_mh_configedit
execute if score -compact vs_mh_configedit matches 0.. store result score -compact vs_mh_config run scoreboard players get -compact vs_mh_configedit
execute if score -displayhud vs_mh_configedit matches 0.. store result score -displayhud vs_mh_config run scoreboard players get -displayhud vs_mh_configedit
execute if score -trigger vs_mh_configedit matches 0.. store result score -trigger vs_mh_config run scoreboard players get -trigger vs_mh_configedit

execute if score -trigger vs_mh_configedit matches 1 run scoreboard objectives add modularhud trigger
execute if score -trigger vs_mh_configedit matches 0 run scoreboard objectives remove modularhud

execute if score -displayhud vs_mh_configedit matches 1 run scoreboard objectives add modularhud trigger
execute if score -displayhud vs_mh_configedit matches 0 run scoreboard objectives remove modularhud
execute if score -displayhud vs_mh_configedit matches 2 run scoreboard objectives add modularhud trigger

scoreboard objectives remove vs_mh_configedit