execute as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1

tellraw @a[scores={vs_mh_adminlist=1}] [{"text":"\nModularHUD","color":"gold","bold":true},{"text":" "},{"text":"status","color":"white"},{"text":"\n - ","color":"white"},{"text":"ModularHUD","color":"white"},{"text":" "},{"text":"loaded","color":"green"},{"text":"\n"}]

scoreboard objectives remove vs_mh_configedit
scoreboard objectives remove vs_mh_uninstall

scoreboard objectives add modularhud trigger
scoreboard objectives add vs_mh_d_session dummy
scoreboard objectives add vs_mh_sessticks dummy
scoreboard objectives add vs_mh_sessmins dummy
scoreboard objectives add vs_mh_sesshours dummy
scoreboard objectives add vs_mh_sesscalc dummy
scoreboard objectives add vs_mh_sesscheck minecraft.custom:minecraft.leave_game
scoreboard objectives add vs_mh_d_position dummy
scoreboard objectives add vs_mh_positionx dummy
scoreboard objectives add vs_mh_positiony dummy
scoreboard objectives add vs_mh_positionz dummy
scoreboard objectives add vs_mh_d_hmobs dummy
scoreboard objectives add vs_mh_hmobs dummy
scoreboard objectives add vs_mh_d_pitch dummy
scoreboard objectives add vs_mh_pitch dummy
scoreboard objectives add vs_mh_d_time dummy
scoreboard objectives add vs_mh_time dummy
scoreboard objectives add vs_mh_d_dir dummy
scoreboard objectives add vs_mh_direction dummy
scoreboard objectives add vs_mh_dir_n dummy
scoreboard objectives add vs_mh_dir_ne dummy
scoreboard objectives add vs_mh_dir_e dummy
scoreboard objectives add vs_mh_dir_se dummy
scoreboard objectives add vs_mh_dir_s dummy
scoreboard objectives add vs_mh_dir_sw dummy
scoreboard objectives add vs_mh_dir_w dummy
scoreboard objectives add vs_mh_dir_nw dummy
scoreboard objectives add vs_mh_d_compact dummy
scoreboard objectives add vs_mh_d_display dummy
scoreboard objectives add vs_mh_timer dummy
scoreboard objectives add vs_mh_constants dummy
scoreboard objectives add vs_mh_config dummy
scoreboard objectives add vs_mh_adminlist dummy
scoreboard objectives add vs_mh_hi_map dummy
scoreboard objectives add vs_mh_hi_clock dummy
scoreboard objectives add vs_mh_hi_compass dummy
scoreboard objectives add vs_mh_hi_beacon dummy

execute unless score -session vs_mh_config matches 0 unless score -session vs_mh_config matches 2..3 run scoreboard players set -session vs_mh_config 1
execute unless score -position vs_mh_config matches 0 unless score -position vs_mh_config matches 2..3 run scoreboard players set -position vs_mh_config 1
execute unless score -hmobs vs_mh_config matches 1..3 run scoreboard players set -hmobs vs_mh_config 0
execute unless score -pitch vs_mh_config matches 0 unless score -pitch vs_mh_config matches 2..3 run scoreboard players set -pitch vs_mh_config 1
execute unless score -time vs_mh_config matches 0..2 run scoreboard players set -time vs_mh_config 3
execute unless score -direction vs_mh_config matches 0 unless score -direction vs_mh_config matches 2..3 run scoreboard players set -direction vs_mh_config 1
execute unless score -compact vs_mh_config matches 0 unless score -compact vs_mh_config matches 2..3 run scoreboard players set -compact vs_mh_config 1
execute unless score -displayhud vs_mh_config matches 0 unless score -displayhud vs_mh_config matches 2..3 run scoreboard players set -displayhud vs_mh_config 1
execute unless score -trigger vs_mh_config matches 0 run scoreboard players set -trigger vs_mh_config 1
scoreboard players set -6 vs_mh_constants 6
scoreboard players set -24 vs_mh_constants 24
scoreboard players set -60 vs_mh_constants 60
scoreboard players set -100 vs_mh_constants 100
scoreboard players set -360 vs_mh_constants 360
scoreboard players set -1000 vs_mh_constants 1000
scoreboard players set -1200 vs_mh_constants 1200
scoreboard players set -6000 vs_mh_constants 6000
scoreboard players set -72000 vs_mh_constants 72000