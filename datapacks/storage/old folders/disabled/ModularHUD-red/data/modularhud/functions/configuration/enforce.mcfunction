execute as @a run scoreboard players add @s vs_mh_d_session 0
execute as @a run scoreboard players add @s vs_mh_d_position 0
execute as @a run scoreboard players add @s vs_mh_d_hmobs 0
execute as @a run scoreboard players add @s vs_mh_d_pitch 0
execute as @a run scoreboard players add @s vs_mh_d_time 0
execute as @a run scoreboard players add @s vs_mh_d_dir 0
execute as @a run scoreboard players add @s vs_mh_d_compact 0
execute as @a run scoreboard players add @s vs_mh_d_display 0

execute if score -session vs_mh_config matches 0 as @a[scores={vs_mh_d_session=1}] run scoreboard players set @s vs_mh_d_session 0
execute if score -position vs_mh_config matches 0 as @a[scores={vs_mh_d_position=1}] run scoreboard players set @s vs_mh_d_position 0
execute if score -hmobs vs_mh_config matches 0 as @a[scores={vs_mh_d_hmobs=1}] run scoreboard players set @s vs_mh_d_hmobs 0
execute if score -pitch vs_mh_config matches 0 as @a[scores={vs_mh_d_pitch=1}] run scoreboard players set @s vs_mh_d_pitch 0
execute if score -time vs_mh_config matches 0 as @a[scores={vs_mh_d_time=1}] run scoreboard players set @s vs_mh_d_time 0
execute if score -direction vs_mh_config matches 0 as @a[scores={vs_mh_d_dir=1}] run scoreboard players set @s vs_mh_d_dir 0
execute if score -compact vs_mh_config matches 0 as @a[scores={vs_mh_d_compact=1}] run scoreboard players set @s vs_mh_d_compact 0
execute if score -displayhud vs_mh_config matches 0 as @a[scores={vs_mh_d_display=1}] run scoreboard players set @s vs_mh_d_display 0

execute if score -session vs_mh_config matches 2 as @a[scores={vs_mh_d_session=0}] run scoreboard players set @s vs_mh_d_session 1
execute if score -position vs_mh_config matches 2 as @a[scores={vs_mh_d_position=0}] run scoreboard players set @s vs_mh_d_position 1
execute if score -hmobs vs_mh_config matches 2 as @a[scores={vs_mh_d_hmobs=0}] run scoreboard players set @s vs_mh_d_hmobs 1
execute if score -pitch vs_mh_config matches 2 as @a[scores={vs_mh_d_pitch=0}] run scoreboard players set @s vs_mh_d_pitch 1
execute if score -time vs_mh_config matches 2 as @a[scores={vs_mh_d_time=0}] run scoreboard players set @s vs_mh_d_time 1
execute if score -direction vs_mh_config matches 2 as @a[scores={vs_mh_d_dir=0}] run scoreboard players set @s vs_mh_d_dir 1
execute if score -compact vs_mh_config matches 2 as @a[scores={vs_mh_d_compact=0}] run scoreboard players set @s vs_mh_d_compact 1
execute if score -displayhud vs_mh_config matches 2 as @a[scores={vs_mh_d_display=0}] run scoreboard players set @s vs_mh_d_display 1

execute if score -position vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1}] store result score @s vs_mh_hi_map run clear @s minecraft:map 0
execute if score -position vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1}] store result score @s vs_mh_hi_map run clear @s minecraft:filled_map 0
execute if score -session vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1}] store result score @s vs_mh_hi_clock run clear @s minecraft:clock 0
execute if score -time vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1}] store result score @s vs_mh_hi_clock run clear @s minecraft:clock 0
execute if score -pitch vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1}] store result score @s vs_mh_hi_compass run clear @s minecraft:compass 0
execute if score -direction vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1}] store result score @s vs_mh_hi_compass run clear @s minecraft:compass 0
execute if score -hmobs vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1}] store result score @s vs_mh_hi_beacon run clear @s minecraft:beacon 0

execute if score -position vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_map=1}] run scoreboard players set @s vs_mh_d_position 1
execute if score -session vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_clock=1}] run scoreboard players set @s vs_mh_d_session 1
execute if score -time vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_clock=1}] run scoreboard players set @s vs_mh_d_time 1
execute if score -pitch vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_compass=1}] run scoreboard players set @s vs_mh_d_pitch 1
execute if score -direction vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_compass=1}] run scoreboard players set @s vs_mh_d_dir 1
execute if score -hmobs vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_beacon=1}] run scoreboard players set @s vs_mh_d_hmobs 1

execute if score -position vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_map=0}] run scoreboard players set @s vs_mh_d_position 0
execute if score -session vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_clock=0}] run scoreboard players set @s vs_mh_d_session 0
execute if score -time vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_clock=0}] run scoreboard players set @s vs_mh_d_time 0
execute if score -pitch vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_compass=0}] run scoreboard players set @s vs_mh_d_pitch 0
execute if score -direction vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_compass=0}] run scoreboard players set @s vs_mh_d_dir 0
execute if score -hmobs vs_mh_config matches 3 as @a[scores={vs_mh_d_display=1,vs_mh_hi_beacon=0}] run scoreboard players set @s vs_mh_d_hmobs 0