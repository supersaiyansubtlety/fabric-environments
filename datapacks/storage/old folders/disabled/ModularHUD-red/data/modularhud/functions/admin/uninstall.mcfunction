execute as @a[scores={vs_mh_adminlist=1}] at @s run playsound minecraft:block.note_block.hat master @s ~ ~ ~ 1 2 1

scoreboard objectives add vs_mh_uninstall dummy

scoreboard players set -uninstalling vs_mh_uninstall 1

tellraw @a[scores={vs_mh_adminlist=1}] [{"text":"\nModularHUD","color":"gold","bold":true},{"text":" "},{"text":"uninstallation","color":"white"},{"text":"\n - ","color":"white"},{"text":"Are you sure you want to uninstall ModularHUD?","color":"white"},{"text":"\n    - ","color":"white"},{"text":"[CONTINUE]","color":"green","italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Continue and uninstall ModularHUD"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set -uninstalling vs_mh_uninstall 2"}},{"text":" "},{"text":"[CANCEL]\n","color":"red","italic":true,"hoverEvent":{"action":"show_text","value":{"text":"Cancel"}},"clickEvent":{"action":"run_command","value":"/scoreboard players set -uninstalling vs_mh_uninstall 3"}}]