#Current As/At: Person who requested to look at coords but didn't get them corrected yet

#If end requested, but in overworld/nether dimension, correct to end portal coords
#End -> End = End -> End
#End -> Overworld = End -> End End Portal
#End -> Nether = End -> End End Portal
#End -> Nether = End -> End End Portal
#Overworld -> End = Overworld -> Overworld End Portal
#Nether -> End = Nether -> Overworld End Portal
execute as @s[nbt={Dimension:-1}] at @s if score @s bccm_faceDim matches 1 run function bertiecrafter:end_in_overworld/cm_preparefacing
execute as @s[nbt={Dimension:0}] at @s if score @s bccm_faceDim matches 1 run function bertiecrafter:end_in_overworld/cm_preparefacing
execute as @s[nbt={Dimension:1}] at @s unless score @s bccm_faceDim matches 1 run function bertiecrafter:end_in_end/cm_preparefacing

#If overworld is requested and you are in nether, or the other way around, multiply or divide by 8
#Note: The previous section reroutes a cross-dimension journey to the end, so we only have to care about nether/overworld journeys.
execute as @s[nbt={Dimension:-1}] at @s if score @s bccm_faceDim matches 0 run function bertiecrafter:face/cm_dividebyeight
execute as @s[nbt={Dimension:0}] at @s if score @s bccm_faceDim matches -1 run function bertiecrafter:face/cm_multiplybyeight

function bertiecrafter:face/cm_spawnpos