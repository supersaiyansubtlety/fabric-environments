#Current As/At: Dead player
execute store result score @s bccm_deathX run data get entity @s Pos[0]
execute store result score @s bccm_deathY run data get entity @s Pos[1]
execute store result score @s bccm_deathZ run data get entity @s Pos[2]
execute store result score @s bccm_deathD run data get entity @s Dimension
tag @s add bccm_deathObfuscated
scoreboard players enable @s bccm_deathU
scoreboard players set @s bccm_death 0