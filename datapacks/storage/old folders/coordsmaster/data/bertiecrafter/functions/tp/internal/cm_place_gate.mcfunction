execute unless block ~ ~ ~ air run title @s actionbar {"text":"Block at Y=255 in the destination dimension is occupied!","color":"red"}
execute if block ~ ~ ~ air run setblock ~ ~ ~ minecraft:end_gateway{ExactTeleport:1b,Age:200L}
execute if block ~ ~ ~ end_gateway store result block ~ ~ ~ ExitPortal.X int 1 run scoreboard players get @s bccm_tpX
execute if block ~ ~ ~ end_gateway store result block ~ ~ ~ ExitPortal.Y int 1 run scoreboard players get @s bccm_tpY
execute if block ~ ~ ~ end_gateway store result block ~ ~ ~ ExitPortal.Z int 1 run scoreboard players get @s bccm_tpZ
execute if block ~ ~ ~ end_gateway run summon armor_stand ~ ~ ~ {Invulnerable:1b,Invisible:1b,NoGravity:1b,Marker:1b,Tags:["bccm_gateway"]}
tag @s remove bccm_tpNeeded