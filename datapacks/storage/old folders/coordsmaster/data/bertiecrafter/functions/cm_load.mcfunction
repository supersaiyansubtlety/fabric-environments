#Current As/At: Server
tellraw @a {"text":"---------------------------------","color":"aqua","extra":[{"text":"\n\nLoaded Datapack: "},{"text":"Coords Master","color":"gold"},{"text":"\n\nBy: "},{"text":"Bertiecrafter","color":"blue","underlined":true,"clickEvent":{"action":"open_url","value":"https://www.planetminecraft.com/member/bertiecrafter"}},{"text":"\n\n"},{"text":"[Uninstall Instructions]","hoverEvent":{"action":"show_text","value":{"text":"Uninstall by running\n/function bertiecrafter:cm_uninstall\nbefore deleting the datapack files."}}},{"text":"\n\n---------------------------------"}]}
scoreboard objectives add bccm_death deathCount {"text":"Detect deaths"}
scoreboard objectives add bccm_deathX dummy {"text":"X Coordinate On Death"}
scoreboard objectives add bccm_deathY dummy {"text":"Y Coordinate On Death"}
scoreboard objectives add bccm_deathZ dummy {"text":"Z Coordinate On Death"}
scoreboard objectives add bccm_deathD dummy {"text":"Dimension On Death"}
scoreboard objectives add bccm_deathU trigger {"text":"Unlock Death Coords"}
scoreboard objectives add bccm_deathF trigger {"text":"Face Death Coords"}
scoreboard objectives add bccm_faceX dummy {"text":"X Coordinate that should be faced"}
scoreboard objectives add bccm_faceY dummy {"text":"Y Coordinate that should be faced"}
scoreboard objectives add bccm_faceZ dummy {"text":"Z Coordinate that should be faced"}
scoreboard objectives add bccm_faceD dummy {"text":"Divisor of coordinates that should be faced"}
scoreboard objectives add bccm_faceDim dummy {"text":"Dimension of coordinates that should be faced"}
scoreboard players set #negative_one bccm_faceD -1
scoreboard players set #distance_cap bccm_faceD 64
scoreboard players set #one bccm_faceD 1
scoreboard players set #eight bccm_faceD 8
scoreboard objectives add bccm_faceAX dummy {"text":"Absolute X Coordinate that should be faced"}
scoreboard objectives add bccm_faceAY dummy {"text":"Absolute Y Coordinate that should be faced"}
scoreboard objectives add bccm_faceAZ dummy {"text":"Absolute Z Coordinate that should be faced"}
scoreboard objectives add bccm_faceOX dummy {"text":"Own X Coordinate used for facing another"}
scoreboard objectives add bccm_faceOY dummy {"text":"Own Y Coordinate used for facing another"}
scoreboard objectives add bccm_faceOZ dummy {"text":"Own Z Coordinate used for facing another"}
scoreboard objectives add bccm_endDX dummy {"text":"X Of End Portal In End"}
scoreboard objectives add bccm_endDY dummy {"text":"Y Of End Portal In End"}
scoreboard objectives add bccm_endDZ dummy {"text":"Z Of End Portal In End"}
scoreboard objectives add bccm_endDS trigger {"text":"Set End Portal In End Coords"}
scoreboard objectives add bccm_endDF trigger {"text":"Face End Portal In End Coords"}
scoreboard objectives add bccm_endOX dummy {"text":"X Of End Portal In Overworld"}
scoreboard objectives add bccm_endOY dummy {"text":"Y Of End Portal In Overworld"}
scoreboard objectives add bccm_endOZ dummy {"text":"Z Of End Portal In Overworld"}
scoreboard objectives add bccm_endOS trigger {"text":"Set End Portal In Overworld Coords"}
scoreboard objectives add bccm_endOF trigger {"text":"Face End Portal In Overworld Coords"}
scoreboard objectives add bccm_homeSleep minecraft.custom:minecraft.sleep_in_bed {"text":"Sleep trigger"}
scoreboard objectives add bccm_homeX dummy {"text":"X Coordinate Of Home"}
scoreboard objectives add bccm_homeY dummy {"text":"Y Coordinate Of Home"}
scoreboard objectives add bccm_homeZ dummy {"text":"Z Coordinate Of Home"}
scoreboard objectives add bccm_homeU trigger {"text":"Unlock Home Coords"}
scoreboard objectives add bccm_homeF trigger {"text":"Face Home Coords"}
scoreboard objectives add bccm_loc1X dummy {"text":"X Coordinate Of Location 1"}
scoreboard objectives add bccm_loc1Y dummy {"text":"Y Coordinate Of Location 1"}
scoreboard objectives add bccm_loc1Z dummy {"text":"Z Coordinate Of Location 1"}
scoreboard objectives add bccm_loc1D dummy {"text":"Dimension Of Location 1"}
scoreboard objectives add bccm_loc2X dummy {"text":"X Coordinate Of Location 2"}
scoreboard objectives add bccm_loc2Y dummy {"text":"Y Coordinate Of Location 2"}
scoreboard objectives add bccm_loc2Z dummy {"text":"Z Coordinate Of Location 2"}
scoreboard objectives add bccm_loc2D dummy {"text":"Dimension Of Location 2"}
scoreboard objectives add bccm_loc3X dummy {"text":"X Coordinate Of Location 3"}
scoreboard objectives add bccm_loc3Y dummy {"text":"Y Coordinate Of Location 3"}
scoreboard objectives add bccm_loc3Z dummy {"text":"Z Coordinate Of Location 3"}
scoreboard objectives add bccm_loc3D dummy {"text":"Dimension Of Location 3"}
scoreboard objectives add bccm_loc4X dummy {"text":"X Coordinate Of Location 4"}
scoreboard objectives add bccm_loc4Y dummy {"text":"Y Coordinate Of Location 4"}
scoreboard objectives add bccm_loc4Z dummy {"text":"Z Coordinate Of Location 4"}
scoreboard objectives add bccm_loc4D dummy {"text":"Dimension Of Location 4"}
scoreboard objectives add bccm_loc5X dummy {"text":"X Coordinate Of Location 4"}
scoreboard objectives add bccm_loc5Y dummy {"text":"Y Coordinate Of Location 4"}
scoreboard objectives add bccm_loc5Z dummy {"text":"Z Coordinate Of Location 4"}
scoreboard objectives add bccm_loc5D dummy {"text":"Dimension Of Location 4"}
scoreboard objectives add bccm_locS trigger {"text":"Set location coords"}
scoreboard objectives add bccm_locF trigger {"text":"Face location coords"}
scoreboard objectives add bccm_inputX trigger {"text":"X Coordinate Of Input"}
scoreboard objectives add bccm_inputY trigger {"text":"Y Coordinate Of Input"}
scoreboard objectives add bccm_inputZ trigger {"text":"Z Coordinate Of Input"}
scoreboard objectives add bccm_inputD trigger {"text":"Dimension Of Input"}
scoreboard objectives add bccm_inputS trigger {"text":"Set Input coords"}
scoreboard objectives add bccm_inputF trigger {"text":"Face Input coords"}
scoreboard objectives add bccm_tpD trigger {"text":"Dimension that should be TPed to"}
scoreboard objectives add bccm_tpX trigger {"text":"X Coordinate that should be TPed to"}
scoreboard objectives add bccm_tpY trigger {"text":"Y Coordinate that should be TPed to"}
scoreboard objectives add bccm_tpZ trigger {"text":"Z Coordinate that should be TPed to"}