#Current As/At: Player who requested changing input coords.
scoreboard players enable @s bccm_inputX
scoreboard players enable @s bccm_inputY
scoreboard players enable @s bccm_inputZ
scoreboard players enable @s bccm_inputD
scoreboard players set @s bccm_inputX 0
scoreboard players set @s bccm_inputY 0
scoreboard players set @s bccm_inputZ 0
scoreboard players set @s bccm_inputD 0
clear @s[gamemode=survival] minecraft:compass 1
scoreboard players set @s bccm_inputS 0