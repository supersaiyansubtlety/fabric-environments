
# Remove all SurvivalModified scores
scoreboard players reset #sm_colorful_shulkers_installed

# Disable module
datapack disable "file/SM Colorful Shulkers v1.0.0"

# Message
tellraw @s [{"text":"[SurvivalModified] ","color":"light_purple"},{"text":"Uninstall of [Colorful Shulkers] successful.","color":"green"}]
