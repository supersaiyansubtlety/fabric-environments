# Prepare and start Randomizer
scoreboard players set #sm_prng_max_result SurvivalModified 17
function sm_prng:calculate_random_number
scoreboard players reset #sm_prng_max_result

# Get color depending on randomizer result
execute if score #sm_prng_result_in_range SurvivalModified matches 1 run data modify entity @s Color set value 1
execute if score #sm_prng_result_in_range SurvivalModified matches 2 run data modify entity @s Color set value 2
execute if score #sm_prng_result_in_range SurvivalModified matches 3 run data modify entity @s Color set value 3
execute if score #sm_prng_result_in_range SurvivalModified matches 4 run data modify entity @s Color set value 4
execute if score #sm_prng_result_in_range SurvivalModified matches 5 run data modify entity @s Color set value 5
execute if score #sm_prng_result_in_range SurvivalModified matches 6 run data modify entity @s Color set value 6
execute if score #sm_prng_result_in_range SurvivalModified matches 7 run data modify entity @s Color set value 7
execute if score #sm_prng_result_in_range SurvivalModified matches 8 run data modify entity @s Color set value 8
execute if score #sm_prng_result_in_range SurvivalModified matches 9 run data modify entity @s Color set value 9
execute if score #sm_prng_result_in_range SurvivalModified matches 10 run data modify entity @s Color set value 10
execute if score #sm_prng_result_in_range SurvivalModified matches 11 run data modify entity @s Color set value 11
execute if score #sm_prng_result_in_range SurvivalModified matches 12 run data modify entity @s Color set value 12
execute if score #sm_prng_result_in_range SurvivalModified matches 13 run data modify entity @s Color set value 13
execute if score #sm_prng_result_in_range SurvivalModified matches 14 run data modify entity @s Color set value 14
execute if score #sm_prng_result_in_range SurvivalModified matches 15 run data modify entity @s Color set value 15
execute if score #sm_prng_result_in_range SurvivalModified matches 16 run data modify entity @s Color set value 16

tag @s add cs_tag_colored_shulker
