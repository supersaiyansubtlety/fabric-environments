# Get current position
execute store result score @s sm_afk_x1 run data get entity @s Pos[0] 100
execute store result score @s sm_afk_y1 run data get entity @s Pos[1] 100
execute store result score @s sm_afk_z1 run data get entity @s Pos[2] 100

# Compare with position saved last second, if not same, set score to not afk
execute unless score @s sm_afk_x1 = @s sm_afk_x2 run scoreboard players set @s sm_player_is_afk 0
execute unless score @s sm_afk_y1 = @s sm_afk_y2 run scoreboard players set @s sm_player_is_afk 0
execute unless score @s sm_afk_z1 = @s sm_afk_z1 run scoreboard players set @s sm_player_is_afk 0

# If AFK display is enabled, leave in AFK team
execute if score #sm_base_afk_display SurvivalModified matches 1 run team leave @s[scores={sm_player_is_afk=0}]


# Save current position to be compared next second
scoreboard players operation @s sm_afk_x2 = @s sm_afk_x1
scoreboard players operation @s sm_afk_y2 = @s sm_afk_y1
scoreboard players operation @s sm_afk_z2 = @s sm_afk_z1

#Set afk seconds to 0 so the afk timer doesnt go up for afk players
scoreboard players set @s sm_afk_seconds 0 
