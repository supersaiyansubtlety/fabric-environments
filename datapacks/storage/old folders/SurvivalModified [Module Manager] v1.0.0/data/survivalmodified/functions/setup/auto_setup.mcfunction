# Installs all statistic-tracking scoreboards
scoreboard objectives add sm_armor armor "Armor points"
scoreboard objectives add sm_dragonkill minecraft.killed:minecraft.ender_dragon "Ender Dragons killed"
scoreboard objectives add sm_witherkill minecraft.killed:minecraft.wither "Withers killed"
scoreboard objectives add sm_level level "Player XP level"
scoreboard objectives add sm_crafts minecraft.custom:minecraft.interact_with_crafting_table "Interactions with Crafting Table"
scoreboard objectives add sm_damage_dealt minecraft.custom:minecraft.damage_dealt "Damage dealt"
scoreboard objectives add sm_damage_taken minecraft.custom:minecraft.damage_taken "Damage taken"
scoreboard objectives add sm_deaths minecraft.custom:minecraft.deaths "Deaths"
scoreboard objectives add sm_items_enchant minecraft.custom:minecraft.enchant_item "Items enchanted"
scoreboard objectives add sm_mob_kills minecraft.custom:minecraft.mob_kills "Mobs killed"
scoreboard objectives add sm_playtime_disp dummy "Playtime in minutes"
scoreboard objectives add sm_playtime_tick minecraft.custom:minecraft.play_one_minute "Playtime in ticks"
scoreboard players set #sm_playtime_minutes SurvivalModified 1200 
scoreboard objectives add sm_villager_trad minecraft.custom:minecraft.traded_with_villager "Villager trades"
scoreboard objectives add sm_stone_mined minecraft.mined:minecraft.stone "Stone mined"
scoreboard objectives add sm_player_kills playerKillCount "Players killed"
scoreboard objectives add sm_raid_wins minecraft.custom:minecraft.raid_win "Raids won"

# Set up tracking scoreboards
scoreboard objectives add sm_health health {"text":" ❤","color":"red"}
scoreboard objectives add sm_food food "Food Level"

# Set up AFK Scoreboards
scoreboard objectives add sm_afk_x1 dummy
scoreboard objectives add sm_afk_x2 dummy
scoreboard objectives add sm_afk_y1 dummy
scoreboard objectives add sm_afk_y2 dummy
scoreboard objectives add sm_afk_z1 dummy
scoreboard objectives add sm_afk_z2 dummy
scoreboard objectives add sm_player_is_afk dummy

# Set up AFK Team
team add sm_team_afk "AFK"

# Trigger for Info and custom settings
scoreboard objectives add sm_Help trigger

# Player Position
scoreboard objectives add sm_PosX dummy
scoreboard objectives add sm_PosY dummy
scoreboard objectives add sm_PosZ dummy

# ColoredTeams
team add sm_team_aqua
team modify sm_team_aqua color aqua
team add sm_team_black
team modify sm_team_black color black
team add sm_team_blue
team modify sm_team_blue color blue
team add sm_team_darkaqua
team modify sm_team_darkaqua color dark_aqua
team add sm_team_darkblue
team modify sm_team_darkblue color dark_blue
team add sm_team_darkgray
team modify sm_team_darkgray color dark_gray
team add sm_team_darkgrn
team modify sm_team_darkgrn color dark_green
team add sm_team_darkprpl
team modify sm_team_darkprpl color dark_purple
team add sm_team_dark_red
team modify sm_team_dark_red color dark_red
team add sm_team_gold
team modify sm_team_gold color gold
team add sm_team_gray
team modify sm_team_gray color gray
team add sm_team_green
team modify sm_team_green color green
team add sm_team_lghtprpl
team modify sm_team_lghtprpl color light_purple
team add sm_team_red
team modify sm_team_red color red
team add sm_team_white
team modify sm_team_white color white
team add sm_team_yellow
team modify sm_team_yellow color yellow



# Standard Settings
 # List modules on
scoreboard players set #sm_base_list_modules SurvivalModified 1
 # Afk Display on
scoreboard players set #sm_base_afk_display SurvivalModified 1
 # AFK Team Color
scoreboard players set #sm_base_afk_team_color SurvivalModified 11
team modify sm_team_afk color gray
 # Prefix on
scoreboard players set #sm_base_afk_team_prefix SurvivalModified 1
 # Prefix color 
scoreboard players set #sm_base_afk_prefix_color SurvivalModified 6
team modify sm_team_afk prefix {"text":" [AFK] ","color":"dark_gray"}

# Setup Info
tellraw @a [{"text":"[SurvivalModified] ","color":"light_purple"},{"text":"Welcome to SurvivalModified.","color":"aqua"}]
tellraw @a [{"text":"All SurvivalModified Modules come with standard settings which can be customized using the manager by calling ","color":"aqua"},{"text":"/trigger sm_Help","color":"light_purple"},{"text":" and clicking","color":"aqua"},{"text":"[Setup]","color":"gold"},{"text":".","color":"aqua"}]

# Setup finished
scoreboard players set #sm_base_auto_setup_finished SurvivalModified 1



