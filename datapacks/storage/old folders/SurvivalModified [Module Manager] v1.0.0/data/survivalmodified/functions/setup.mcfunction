# Set base objective
scoreboard objectives add SurvivalModified dummy "SurvivalModified Counters"

# Auto Setup: Scoreboards, Standard Settings and Info
execute unless score #sm_base_auto_setup_finished SurvivalModified matches 1 run function survivalmodified:setup/auto_setup


# Prepare Reload: Set module count to 0 and have the moduless add themselves on setup
function survivalmodified:check_installed


# Call all setup functions from dependent modules
function #survivalmodified:setup

# Call reload function
function survivalmodified:reload
