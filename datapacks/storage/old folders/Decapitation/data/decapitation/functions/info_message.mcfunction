# Desc: Tells the selected player more info about datapack:
# Called by: decapitation:conditioncheck
# Datapack made by TheDiamondPlayables

tellraw @s [{"text":"[Decapitation] ","color":"aqua"},{"text":"This datapack is made to do the following things:","color":"gold"}]
tellraw @s [{"text":" - All mobs now have a chance of dropping their heads when killed by a player.","color":"gold"}]
tellraw @s [{"text":" - If a mob is killed by a","color":"gold"},{"text":" charged creeper","color":"green"},{"text":" it will instantly drop its head!","color":"gold"}]
scoreboard players set @s dc_info 0