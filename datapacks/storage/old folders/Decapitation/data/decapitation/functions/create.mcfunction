# Desc: Creates required global scoreboards
# Called by: main:create(tag)
# Datapack made by TheDiamondPlayables

scoreboard objectives add dc_info trigger {"text":"Datapack Info","color":"green"}