execute as @s[type=hoglin] run particle minecraft:block redstone_block ~ ~0.6 ~ 0.1 0.1 0.1 1 10
execute as @s[type=hoglin,nbt={Age:0}] run particle minecraft:block redstone_block ~ ~1.4 ~ 0.3 0.2 0.3 1 15
execute as @s[type=piglin] run particle minecraft:block redstone_block ~ ~1.2 ~ 0.1 0.2 0.1 1 15
execute as @s[type=zombified_piglin] run particle minecraft:block lime_concrete_powder ~ ~1.2 ~ 0.1 0.2 0.1 1 10
execute as @s[type=zombified_piglin] run particle minecraft:block crimson_nylium ~ ~1.2 ~ 0.1 0.2 0.1 1 5
execute as @s[type=zombified_piglin] run particle minecraft:block bone_block ~ ~1.7 ~ 0.1 0.1 0.1 1 3
# new particules
execute as @s[type=wither_skeleton] run particle minecraft:soul ~ ~1.5 ~ 0.2 0.4 0.2 0.0001 3