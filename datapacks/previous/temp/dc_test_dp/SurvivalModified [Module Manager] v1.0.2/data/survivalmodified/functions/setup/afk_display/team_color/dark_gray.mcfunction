# Set Color, finish install
scoreboard players set #sm_base_afk_team_color SurvivalModified 6
team modify sm_team_afk color dark_gray

# Message 
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Color: ","color":"green"},{"text":"[Dark Gray]","color":"dark_gray"},{"text":".","color":"green"}]
