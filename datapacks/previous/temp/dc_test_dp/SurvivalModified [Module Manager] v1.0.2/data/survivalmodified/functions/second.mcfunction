# Countdown Timer
scoreboard players remove #sm_timer_1_minute SurvivalModified 1
execute if score #sm_timer_1_minute SurvivalModified matches 0 run function #survivalmodified:minute
execute if score #sm_timer_1_minute SurvivalModified matches 0 run scoreboard players set #sm_timer_1_minute SurvivalModified 60

# Check still AFK
execute as @a[scores={sm_player_is_afk=1}] at @s run function survivalmodified:afk_display/check_still_afk

# Help
execute as @a[scores={sm_Help=1}] run function survivalmodified:help
scoreboard players enable @a sm_Help
