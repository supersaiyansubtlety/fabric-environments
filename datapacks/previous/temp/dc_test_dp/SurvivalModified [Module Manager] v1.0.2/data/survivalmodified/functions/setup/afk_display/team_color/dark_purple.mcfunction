# Set Color, finish install
scoreboard players set #sm_base_afk_team_color SurvivalModified 8
team modify sm_team_afk color dark_purple

# Message 
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Color: ","color":"green"},{"text":"[Dark Purple]","color":"dark_purple"},{"text":".","color":"green"}]
