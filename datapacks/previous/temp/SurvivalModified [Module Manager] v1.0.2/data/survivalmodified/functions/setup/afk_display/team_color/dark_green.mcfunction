# Set Color, finish install
scoreboard players set #sm_base_afk_team_color SurvivalModified 7
team modify sm_team_afk color dark_green

# Message 
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Color: ","color":"green"},{"text":"[Dark Green]","color":"dark_green"},{"text":".","color":"green"}]
