# If called, set Setup to "complete"
scoreboard players set #sm_base_afk_prefix_color SurvivalModified 13
team modify sm_team_afk prefix {"text":" [AFK] ","color":"light_purple"}

# Message
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Prefix: ","color":"green"},{"text":"[AFK]","color":"light_purple"},{"text":".","color":"green"}]
