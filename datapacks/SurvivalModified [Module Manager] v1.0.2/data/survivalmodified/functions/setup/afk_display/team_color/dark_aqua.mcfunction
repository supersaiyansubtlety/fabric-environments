# Set Color, finish install
scoreboard players set #sm_base_afk_team_color SurvivalModified 4
team modify sm_team_afk color dark_aqua

# Message 
tellraw @a [{"text":"[SurvivalModified Setup] ","color":"light_purple"},{"text":"AFK Team Color: ","color":"green"},{"text":"[Dark Aqua]","color":"dark_aqua"},{"text":".","color":"green"}]
