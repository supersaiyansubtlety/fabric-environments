# Get current position
execute store result score @s sm_afk_x1 run data get entity @s Pos[0] 100
execute store result score @s sm_afk_y1 run data get entity @s Pos[1] 100
execute store result score @s sm_afk_z1 run data get entity @s Pos[2] 100
execute store result score @s sm_afk_hori1 run data get entity @s Rotation[0] 100
execute store result score @s sm_afk_verti1 run data get entity @s Rotation[1] 100

# Compare with position saved 2 minutes ago, if same, set score to afk
execute if score @s sm_afk_x1 = @s sm_afk_x2 if score @s sm_afk_y1 = @s sm_afk_y2 if score @s sm_afk_z1 = @s sm_afk_z2 if score @s sm_afk_hori1 = @s sm_afk_hori2 if score @s sm_afk_verti1 = @s sm_afk_verti2 run scoreboard players set @s sm_player_is_afk 1

# If AFK display is enabled, put player in AFK team
execute if score #sm_base_afk_display SurvivalModified matches 1 run team join sm_team_afk @s[scores={sm_player_is_afk=1}]

# Save current position to be compared in 2 minutes
scoreboard players operation @s sm_afk_x2 = @s sm_afk_x1
scoreboard players operation @s sm_afk_y2 = @s sm_afk_y1
scoreboard players operation @s sm_afk_z2 = @s sm_afk_z1
scoreboard players operation @s sm_afk_hori2 = @s sm_afk_hori1
scoreboard players operation @s sm_afk_verti2 = @s sm_afk_verti1

