
# Template
#scoreboard players set #sm_?_installed SurvivalModified 0

# Check installed Packs by setting them to 0 and have them re-enable themselves on reload
scoreboard players set #sm_anti_grief_installed SurvivalModified 0
scoreboard players set #sm_anti_enderman_grief_installed SurvivalModified 0
scoreboard players set #sm_anti_enderdragon_grief_installed SurvivalModified 0
scoreboard players set #sm_anti_wither_grief_installed SurvivalModified 0
scoreboard players set #sm_biting_bats_installed SurvivalModified 0
scoreboard players set #sm_blood_moon_installed SurvivalModified 0
scoreboard players set #sm_buffed_totem_of_undying_installed SurvivalModified 0
scoreboard players set #sm_colorful_shulkers_installed SurvivalModified 0
scoreboard players set #sm_compressed_cobblestone_installed SurvivalModified 0
scoreboard players set #sm_dragon_demands_battle_installed SurvivalModified 0
scoreboard players set #sm_effect_names_installed SurvivalModified 0
scoreboard players set #sm_extra_hard_difficulty_installed SurvivalModified 0
scoreboard players set #sm_health_boost_rewards_installed SurvivalModified 0
scoreboard players set #sm_heavy_armor_installed SurvivalModified 0
scoreboard players set #sm_lazy_chunk_cleanup_installed SurvivalModified 0
scoreboard players set #sm_leather_armor_effects_installed SurvivalModified 0
scoreboard players set #sm_prevent_afk_fishing_installed SurvivalModified 0
scoreboard players set #sm_progressive_tools_crafting_installed SurvivalModified 0
scoreboard players set #sm_scoreboard_displayer_installed SurvivalModified 0
scoreboard players set #sm_stronger_shulker_bullets_installed SurvivalModified 0

# Module Count to 0
scoreboard players set #sm_reload_module_count SurvivalModified 0
