
scoreboard players set @s EffectNames 0


# EffectNames Info Trigger
tellraw @s [{"text":"[Effect Names] ","color":"light_purple"},{"text":"Hover over a name to get more info:","color":"aqua"}]
tellraw @s [{"text":"[No Despawn] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Prevents mob from despawning.","color":"aqua"}}},{"text":"[Despawn]","color":"aqua","hoverEvent":{"action":"show_text","value":{"text":"Mob that previously was named 'No Despawn' can despawn again.","color":"aqua"}}}]
tellraw @s [{"text":"[Mute] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Mutes mob entirely.","color":"aqua"}}},{"text":"[Unmute]","color":"aqua","hoverEvent":{"action":"show_text","value":{"text":"Unmutes muted mob.","color":"aqua"}}}]
tellraw @s [{"text":"[Ghost] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Turns mob invisible with a white glow until renamed into anything else.","color":"aqua"}}}]
tellraw @s [{"text":"[Baby] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Turns mob forever into a baby variant.","color":"aqua"}}},{"text":"[Adult]","color":"aqua","hoverEvent":{"action":"show_text","value":{"text":"Instantly turns mob into adult.","color":"aqua"}}}]
tellraw @s [{"text":"[Lefthanded] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Turns mob into lefthanded variant.","color":"aqua"}}},{"text":"[Righthanded]","color":"aqua","hoverEvent":{"action":"show_text","value":{"text":"Turns mob into righthanded variant.","color":"aqua"}}}]
tellraw @s [{"text":"[Skeleton] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Turns Zombie horse into Skeleton horse.","color":"aqua"}}},{"text":"[Zombie]","color":"aqua","hoverEvent":{"action":"show_text","value":{"text":"Turns Skeleton horse into Zombie horse.","color":"aqua"}}}]
tellraw @s [{"text":"[Glowing *color*] ","color":"light_purple","hoverEvent":{"action":"show_text","value":{"text":"Makes the mob glow in the selected color (see Minecraft color code for all colors)","color":"aqua"}}},{"text":"[Remove Glowing]","color":"aqua","hoverEvent":{"action":"show_text","value":{"text":"Removes Glowing again.","color":"aqua"}}}]
